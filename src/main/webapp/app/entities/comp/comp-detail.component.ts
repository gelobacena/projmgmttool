import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { Comp } from './comp.model';
import { CompService } from './comp.service';

import { SharedStorageService } from '../../shared';

@Component({
    selector: 'jhi-comp-detail',
    templateUrl: './comp-detail.component.html'
})
export class CompDetailComponent implements OnInit, OnDestroy {

    comp: Comp;
    private subscription: Subscription;
    private eventSubscriber: Subscription;
    public history: any;

    constructor(
        private eventManager: JhiEventManager,
        private compService: CompService,
        private sharedStorageService: SharedStorageService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInComps();
    }

    load(id) {
        this.compService.find(id).subscribe((comp) => {
            this.comp = comp;
            this.sharedStorageService.setCurrentComp(this.comp);
            this.loadTests(id);
            this.loadClarifications(id);
            this.loadReviewPoints(id);
            this.loadHistory(id);
        });
    }

    loadTests(id) {
        this.compService.findTests(id).subscribe((data) => {
            this.comp.unitTests = data;
        });
    }

    loadClarifications(id) {
        this.compService.findClarifications(id).subscribe((data) => {
            this.comp.clarifications = data;
        });
    }

    loadReviewPoints(id) {
        this.compService.findPoinsheets(id).subscribe((data) => {
            this.comp.pointsheets = data;
        });
    }

    loadHistory(id) {
        this.compService.findHistory(id).subscribe((data) => {
            this.history = data;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
        this.sharedStorageService.clearStorage();
    }

    registerChangeInComps() {
        this.eventSubscriber = this.eventManager.subscribe(
            'compListModification',
            (response) => this.load(this.comp.id)
        );
    }
}
