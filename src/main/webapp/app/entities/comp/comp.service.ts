import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { Comp } from './comp.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class CompService {

    private resourceUrl = 'api/comps';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(comp: Comp): Observable<Comp> {
        const copy = this.convert(comp);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(comp: Comp): Observable<Comp> {
        const copy = this.convert(comp);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<Comp> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    findTests(id: number) {
        return this.http.get(`${this.resourceUrl}/${id}/tests`).map((res: Response) => res.json());
    }

    findClarifications(id: number) {
        return this.http.get(`${this.resourceUrl}/${id}/clarifications`).map((res: Response) => res.json());
    }

    findHistory(id: number) {
        return this.http.get(`${this.resourceUrl}/${id}/history`).map((res: Response) => res.json());
    }

    findPoinsheets(id: number) {
        return this.http.get(`${this.resourceUrl}/${id}/reviewpoints`).map((res: Response) => res.json());
    }

    findDevelopers(team: string) {
        return this.http.get('api/users/developers?team=' + team).map((res: Response) => res.json());
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.baseline_start = this.dateUtils
            .convertLocalDateFromServer(entity.baseline_start);
        entity.baseline_finish = this.dateUtils
            .convertLocalDateFromServer(entity.baseline_finish);
        entity.actual_start = this.dateUtils
            .convertLocalDateFromServer(entity.actual_start);
        entity.actual_finish = this.dateUtils
            .convertLocalDateFromServer(entity.actual_finish);
        entity.created_date = this.dateUtils
            .convertLocalDateFromServer(entity.created_date);
        entity.modified_date = this.dateUtils
            .convertLocalDateFromServer(entity.modified_date);
    }

    private convert(comp: Comp): Comp {
        const copy: Comp = Object.assign({}, comp);
        copy.baseline_start = this.dateUtils
            .convertLocalDateToServer(comp.baseline_start);
        copy.baseline_finish = this.dateUtils
            .convertLocalDateToServer(comp.baseline_finish);
        copy.actual_start = this.dateUtils
            .convertLocalDateToServer(comp.actual_start);
        copy.actual_finish = this.dateUtils
            .convertLocalDateToServer(comp.actual_finish);
        copy.created_date = this.dateUtils
            .convertLocalDateToServer(comp.created_date);
        copy.modified_date = this.dateUtils
            .convertLocalDateToServer(comp.modified_date);
        return copy;
    }
}
