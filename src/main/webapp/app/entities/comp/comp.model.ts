import { BaseEntity } from './../../shared';

export class Comp implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public budget_at_completion?: number,
        public baseline_start?: any,
        public baseline_finish?: any,
        public actual_cost?: number,
        public actual_start?: any,
        public actual_finish?: any,
        public developer?: string,
        public reviewer?: string,
        public component_type?: string,
        public status?: string,
        public created_by?: string,
        public created_date?: any,
        public modified_by?: string,
        public modified_date?: any,
        public is_deleted?: number,
        public project?: BaseEntity,
        public clarifications?: BaseEntity[],
        public pointsheets?: BaseEntity[],
        public unitTests?: BaseEntity[],
    ) {
    }
}
