import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Comp } from './comp.model';
import { CompService } from './comp.service';

@Injectable()
export class CompPopupService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
        private router: Router,
        private compService: CompService

    ) {}

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.compService.find(id).subscribe((comp) => {
                if (comp.baseline_start) {
                    comp.baseline_start = {
                        year: comp.baseline_start.getFullYear(),
                        month: comp.baseline_start.getMonth() + 1,
                        day: comp.baseline_start.getDate()
                    };
                }
                if (comp.baseline_finish) {
                    comp.baseline_finish = {
                        year: comp.baseline_finish.getFullYear(),
                        month: comp.baseline_finish.getMonth() + 1,
                        day: comp.baseline_finish.getDate()
                    };
                }
                if (comp.actual_start) {
                    comp.actual_start = {
                        year: comp.actual_start.getFullYear(),
                        month: comp.actual_start.getMonth() + 1,
                        day: comp.actual_start.getDate()
                    };
                }
                if (comp.actual_finish) {
                    comp.actual_finish = {
                        year: comp.actual_finish.getFullYear(),
                        month: comp.actual_finish.getMonth() + 1,
                        day: comp.actual_finish.getDate()
                    };
                }
                if (comp.created_date) {
                    comp.created_date = {
                        year: comp.created_date.getFullYear(),
                        month: comp.created_date.getMonth() + 1,
                        day: comp.created_date.getDate()
                    };
                }
                if (comp.modified_date) {
                    comp.modified_date = {
                        year: comp.modified_date.getFullYear(),
                        month: comp.modified_date.getMonth() + 1,
                        day: comp.modified_date.getDate()
                    };
                }
                this.compModalRef(component, comp);
            });
        } else {
            return this.compModalRef(component, new Comp());
        }
    }

    compModalRef(component: Component, comp: Comp): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.comp = comp;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
