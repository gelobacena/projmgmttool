import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Comp } from './comp.model';
import { CompPopupService } from './comp-popup.service';
import { CompService } from './comp.service';
import { Project, ProjectService } from '../project';
import { Account, Principal, ResponseWrapper, SharedStorageService } from '../../shared';

@Component({
    selector: 'jhi-comp-dialog',
    templateUrl: './comp-dialog.component.html'
})
export class CompDialogComponent implements OnInit {

    comp: Comp;
    authorities: any[];
    isSaving: boolean;
    isEdit: boolean;
    identity$: Promise<Account>;
    user: any;
    developers: any[];
    reviewers: any[];

    projects: Project[];
    baseline_startDp: any;
    baseline_finishDp: any;
    actual_startDp: any;
    actual_finishDp: any;
    created_dateDp: any;
    modified_dateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private compService: CompService,
        private principal: Principal,
        private projectService: ProjectService,
        private sharedStorageService: SharedStorageService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        const currDate = new Date();
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        if (this.comp.id !== undefined) {
            this.isEdit = true;
        }
        this.user = this.principal.identity().then((account) => {
            this.compService.findDevelopers(account.team).subscribe((devs) => {
                this.developers = devs;
            });
            if (this.isEdit) {
                this.comp.modified_by = account.login;
                this.comp.modified_date = {
                    year: currDate.getFullYear(),
                    month: currDate.getMonth() + 1,
                    day: currDate.getDate()
                };
            } else {
                this.comp.created_by = account.login;
                this.comp.created_date = {
                    year: currDate.getFullYear(),
                    month: currDate.getMonth() + 1,
                    day: currDate.getDate()
                };
            }
        });
        if (this.sharedStorageService.getCurrentProject() !== null) {
            this.comp.project = this.sharedStorageService.getCurrentProject();
        } else {
            console.log('NO PROJECT SAVED IN SHARED STORAGE');
        }
        if (this.comp.is_deleted === undefined) {
            this.comp.is_deleted = 0;
            this.comp.status = 'New';
        }
        this.projectService.query()
            .subscribe((res: ResponseWrapper) => { this.projects = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.projects = this.projects.filter((p) => p.is_deleted !== 1);
    }

    onSelect(dev) {
        this.comp.reviewer = null;
        this.reviewers = this.developers.filter((d) => d !== dev);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.comp.id !== undefined) {
            this.subscribeToSaveResponse(
                this.compService.update(this.comp));
        } else {
            this.subscribeToSaveResponse(
                this.compService.create(this.comp));
        }
    }

    private subscribeToSaveResponse(result: Observable<Comp>) {
        result.subscribe((res: Comp) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: Comp) {
        this.eventManager.broadcast({ name: 'compListModification', content: 'OK'});
        this.eventManager.broadcast({ name: 'projectListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackProjectById(index: number, item: Project) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-comp-popup',
    template: ''
})
export class CompPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private compPopupService: CompPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.compPopupService
                    .open(CompDialogComponent, params['id']);
            } else {
                this.modalRef = this.compPopupService
                    .open(CompDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
