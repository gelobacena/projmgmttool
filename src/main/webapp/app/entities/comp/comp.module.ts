import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { QdestroyerSharedModule } from '../../shared';
import {
    CompService,
    CompPopupService,
    CompComponent,
    CompDetailComponent,
    CompDialogComponent,
    CompPopupComponent,
    CompDeletePopupComponent,
    CompDeleteDialogComponent,
    compRoute,
    compPopupRoute,
} from './';

const ENTITY_STATES = [
    ...compRoute,
    ...compPopupRoute,
];

@NgModule({
    imports: [
        QdestroyerSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        CompComponent,
        CompDetailComponent,
        CompDialogComponent,
        CompDeleteDialogComponent,
        CompPopupComponent,
        CompDeletePopupComponent,
    ],
    entryComponents: [
        CompComponent,
        CompDialogComponent,
        CompPopupComponent,
        CompDeleteDialogComponent,
        CompDeletePopupComponent,
    ],
    providers: [
        CompService,
        CompPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QdestroyerCompModule {}
