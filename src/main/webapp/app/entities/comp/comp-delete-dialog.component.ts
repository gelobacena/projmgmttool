import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Comp } from './comp.model';
import { CompPopupService } from './comp-popup.service';
import { CompService } from './comp.service';

@Component({
    selector: 'jhi-comp-delete-dialog',
    templateUrl: './comp-delete-dialog.component.html'
})
export class CompDeleteDialogComponent {

    comp: Comp;

    constructor(
        private compService: CompService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.compService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'compListModification',
                content: 'Deleted an comp'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-comp-delete-popup',
    template: ''
})
export class CompDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private compPopupService: CompPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.compPopupService
                .open(CompDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
