import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { CompComponent } from './comp.component';
import { CompDetailComponent } from './comp-detail.component';
import { CompPopupComponent } from './comp-dialog.component';
import { CompDeletePopupComponent } from './comp-delete-dialog.component';

import { Principal } from '../../shared';

export const compRoute: Routes = [
    {
        path: 'comp',
        component: CompComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Comps'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'comp/:id',
        component: CompDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Comps'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const compPopupRoute: Routes = [
    {
        path: 'comp-new',
        component: CompPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Comps'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'comp/:id/edit',
        component: CompPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Comps'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'comp/:id/delete',
        component: CompDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Comps'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
