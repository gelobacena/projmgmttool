import { BaseEntity } from './../../shared';

export class ProjLog implements BaseEntity {
    constructor(
        public id?: number,
        public record_id?: number,
        public log_type?: string,
        public description?: string,
        public created_by?: string,
        public created_date?: any,
        public action?: string,
        public is_deleted?: number,
        public project?: BaseEntity,
    ) {
    }
}
