import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ProjLog } from './proj-log.model';
import { ProjLogService } from './proj-log.service';

@Injectable()
export class ProjLogPopupService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
        private router: Router,
        private projLogService: ProjLogService

    ) {}

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.projLogService.find(id).subscribe((projLog) => {
                if (projLog.created_date) {
                    projLog.created_date = {
                        year: projLog.created_date.getFullYear(),
                        month: projLog.created_date.getMonth() + 1,
                        day: projLog.created_date.getDate()
                    };
                }
                this.projLogModalRef(component, projLog);
            });
        } else {
            return this.projLogModalRef(component, new ProjLog());
        }
    }

    projLogModalRef(component: Component, projLog: ProjLog): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.projLog = projLog;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
