import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ProjLog } from './proj-log.model';
import { ProjLogPopupService } from './proj-log-popup.service';
import { ProjLogService } from './proj-log.service';

@Component({
    selector: 'jhi-proj-log-delete-dialog',
    templateUrl: './proj-log-delete-dialog.component.html'
})
export class ProjLogDeleteDialogComponent {

    projLog: ProjLog;

    constructor(
        private projLogService: ProjLogService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.projLogService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'projLogListModification',
                content: 'Deleted an projLog'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-proj-log-delete-popup',
    template: ''
})
export class ProjLogDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private projLogPopupService: ProjLogPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.projLogPopupService
                .open(ProjLogDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
