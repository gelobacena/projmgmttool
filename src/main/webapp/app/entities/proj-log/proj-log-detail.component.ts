import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { ProjLog } from './proj-log.model';
import { ProjLogService } from './proj-log.service';

@Component({
    selector: 'jhi-proj-log-detail',
    templateUrl: './proj-log-detail.component.html'
})
export class ProjLogDetailComponent implements OnInit, OnDestroy {

    projLog: ProjLog;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private projLogService: ProjLogService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInProjLogs();
    }

    load(id) {
        this.projLogService.find(id).subscribe((projLog) => {
            this.projLog = projLog;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInProjLogs() {
        this.eventSubscriber = this.eventManager.subscribe(
            'projLogListModification',
            (response) => this.load(this.projLog.id)
        );
    }
}
