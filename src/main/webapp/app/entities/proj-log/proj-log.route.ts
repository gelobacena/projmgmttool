import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ProjLogComponent } from './proj-log.component';
import { ProjLogDetailComponent } from './proj-log-detail.component';
import { ProjLogPopupComponent } from './proj-log-dialog.component';
import { ProjLogDeletePopupComponent } from './proj-log-delete-dialog.component';

import { Principal } from '../../shared';

export const projLogRoute: Routes = [
    {
        path: 'proj-log',
        component: ProjLogComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ProjLogs'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'proj-log/:id',
        component: ProjLogDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ProjLogs'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const projLogPopupRoute: Routes = [
    {
        path: 'proj-log-new',
        component: ProjLogPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ProjLogs'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'proj-log/:id/edit',
        component: ProjLogPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ProjLogs'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'proj-log/:id/delete',
        component: ProjLogDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ProjLogs'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
