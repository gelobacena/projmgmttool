import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ProjLog } from './proj-log.model';
import { ProjLogPopupService } from './proj-log-popup.service';
import { ProjLogService } from './proj-log.service';
import { Project, ProjectService } from '../project';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-proj-log-dialog',
    templateUrl: './proj-log-dialog.component.html'
})
export class ProjLogDialogComponent implements OnInit {

    projLog: ProjLog;
    authorities: any[];
    isSaving: boolean;

    projects: Project[];
    created_dateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private projLogService: ProjLogService,
        private projectService: ProjectService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.projectService.query()
            .subscribe((res: ResponseWrapper) => { this.projects = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.projLog.id !== undefined) {
            this.subscribeToSaveResponse(
                this.projLogService.update(this.projLog));
        } else {
            this.subscribeToSaveResponse(
                this.projLogService.create(this.projLog));
        }
    }

    private subscribeToSaveResponse(result: Observable<ProjLog>) {
        result.subscribe((res: ProjLog) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: ProjLog) {
        this.eventManager.broadcast({ name: 'projLogListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackProjectById(index: number, item: Project) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-proj-log-popup',
    template: ''
})
export class ProjLogPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private projLogPopupService: ProjLogPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.projLogPopupService
                    .open(ProjLogDialogComponent, params['id']);
            } else {
                this.modalRef = this.projLogPopupService
                    .open(ProjLogDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
