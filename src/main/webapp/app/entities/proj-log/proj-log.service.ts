import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { ProjLog } from './proj-log.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ProjLogService {

    private resourceUrl = 'api/proj-logs';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(projLog: ProjLog): Observable<ProjLog> {
        const copy = this.convert(projLog);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(projLog: ProjLog): Observable<ProjLog> {
        const copy = this.convert(projLog);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<ProjLog> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.created_date = this.dateUtils
            .convertLocalDateFromServer(entity.created_date);
    }

    private convert(projLog: ProjLog): ProjLog {
        const copy: ProjLog = Object.assign({}, projLog);
        copy.created_date = this.dateUtils
            .convertLocalDateToServer(projLog.created_date);
        return copy;
    }
}
