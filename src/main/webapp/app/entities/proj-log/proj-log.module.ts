import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { QdestroyerSharedModule } from '../../shared';
import {
    ProjLogService,
    ProjLogPopupService,
    ProjLogComponent,
    ProjLogDetailComponent,
    ProjLogDialogComponent,
    ProjLogPopupComponent,
    ProjLogDeletePopupComponent,
    ProjLogDeleteDialogComponent,
    projLogRoute,
    projLogPopupRoute,
} from './';

const ENTITY_STATES = [
    ...projLogRoute,
    ...projLogPopupRoute,
];

@NgModule({
    imports: [
        QdestroyerSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ProjLogComponent,
        ProjLogDetailComponent,
        ProjLogDialogComponent,
        ProjLogDeleteDialogComponent,
        ProjLogPopupComponent,
        ProjLogDeletePopupComponent,
    ],
    entryComponents: [
        ProjLogComponent,
        ProjLogDialogComponent,
        ProjLogPopupComponent,
        ProjLogDeleteDialogComponent,
        ProjLogDeletePopupComponent,
    ],
    providers: [
        ProjLogService,
        ProjLogPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QdestroyerProjLogModule {}
