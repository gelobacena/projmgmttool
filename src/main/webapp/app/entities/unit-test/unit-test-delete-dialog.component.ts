import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { UnitTest } from './unit-test.model';
import { UnitTestPopupService } from './unit-test-popup.service';
import { UnitTestService } from './unit-test.service';

@Component({
    selector: 'jhi-unit-test-delete-dialog',
    templateUrl: './unit-test-delete-dialog.component.html'
})
export class UnitTestDeleteDialogComponent {

    unitTest: UnitTest;

    constructor(
        private unitTestService: UnitTestService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.unitTestService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'unitTestListModification',
                content: 'Deleted an unitTest'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-unit-test-delete-popup',
    template: ''
})
export class UnitTestDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private unitTestPopupService: UnitTestPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.unitTestPopupService
                .open(UnitTestDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
