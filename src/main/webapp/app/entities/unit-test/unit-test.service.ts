import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { UnitTest } from './unit-test.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class UnitTestService {

    private resourceUrl = 'api/unit-tests';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(unitTest: UnitTest): Observable<UnitTest> {
        const copy = this.convert(unitTest);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(unitTest: UnitTest): Observable<UnitTest> {
        const copy = this.convert(unitTest);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<UnitTest> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    findHistory(id: number) {
        return this.http.get(`${this.resourceUrl}/${id}/history`).map((res: Response) => res.json());
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.execution_date = this.dateUtils
            .convertLocalDateFromServer(entity.execution_date);
        entity.created_date = this.dateUtils
            .convertLocalDateFromServer(entity.created_date);
        entity.modified_date = this.dateUtils
            .convertLocalDateFromServer(entity.modified_date);
    }

    private convert(unitTest: UnitTest): UnitTest {
        const copy: UnitTest = Object.assign({}, unitTest);
        copy.execution_date = this.dateUtils
            .convertLocalDateToServer(unitTest.execution_date);
        copy.created_date = this.dateUtils
            .convertLocalDateToServer(unitTest.created_date);
        copy.modified_date = this.dateUtils
            .convertLocalDateToServer(unitTest.modified_date);
        return copy;
    }
}
