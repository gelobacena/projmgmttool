import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { UnitTest } from './unit-test.model';
import { UnitTestPopupService } from './unit-test-popup.service';
import { UnitTestService } from './unit-test.service';
import { Project, ProjectService } from '../project';
import { Comp, CompService } from '../comp';
import { Principal, ResponseWrapper, SharedStorageService } from '../../shared';

@Component({
    selector: 'jhi-unit-test-dialog',
    templateUrl: './unit-test-dialog.component.html'
})
export class UnitTestDialogComponent implements OnInit {

    unitTest: UnitTest;
    authorities: any[];
    isSaving: boolean;
    user: any;
    isEdit: boolean;
    projComps: any;

    projects: Project[];

    comps: Comp[];
    execution_dateDp: any;
    created_dateDp: any;
    modified_dateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private principal: Principal,
        private unitTestService: UnitTestService,
        private projectService: ProjectService,
        private compService: CompService,
        private sharedStorageService: SharedStorageService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        const currDate = new Date();
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        if (this.unitTest.id !== undefined) {
            this.isEdit = true;
        }
        this.user = this.principal.identity().then((account) => {
            if (this.isEdit) {
                this.unitTest.modified_by = account.login;
                this.unitTest.modified_date = {
                    year: currDate.getFullYear(),
                    month: currDate.getMonth() + 1,
                    day: currDate.getDate()
                };
            } else {
                this.unitTest.created_by = account.login;
                this.unitTest.created_date = {
                    year: currDate.getFullYear(),
                    month: currDate.getMonth() + 1,
                    day: currDate.getDate()
                };
            }
        });
        if (this.unitTest.is_deleted === undefined) {
            this.unitTest.is_deleted = 0;
            this.unitTest.execution_status = 'New';
        }
        if (this.sharedStorageService.getCurrentProject()) {
            this.unitTest.project = this.sharedStorageService.getCurrentProject();
        }
        if (this.sharedStorageService.getCurrentComp()) {
            const storedComp = this.sharedStorageService.getCurrentComp();
            this.unitTest.comp = storedComp;
            if (this.unitTest.project === undefined) {
                this.unitTest.project = storedComp.project;
            }
        }
        this.projectService.query()
            .subscribe((res: ResponseWrapper) => { this.projects = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.compService.query()
            .subscribe((res: ResponseWrapper) => { this.comps = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        if (this.unitTest.project !== undefined) {
            this.projectService.findComponents(this.unitTest.project.id).subscribe((data) => {
                this.projComps = data;
            });
        }
        this.projects = this.projects.filter((p) => p.is_deleted !== 1);
        this.comps = this.comps.filter((p) => p.is_deleted !== 1);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.unitTest.id !== undefined) {
            this.subscribeToSaveResponse(
                this.unitTestService.update(this.unitTest));
        } else {
            this.subscribeToSaveResponse(
                this.unitTestService.create(this.unitTest));
        }
    }

    private subscribeToSaveResponse(result: Observable<UnitTest>) {
        result.subscribe((res: UnitTest) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: UnitTest) {
        this.eventManager.broadcast({ name: 'unitTestListModification', content: 'OK'});
        this.eventManager.broadcast({ name: 'projectListModification', content: 'OK'});
        this.eventManager.broadcast({ name: 'compListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
        this.sharedStorageService.clearStorage();
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackProjectById(index: number, item: Project) {
        return item.id;
    }

    trackCompById(index: number, item: Comp) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-unit-test-popup',
    template: ''
})
export class UnitTestPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private unitTestPopupService: UnitTestPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.unitTestPopupService
                    .open(UnitTestDialogComponent, params['id']);
            } else {
                this.modalRef = this.unitTestPopupService
                    .open(UnitTestDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
