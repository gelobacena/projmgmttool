import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiAlertService } from 'ng-jhipster';

import { UnitTest } from './unit-test.model';
import { UnitTestService } from './unit-test.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

@Component({
    selector: 'jhi-unit-test',
    templateUrl: './unit-test.component.html'
})
export class UnitTestComponent implements OnInit, OnDestroy {
unitTests: UnitTest[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private unitTestService: UnitTestService,
        private alertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.unitTestService.query().subscribe(
            (res: ResponseWrapper) => {
                this.unitTests = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInUnitTests();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: UnitTest) {
        return item.id;
    }
    registerChangeInUnitTests() {
        this.eventSubscriber = this.eventManager.subscribe('unitTestListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
