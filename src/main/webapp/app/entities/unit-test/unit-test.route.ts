import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UnitTestComponent } from './unit-test.component';
import { UnitTestDetailComponent } from './unit-test-detail.component';
import { UnitTestPopupComponent } from './unit-test-dialog.component';
import { UnitTestDeletePopupComponent } from './unit-test-delete-dialog.component';

import { Principal } from '../../shared';

export const unitTestRoute: Routes = [
    {
        path: 'unit-test',
        component: UnitTestComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'UnitTests'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'unit-test/:id',
        component: UnitTestDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'UnitTests'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const unitTestPopupRoute: Routes = [
    {
        path: 'unit-test-new',
        component: UnitTestPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'UnitTests'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'unit-test/:id/edit',
        component: UnitTestPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'UnitTests'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'unit-test/:id/delete',
        component: UnitTestDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'UnitTests'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
