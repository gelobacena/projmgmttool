import { BaseEntity } from './../../shared';

export class UnitTest implements BaseEntity {
    constructor(
        public id?: number,
        public test_scenario?: string,
        public test_condition?: string,
        public actions?: string,
        public input_data?: string,
        public expected_result?: string,
        public execution_status?: string,
        public execution_date?: any,
        public evidences?: string,
        public scenario_id?: number,
        public condition_id?: number,
        public is_deleted?: number,
        public created_by?: string,
        public created_date?: any,
        public modified_by?: string,
        public modified_date?: any,
        public project?: BaseEntity,
        public comp?: BaseEntity,
    ) {
    }
}
