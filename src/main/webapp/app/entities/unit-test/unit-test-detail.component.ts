import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { UnitTest } from './unit-test.model';
import { UnitTestService } from './unit-test.service';

@Component({
    selector: 'jhi-unit-test-detail',
    templateUrl: './unit-test-detail.component.html'
})
export class UnitTestDetailComponent implements OnInit, OnDestroy {

    unitTest: UnitTest;
    private subscription: Subscription;
    private eventSubscriber: Subscription;
    public history: any;

    constructor(
        private eventManager: JhiEventManager,
        private unitTestService: UnitTestService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInUnitTests();
    }

    load(id) {
        this.unitTestService.find(id).subscribe((unitTest) => {
            this.unitTest = unitTest;
        });
        this.loadHistory(id);
    }

    loadHistory(id) {
        this.unitTestService.findHistory(id).subscribe((data) => {
            this.history = data;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInUnitTests() {
        this.eventSubscriber = this.eventManager.subscribe(
            'unitTestListModification',
            (response) => this.load(this.unitTest.id)
        );
    }
}
