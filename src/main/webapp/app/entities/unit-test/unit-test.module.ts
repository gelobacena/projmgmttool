import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { QdestroyerSharedModule } from '../../shared';
import {
    UnitTestService,
    UnitTestPopupService,
    UnitTestComponent,
    UnitTestDetailComponent,
    UnitTestDialogComponent,
    UnitTestPopupComponent,
    UnitTestDeletePopupComponent,
    UnitTestDeleteDialogComponent,
    unitTestRoute,
    unitTestPopupRoute,
} from './';

const ENTITY_STATES = [
    ...unitTestRoute,
    ...unitTestPopupRoute,
];

@NgModule({
    imports: [
        QdestroyerSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        UnitTestComponent,
        UnitTestDetailComponent,
        UnitTestDialogComponent,
        UnitTestDeleteDialogComponent,
        UnitTestPopupComponent,
        UnitTestDeletePopupComponent,
    ],
    entryComponents: [
        UnitTestComponent,
        UnitTestDialogComponent,
        UnitTestPopupComponent,
        UnitTestDeleteDialogComponent,
        UnitTestDeletePopupComponent,
    ],
    providers: [
        UnitTestService,
        UnitTestPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QdestroyerUnitTestModule {}
