import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { UnitTest } from './unit-test.model';
import { UnitTestService } from './unit-test.service';

@Injectable()
export class UnitTestPopupService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
        private router: Router,
        private unitTestService: UnitTestService

    ) {}

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.unitTestService.find(id).subscribe((unitTest) => {
                if (unitTest.execution_date) {
                    unitTest.execution_date = {
                        year: unitTest.execution_date.getFullYear(),
                        month: unitTest.execution_date.getMonth() + 1,
                        day: unitTest.execution_date.getDate()
                    };
                }
                if (unitTest.created_date) {
                    unitTest.created_date = {
                        year: unitTest.created_date.getFullYear(),
                        month: unitTest.created_date.getMonth() + 1,
                        day: unitTest.created_date.getDate()
                    };
                }
                if (unitTest.modified_date) {
                    unitTest.modified_date = {
                        year: unitTest.modified_date.getFullYear(),
                        month: unitTest.modified_date.getMonth() + 1,
                        day: unitTest.modified_date.getDate()
                    };
                }
                this.unitTestModalRef(component, unitTest);
            });
        } else {
            return this.unitTestModalRef(component, new UnitTest());
        }
    }

    unitTestModalRef(component: Component, unitTest: UnitTest): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.unitTest = unitTest;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
