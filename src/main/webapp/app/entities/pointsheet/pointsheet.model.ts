import { BaseEntity } from './../../shared';

export class Pointsheet implements BaseEntity {
    constructor(
        public id?: number,
        public pointsheet_type?: string,
        public inspection_point?: string,
        public subprocess_defect_origin?: string,
        public defect_type?: string,
        public severity?: string,
        public action_taken?: string,
        public completion_date?: any,
        public reopen_date?: any,
        public status?: string,
        public possible_root_cause?: string,
        public suggestion_for_avoidance?: string,
        public is_deleted?: number,
        public created_by?: string,
        public created_date?: any,
        public modified_by?: string,
        public modified_date?: any,
        public project?: BaseEntity,
        public comp?: BaseEntity,
    ) {
    }
}
