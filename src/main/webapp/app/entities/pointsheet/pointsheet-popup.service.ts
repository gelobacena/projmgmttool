import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Pointsheet } from './pointsheet.model';
import { PointsheetService } from './pointsheet.service';

@Injectable()
export class PointsheetPopupService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
        private router: Router,
        private pointsheetService: PointsheetService

    ) {}

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.pointsheetService.find(id).subscribe((pointsheet) => {
                if (pointsheet.completion_date) {
                    pointsheet.completion_date = {
                        year: pointsheet.completion_date.getFullYear(),
                        month: pointsheet.completion_date.getMonth() + 1,
                        day: pointsheet.completion_date.getDate()
                    };
                }
                if (pointsheet.reopen_date) {
                    pointsheet.reopen_date = {
                        year: pointsheet.reopen_date.getFullYear(),
                        month: pointsheet.reopen_date.getMonth() + 1,
                        day: pointsheet.reopen_date.getDate()
                    };
                }
                if (pointsheet.created_date) {
                    pointsheet.created_date = {
                        year: pointsheet.created_date.getFullYear(),
                        month: pointsheet.created_date.getMonth() + 1,
                        day: pointsheet.created_date.getDate()
                    };
                }
                if (pointsheet.modified_date) {
                    pointsheet.modified_date = {
                        year: pointsheet.modified_date.getFullYear(),
                        month: pointsheet.modified_date.getMonth() + 1,
                        day: pointsheet.modified_date.getDate()
                    };
                }
                this.pointsheetModalRef(component, pointsheet);
            });
        } else {
            return this.pointsheetModalRef(component, new Pointsheet());
        }
    }

    pointsheetModalRef(component: Component, pointsheet: Pointsheet): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.pointsheet = pointsheet;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
