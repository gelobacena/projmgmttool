import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Pointsheet } from './pointsheet.model';
import { PointsheetPopupService } from './pointsheet-popup.service';
import { PointsheetService } from './pointsheet.service';

@Component({
    selector: 'jhi-pointsheet-delete-dialog',
    templateUrl: './pointsheet-delete-dialog.component.html'
})
export class PointsheetDeleteDialogComponent {

    pointsheet: Pointsheet;

    constructor(
        private pointsheetService: PointsheetService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.pointsheetService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'pointsheetListModification',
                content: 'Deleted an pointsheet'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-pointsheet-delete-popup',
    template: ''
})
export class PointsheetDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private pointsheetPopupService: PointsheetPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.pointsheetPopupService
                .open(PointsheetDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
