import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { Pointsheet } from './pointsheet.model';
import { PointsheetService } from './pointsheet.service';

@Component({
    selector: 'jhi-pointsheet-detail',
    templateUrl: './pointsheet-detail.component.html'
})
export class PointsheetDetailComponent implements OnInit, OnDestroy {

    pointsheet: Pointsheet;
    private subscription: Subscription;
    private eventSubscriber: Subscription;
    public history: any;

    constructor(
        private eventManager: JhiEventManager,
        private pointsheetService: PointsheetService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPointsheets();
    }

    load(id) {
        this.pointsheetService.find(id).subscribe((pointsheet) => {
            this.pointsheet = pointsheet;
        });
        this.loadHistory(id);
    }

    loadHistory(id) {
        this.pointsheetService.findHistory(id).subscribe((data) => {
            this.history = data;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPointsheets() {
        this.eventSubscriber = this.eventManager.subscribe(
            'pointsheetListModification',
            (response) => this.load(this.pointsheet.id)
        );
    }
}
