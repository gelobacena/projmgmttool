import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PointsheetComponent } from './pointsheet.component';
import { PointsheetDetailComponent } from './pointsheet-detail.component';
import { PointsheetPopupComponent } from './pointsheet-dialog.component';
import { PointsheetDeletePopupComponent } from './pointsheet-delete-dialog.component';

import { Principal } from '../../shared';

export const pointsheetRoute: Routes = [
    {
        path: 'pointsheet',
        component: PointsheetComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Pointsheets'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'pointsheet/:id',
        component: PointsheetDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Pointsheets'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const pointsheetPopupRoute: Routes = [
    {
        path: 'pointsheet-new',
        component: PointsheetPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Pointsheets'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'pointsheet/:id/edit',
        component: PointsheetPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Pointsheets'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'pointsheet/:id/delete',
        component: PointsheetDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Pointsheets'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
