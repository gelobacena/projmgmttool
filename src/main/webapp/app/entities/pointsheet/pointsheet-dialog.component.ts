import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Pointsheet } from './pointsheet.model';
import { PointsheetPopupService } from './pointsheet-popup.service';
import { PointsheetService } from './pointsheet.service';
import { Project, ProjectService } from '../project';
import { Comp, CompService } from '../comp';
import { Principal, ResponseWrapper, SharedStorageService } from '../../shared';

@Component({
    selector: 'jhi-pointsheet-dialog',
    templateUrl: './pointsheet-dialog.component.html'
})
export class PointsheetDialogComponent implements OnInit {

    pointsheet: Pointsheet;
    authorities: any[];
    isSaving: boolean;
    user: any;
    isEdit: boolean;
    projComps: any;

    projects: Project[];

    comps: Comp[];
    completion_dateDp: any;
    reopen_dateDp: any;
    created_dateDp: any;
    modified_dateDp: any;
    defects: any[];
    prcs: any[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private pointsheetService: PointsheetService,
        private principal: Principal,
        private projectService: ProjectService,
        private compService: CompService,
        private sharedStorageService: SharedStorageService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        const currDate = new Date();
        this.defects = ['Not a Defect', 'Incorrect Requirement', 'Inconsistent Requirements',
        'Invalid, Incorrect, or Incomplete Test Case', 'Standards - Defect', 'Logic Defect', 'Traceability Defect',
        'Testability Defect', 'Interface Defects', 'User Interface Defects'];
        this.prcs = ['Transcription', 'Education', 'Communication', 'Oversight', 'Process - No Process',
        'Process - Wrong Process', 'Process - Incorrectly Applied'];
        if (this.pointsheet.id !== undefined) {
            this.isEdit = true;
        } else {
            this.pointsheet.status = 'New';
            this.pointsheet.subprocess_defect_origin = 'Build Application Components/Code';
            this.pointsheet.is_deleted = 0;
        }
        this.user = this.principal.identity().then((account) => {
            if (this.isEdit) {
                this.pointsheet.modified_by = account.login;
                this.pointsheet.modified_date = {
                    year: currDate.getFullYear(),
                    month: currDate.getMonth() + 1,
                    day: currDate.getDate()
                };
            } else {
                this.pointsheet.created_by = account.login;
                this.pointsheet.created_date = {
                    year: currDate.getFullYear(),
                    month: currDate.getMonth() + 1,
                    day: currDate.getDate()
                };
            }
        });
        if (this.sharedStorageService.getCurrentProject() !== null) {
            this.pointsheet.project = this.sharedStorageService.getCurrentProject();
        }
        if (this.sharedStorageService.getCurrentComp() != null) {
            const storedComp = this.sharedStorageService.getCurrentComp();
            this.pointsheet.comp = storedComp;
            this.pointsheet.project = storedComp.project;
        }
        this.projectService.query()
            .subscribe((res: ResponseWrapper) => { this.projects = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.compService.query()
            .subscribe((res: ResponseWrapper) => { this.comps = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        if (this.pointsheet.project !== undefined) {
            this.projectService.findComponents(this.pointsheet.project.id).subscribe((data) => {
                this.projComps = data;
            });
        }
        this.projects = this.projects.filter((p) => p.is_deleted !== 1);
        this.comps = this.comps.filter((p) => p.is_deleted !== 1);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.pointsheet.id !== undefined) {
            this.subscribeToSaveResponse(
                this.pointsheetService.update(this.pointsheet));
        } else {
            this.subscribeToSaveResponse(
                this.pointsheetService.create(this.pointsheet));
        }
    }

    private subscribeToSaveResponse(result: Observable<Pointsheet>) {
        result.subscribe((res: Pointsheet) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: Pointsheet) {
        this.eventManager.broadcast({ name: 'pointsheetListModification', content: 'OK'});
        this.eventManager.broadcast({ name: 'projectListModification', content: 'OK'});
        this.eventManager.broadcast({ name: 'compListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackProjectById(index: number, item: Project) {
        return item.id;
    }

    trackCompById(index: number, item: Comp) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-pointsheet-popup',
    template: ''
})
export class PointsheetPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private pointsheetPopupService: PointsheetPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.pointsheetPopupService
                    .open(PointsheetDialogComponent, params['id']);
            } else {
                this.modalRef = this.pointsheetPopupService
                    .open(PointsheetDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
