import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { Pointsheet } from './pointsheet.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PointsheetService {

    private resourceUrl = 'api/pointsheets';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(pointsheet: Pointsheet): Observable<Pointsheet> {
        const copy = this.convert(pointsheet);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(pointsheet: Pointsheet): Observable<Pointsheet> {
        const copy = this.convert(pointsheet);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<Pointsheet> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    findHistory(id: number) {
        return this.http.get(`${this.resourceUrl}/${id}/history`).map((res: Response) => res.json());
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.completion_date = this.dateUtils
            .convertLocalDateFromServer(entity.completion_date);
        entity.reopen_date = this.dateUtils
            .convertLocalDateFromServer(entity.reopen_date);
        entity.created_date = this.dateUtils
            .convertLocalDateFromServer(entity.created_date);
        entity.modified_date = this.dateUtils
            .convertLocalDateFromServer(entity.modified_date);
    }

    private convert(pointsheet: Pointsheet): Pointsheet {
        const copy: Pointsheet = Object.assign({}, pointsheet);
        copy.completion_date = this.dateUtils
            .convertLocalDateToServer(pointsheet.completion_date);
        copy.reopen_date = this.dateUtils
            .convertLocalDateToServer(pointsheet.reopen_date);
        copy.created_date = this.dateUtils
            .convertLocalDateToServer(pointsheet.created_date);
        copy.modified_date = this.dateUtils
            .convertLocalDateToServer(pointsheet.modified_date);
        return copy;
    }
}
