import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { QdestroyerSharedModule } from '../../shared';
import {
    PointsheetService,
    PointsheetPopupService,
    PointsheetComponent,
    PointsheetDetailComponent,
    PointsheetDialogComponent,
    PointsheetPopupComponent,
    PointsheetDeletePopupComponent,
    PointsheetDeleteDialogComponent,
    pointsheetRoute,
    pointsheetPopupRoute,
} from './';

const ENTITY_STATES = [
    ...pointsheetRoute,
    ...pointsheetPopupRoute,
];

@NgModule({
    imports: [
        QdestroyerSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        PointsheetComponent,
        PointsheetDetailComponent,
        PointsheetDialogComponent,
        PointsheetDeleteDialogComponent,
        PointsheetPopupComponent,
        PointsheetDeletePopupComponent,
    ],
    entryComponents: [
        PointsheetComponent,
        PointsheetDialogComponent,
        PointsheetPopupComponent,
        PointsheetDeleteDialogComponent,
        PointsheetDeletePopupComponent,
    ],
    providers: [
        PointsheetService,
        PointsheetPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QdestroyerPointsheetModule {}
