import { BaseEntity } from './../../shared';

export class RiskAndIssues implements BaseEntity {
    constructor(
        public id?: number,
        public rec_type?: string,
        public description?: string,
        public source?: string,
        public priority?: string,
        public status?: string,
        public severity?: string,
        public probability_percent?: string,
        public mitigation_plan?: string,
        public mitigation_due_date?: any,
        public contingency_plan?: string,
        public contingency_due_date?: any,
        public remarks?: string,
        public created_by?: string,
        public created_date?: any,
        public modified_by?: string,
        public modified_date?: any,
        public is_deleted?: number,
        public resp?: string,
        public date_occured?: any,
        public impact?: string,
        public project?: BaseEntity,
    ) {
    }
}
