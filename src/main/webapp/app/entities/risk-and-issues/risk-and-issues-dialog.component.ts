import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { RiskAndIssues } from './risk-and-issues.model';
import { RiskAndIssuesPopupService } from './risk-and-issues-popup.service';
import { RiskAndIssuesService } from './risk-and-issues.service';
import { Project, ProjectService } from '../project';
import { Principal, ResponseWrapper, SharedStorageService } from '../../shared';

@Component({
    selector: 'jhi-risk-and-issues-dialog',
    templateUrl: './risk-and-issues-dialog.component.html'
})
export class RiskAndIssuesDialogComponent implements OnInit {

    riskAndIssues: RiskAndIssues;
    authorities: any[];
    isSaving: boolean;
    isEdit: boolean;
    sources: any;
    probPercent: any;
    priorities: any;
    severities: any;
    statuses: any;
    responses: any;
    user: any;

    projects: Project[];
    mitigation_due_dateDp: any;
    contingency_due_dateDp: any;
    created_dateDp: any;
    modified_dateDp: any;
    date_occuredDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private principal: Principal,
        private alertService: JhiAlertService,
        private riskAndIssuesService: RiskAndIssuesService,
        private sharedStorageService: SharedStorageService,
        private projectService: ProjectService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        const currDate = new Date();
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.sources = ['Cost', 'Schedule', 'Technology', 'Operational', 'External', 'Quality', 'Staffing',
        'Physical Resource', 'Unstable Process', 'Vendor', 'Training'];
        this.probPercent = ['0%', '20%', '40%', '60%', '80%', '100%'];
        this.priorities = ['Low', 'Medium', 'High'];
        this.severities = ['1', '2', '3', '4', '5'];
        this.statuses = ['New', 'Action Taken', 'Closed'];
        this.responses = ['Accept', 'Avoid', 'Control', 'Transfer', 'Investigate'];
        if (this.riskAndIssues.id !== undefined) {
            this.isEdit = true;
        }
        if (this.sharedStorageService.getCurrentProject()) {
            this.riskAndIssues.project = this.sharedStorageService.getCurrentProject();
        }
        if (this.riskAndIssues.id === undefined) {
            this.riskAndIssues.status = 'New';
            this.riskAndIssues.rec_type = 'Risk';
        }
        this.user = this.principal.identity().then((account) => {
            if (this.isEdit) {
                this.riskAndIssues.modified_by = account.login;
                this.riskAndIssues.modified_date = {
                    year: currDate.getFullYear(),
                    month: currDate.getMonth() + 1,
                    day: currDate.getDate()
                };
            } else {
                this.riskAndIssues.created_by = account.login;
                this.riskAndIssues.created_date = {
                    year: currDate.getFullYear(),
                    month: currDate.getMonth() + 1,
                    day: currDate.getDate()
                };
            }
        });
        this.projectService.query()
            .subscribe((res: ResponseWrapper) => { this.projects = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.projects = this.projects.filter((p) => p.is_deleted !== 1);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.riskAndIssues.id !== undefined) {
            this.subscribeToSaveResponse(
                this.riskAndIssuesService.update(this.riskAndIssues));
        } else {
            this.subscribeToSaveResponse(
                this.riskAndIssuesService.create(this.riskAndIssues));
        }
    }

    private subscribeToSaveResponse(result: Observable<RiskAndIssues>) {
        result.subscribe((res: RiskAndIssues) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: RiskAndIssues) {
        this.eventManager.broadcast({ name: 'riskAndIssuesListModification', content: 'OK'});
        this.eventManager.broadcast({ name: 'projectListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackProjectById(index: number, item: Project) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-risk-and-issues-popup',
    template: ''
})
export class RiskAndIssuesPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private riskAndIssuesPopupService: RiskAndIssuesPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.riskAndIssuesPopupService
                    .open(RiskAndIssuesDialogComponent, params['id']);
            } else {
                this.modalRef = this.riskAndIssuesPopupService
                    .open(RiskAndIssuesDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
