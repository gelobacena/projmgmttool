import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { QdestroyerSharedModule } from '../../shared';
import {
    RiskAndIssuesService,
    RiskAndIssuesPopupService,
    RiskAndIssuesComponent,
    RiskAndIssuesDetailComponent,
    RiskAndIssuesDialogComponent,
    RiskAndIssuesPopupComponent,
    RiskAndIssuesDeletePopupComponent,
    RiskAndIssuesDeleteDialogComponent,
    riskAndIssuesRoute,
    riskAndIssuesPopupRoute,
} from './';

const ENTITY_STATES = [
    ...riskAndIssuesRoute,
    ...riskAndIssuesPopupRoute,
];

@NgModule({
    imports: [
        QdestroyerSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        RiskAndIssuesComponent,
        RiskAndIssuesDetailComponent,
        RiskAndIssuesDialogComponent,
        RiskAndIssuesDeleteDialogComponent,
        RiskAndIssuesPopupComponent,
        RiskAndIssuesDeletePopupComponent,
    ],
    entryComponents: [
        RiskAndIssuesComponent,
        RiskAndIssuesDialogComponent,
        RiskAndIssuesPopupComponent,
        RiskAndIssuesDeleteDialogComponent,
        RiskAndIssuesDeletePopupComponent,
    ],
    providers: [
        RiskAndIssuesService,
        RiskAndIssuesPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QdestroyerRiskAndIssuesModule {}
