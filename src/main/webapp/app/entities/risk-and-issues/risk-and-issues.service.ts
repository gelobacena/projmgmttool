import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { RiskAndIssues } from './risk-and-issues.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class RiskAndIssuesService {

    private resourceUrl = 'api/risk-and-issues';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(riskAndIssues: RiskAndIssues): Observable<RiskAndIssues> {
        const copy = this.convert(riskAndIssues);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(riskAndIssues: RiskAndIssues): Observable<RiskAndIssues> {
        const copy = this.convert(riskAndIssues);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<RiskAndIssues> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    findHistory(id: number) {
        return this.http.get(`${this.resourceUrl}/${id}/history`).map((res: Response) => res.json());
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.mitigation_due_date = this.dateUtils
            .convertLocalDateFromServer(entity.mitigation_due_date);
        entity.contingency_due_date = this.dateUtils
            .convertLocalDateFromServer(entity.contingency_due_date);
        entity.created_date = this.dateUtils
            .convertLocalDateFromServer(entity.created_date);
        entity.modified_date = this.dateUtils
            .convertLocalDateFromServer(entity.modified_date);
        entity.date_occured = this.dateUtils
            .convertLocalDateFromServer(entity.date_occured);
    }

    private convert(riskAndIssues: RiskAndIssues): RiskAndIssues {
        const copy: RiskAndIssues = Object.assign({}, riskAndIssues);
        copy.mitigation_due_date = this.dateUtils
            .convertLocalDateToServer(riskAndIssues.mitigation_due_date);
        copy.contingency_due_date = this.dateUtils
            .convertLocalDateToServer(riskAndIssues.contingency_due_date);
        copy.created_date = this.dateUtils
            .convertLocalDateToServer(riskAndIssues.created_date);
        copy.modified_date = this.dateUtils
            .convertLocalDateToServer(riskAndIssues.modified_date);
        copy.date_occured = this.dateUtils
            .convertLocalDateToServer(riskAndIssues.date_occured);
        return copy;
    }
}
