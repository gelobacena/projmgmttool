import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { RiskAndIssuesComponent } from './risk-and-issues.component';
import { RiskAndIssuesDetailComponent } from './risk-and-issues-detail.component';
import { RiskAndIssuesPopupComponent } from './risk-and-issues-dialog.component';
import { RiskAndIssuesDeletePopupComponent } from './risk-and-issues-delete-dialog.component';

import { Principal } from '../../shared';

export const riskAndIssuesRoute: Routes = [
    {
        path: 'risk-and-issues',
        component: RiskAndIssuesComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'RiskAndIssues'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'risk-and-issues/:id',
        component: RiskAndIssuesDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'RiskAndIssues'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const riskAndIssuesPopupRoute: Routes = [
    {
        path: 'risk-and-issues-new',
        component: RiskAndIssuesPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'RiskAndIssues'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'risk-and-issues/:id/edit',
        component: RiskAndIssuesPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'RiskAndIssues'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'risk-and-issues/:id/delete',
        component: RiskAndIssuesDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'RiskAndIssues'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
