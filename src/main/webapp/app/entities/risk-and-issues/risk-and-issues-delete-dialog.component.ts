import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { RiskAndIssues } from './risk-and-issues.model';
import { RiskAndIssuesPopupService } from './risk-and-issues-popup.service';
import { RiskAndIssuesService } from './risk-and-issues.service';

@Component({
    selector: 'jhi-risk-and-issues-delete-dialog',
    templateUrl: './risk-and-issues-delete-dialog.component.html'
})
export class RiskAndIssuesDeleteDialogComponent {

    riskAndIssues: RiskAndIssues;

    constructor(
        private riskAndIssuesService: RiskAndIssuesService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.riskAndIssuesService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'riskAndIssuesListModification',
                content: 'Deleted an riskAndIssues'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-risk-and-issues-delete-popup',
    template: ''
})
export class RiskAndIssuesDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private riskAndIssuesPopupService: RiskAndIssuesPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.riskAndIssuesPopupService
                .open(RiskAndIssuesDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
