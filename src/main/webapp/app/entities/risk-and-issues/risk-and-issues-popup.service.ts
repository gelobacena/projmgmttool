import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RiskAndIssues } from './risk-and-issues.model';
import { RiskAndIssuesService } from './risk-and-issues.service';

@Injectable()
export class RiskAndIssuesPopupService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
        private router: Router,
        private riskAndIssuesService: RiskAndIssuesService

    ) {}

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.riskAndIssuesService.find(id).subscribe((riskAndIssues) => {
                if (riskAndIssues.mitigation_due_date) {
                    riskAndIssues.mitigation_due_date = {
                        year: riskAndIssues.mitigation_due_date.getFullYear(),
                        month: riskAndIssues.mitigation_due_date.getMonth() + 1,
                        day: riskAndIssues.mitigation_due_date.getDate()
                    };
                }
                if (riskAndIssues.contingency_due_date) {
                    riskAndIssues.contingency_due_date = {
                        year: riskAndIssues.contingency_due_date.getFullYear(),
                        month: riskAndIssues.contingency_due_date.getMonth() + 1,
                        day: riskAndIssues.contingency_due_date.getDate()
                    };
                }
                if (riskAndIssues.created_date) {
                    riskAndIssues.created_date = {
                        year: riskAndIssues.created_date.getFullYear(),
                        month: riskAndIssues.created_date.getMonth() + 1,
                        day: riskAndIssues.created_date.getDate()
                    };
                }
                if (riskAndIssues.modified_date) {
                    riskAndIssues.modified_date = {
                        year: riskAndIssues.modified_date.getFullYear(),
                        month: riskAndIssues.modified_date.getMonth() + 1,
                        day: riskAndIssues.modified_date.getDate()
                    };
                }
                if (riskAndIssues.date_occured) {
                    riskAndIssues.date_occured = {
                        year: riskAndIssues.date_occured.getFullYear(),
                        month: riskAndIssues.date_occured.getMonth() + 1,
                        day: riskAndIssues.date_occured.getDate()
                    };
                }
                this.riskAndIssuesModalRef(component, riskAndIssues);
            });
        } else {
            return this.riskAndIssuesModalRef(component, new RiskAndIssues());
        }
    }

    riskAndIssuesModalRef(component: Component, riskAndIssues: RiskAndIssues): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.riskAndIssues = riskAndIssues;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
