import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { RiskAndIssues } from './risk-and-issues.model';
import { RiskAndIssuesService } from './risk-and-issues.service';

@Component({
    selector: 'jhi-risk-and-issues-detail',
    templateUrl: './risk-and-issues-detail.component.html'
})
export class RiskAndIssuesDetailComponent implements OnInit, OnDestroy {

    riskAndIssues: RiskAndIssues;
    private subscription: Subscription;
    private eventSubscriber: Subscription;
    public history: any;

    constructor(
        private eventManager: JhiEventManager,
        private riskAndIssuesService: RiskAndIssuesService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInRiskAndIssues();
    }

    load(id) {
        this.riskAndIssuesService.find(id).subscribe((riskAndIssues) => {
            this.riskAndIssues = riskAndIssues;
        });
        this.loadHistory(id);
    }

    loadHistory(id) {
        this.riskAndIssuesService.findHistory(id).subscribe((data) => {
            this.history = data;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInRiskAndIssues() {
        this.eventSubscriber = this.eventManager.subscribe(
            'riskAndIssuesListModification',
            (response) => this.load(this.riskAndIssues.id)
        );
    }
}
