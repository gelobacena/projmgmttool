import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { QdestroyerSharedModule } from '../../shared';
import {
    ClarificationService,
    ClarificationPopupService,
    ClarificationComponent,
    ClarificationDetailComponent,
    ClarificationDialogComponent,
    ClarificationPopupComponent,
    ClarificationDeletePopupComponent,
    ClarificationDeleteDialogComponent,
    clarificationRoute,
    clarificationPopupRoute,
} from './';

const ENTITY_STATES = [
    ...clarificationRoute,
    ...clarificationPopupRoute,
];

@NgModule({
    imports: [
        QdestroyerSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ClarificationComponent,
        ClarificationDetailComponent,
        ClarificationDialogComponent,
        ClarificationDeleteDialogComponent,
        ClarificationPopupComponent,
        ClarificationDeletePopupComponent,
    ],
    entryComponents: [
        ClarificationComponent,
        ClarificationDialogComponent,
        ClarificationPopupComponent,
        ClarificationDeleteDialogComponent,
        ClarificationDeletePopupComponent,
    ],
    providers: [
        ClarificationService,
        ClarificationPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QdestroyerClarificationModule {}
