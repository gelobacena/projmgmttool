import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Clarification } from './clarification.model';
import { ClarificationPopupService } from './clarification-popup.service';
import { ClarificationService } from './clarification.service';

@Component({
    selector: 'jhi-clarification-delete-dialog',
    templateUrl: './clarification-delete-dialog.component.html'
})
export class ClarificationDeleteDialogComponent {

    clarification: Clarification;

    constructor(
        private clarificationService: ClarificationService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.clarificationService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'clarificationListModification',
                content: 'Deleted an clarification'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-clarification-delete-popup',
    template: ''
})
export class ClarificationDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clarificationPopupService: ClarificationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.clarificationPopupService
                .open(ClarificationDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
