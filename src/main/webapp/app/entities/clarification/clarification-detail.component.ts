import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { Clarification } from './clarification.model';
import { ClarificationService } from './clarification.service';

@Component({
    selector: 'jhi-clarification-detail',
    templateUrl: './clarification-detail.component.html'
})
export class ClarificationDetailComponent implements OnInit, OnDestroy {

    clarification: Clarification;
    private subscription: Subscription;
    private eventSubscriber: Subscription;
    public history: any;

    constructor(
        private eventManager: JhiEventManager,
        private clarificationService: ClarificationService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInClarifications();
    }

    load(id) {
        this.clarificationService.find(id).subscribe((clarification) => {
            this.clarification = clarification;
        });
        this.loadHistory(id);
    }

    loadHistory(id) {
        this.clarificationService.findHistory(id).subscribe((data) => {
            this.history = data;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInClarifications() {
        this.eventSubscriber = this.eventManager.subscribe(
            'clarificationListModification',
            (response) => this.load(this.clarification.id)
        );
    }
}
