import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Clarification } from './clarification.model';
import { ClarificationPopupService } from './clarification-popup.service';
import { ClarificationService } from './clarification.service';
import { Comp, CompService } from '../comp';
import { Project, ProjectService } from '../project';
import { Principal, ResponseWrapper, SharedStorageService } from '../../shared';

@Component({
    selector: 'jhi-clarification-dialog',
    templateUrl: './clarification-dialog.component.html'
})
export class ClarificationDialogComponent implements OnInit {

    clarification: Clarification;
    authorities: any[];
    isSaving: boolean;
    isEdit: boolean;
    user: any;
    priorities: any;
    statuses: any;
    classifications: any;

    comps: Comp[];

    projects: Project[];
    cl_dateDp: any;
    date_closedDp: any;
    created_dateDp: any;
    modified_dateDp: any;

    projComps: any;

    constructor(
        public activeModal: NgbActiveModal,
        private principal: Principal,
        private sharedStorageService: SharedStorageService,
        private alertService: JhiAlertService,
        private clarificationService: ClarificationService,
        private compService: CompService,
        private projectService: ProjectService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        const currDate = new Date();
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.priorities = ['Low', 'Medium', 'High'];
        this.statuses = ['New', 'Pending', 'Clarified', 'Re-Open', 'Closed'];
        this.classifications = ['Technical', 'Functional'];
        if (this.clarification.id !== undefined) {
            this.isEdit = true;
        }
        if (this.clarification.id === undefined) {
            this.clarification.status = 'New';
            this.clarification.is_deleted = 0;
        }
        this.user = this.principal.identity().then((account) => {
            if (this.isEdit) {
                this.clarification.modified_by = account.login;
                this.clarification.modified_date = {
                    year: currDate.getFullYear(),
                    month: currDate.getMonth() + 1,
                    day: currDate.getDate()
                };
            } else {
                this.clarification.created_by = account.login;
                this.clarification.created_date = {
                    year: currDate.getFullYear(),
                    month: currDate.getMonth() + 1,
                    day: currDate.getDate()
                };
            }
        });
        if (this.sharedStorageService.getCurrentProject()) {
            this.clarification.project = this.sharedStorageService.getCurrentProject();
        }
        if (this.sharedStorageService.getCurrentComp()) {
            const storedComp = this.sharedStorageService.getCurrentComp();
            this.clarification.comp = storedComp;
            if (this.clarification.project === undefined) {
                this.clarification.project = storedComp.project;
            }
        }
        this.compService.query()
            .subscribe((res: ResponseWrapper) => { this.comps = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.projectService.query()
            .subscribe((res: ResponseWrapper) => { this.projects = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        if (this.clarification.project !== undefined) {
            this.projectService.findComponents(this.clarification.project.id).subscribe((data) => {
                this.projComps = data;
            });
        }
        this.projects = this.projects.filter((p) => p.is_deleted !== 1);
        this.comps = this.comps.filter((c) => c.is_deleted !== 1);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.clarification.id !== undefined) {
            this.subscribeToSaveResponse(
                this.clarificationService.update(this.clarification));
        } else {
            this.subscribeToSaveResponse(
                this.clarificationService.create(this.clarification));
        }
    }

    private subscribeToSaveResponse(result: Observable<Clarification>) {
        result.subscribe((res: Clarification) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: Clarification) {
        this.eventManager.broadcast({ name: 'clarificationListModification', content: 'OK'});
        this.eventManager.broadcast({ name: 'projectListModification', content: 'OK'});
        this.eventManager.broadcast({ name: 'compListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackCompById(index: number, item: Comp) {
        return item.id;
    }

    trackProjectById(index: number, item: Project) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-clarification-popup',
    template: ''
})
export class ClarificationPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clarificationPopupService: ClarificationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.clarificationPopupService
                    .open(ClarificationDialogComponent, params['id']);
            } else {
                this.modalRef = this.clarificationPopupService
                    .open(ClarificationDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
