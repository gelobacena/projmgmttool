import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { Clarification } from './clarification.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ClarificationService {

    private resourceUrl = 'api/clarifications';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(clarification: Clarification): Observable<Clarification> {
        const copy = this.convert(clarification);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(clarification: Clarification): Observable<Clarification> {
        const copy = this.convert(clarification);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<Clarification> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    findHistory(id: number) {
        return this.http.get(`${this.resourceUrl}/${id}/history`).map((res: Response) => res.json());
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.cl_date = this.dateUtils
            .convertLocalDateFromServer(entity.cl_date);
        entity.date_closed = this.dateUtils
            .convertLocalDateFromServer(entity.date_closed);
        entity.created_date = this.dateUtils
            .convertLocalDateFromServer(entity.created_date);
        entity.modified_date = this.dateUtils
            .convertLocalDateFromServer(entity.modified_date);
    }

    private convert(clarification: Clarification): Clarification {
        const copy: Clarification = Object.assign({}, clarification);
        copy.cl_date = this.dateUtils
            .convertLocalDateToServer(clarification.cl_date);
        copy.date_closed = this.dateUtils
            .convertLocalDateToServer(clarification.date_closed);
        copy.created_date = this.dateUtils
            .convertLocalDateToServer(clarification.created_date);
        copy.modified_date = this.dateUtils
            .convertLocalDateToServer(clarification.modified_date);
        return copy;
    }
}
