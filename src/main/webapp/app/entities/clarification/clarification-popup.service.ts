import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Clarification } from './clarification.model';
import { ClarificationService } from './clarification.service';

@Injectable()
export class ClarificationPopupService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
        private router: Router,
        private clarificationService: ClarificationService

    ) {}

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.clarificationService.find(id).subscribe((clarification) => {
                if (clarification.cl_date) {
                    clarification.cl_date = {
                        year: clarification.cl_date.getFullYear(),
                        month: clarification.cl_date.getMonth() + 1,
                        day: clarification.cl_date.getDate()
                    };
                }
                if (clarification.date_closed) {
                    clarification.date_closed = {
                        year: clarification.date_closed.getFullYear(),
                        month: clarification.date_closed.getMonth() + 1,
                        day: clarification.date_closed.getDate()
                    };
                }
                if (clarification.created_date) {
                    clarification.created_date = {
                        year: clarification.created_date.getFullYear(),
                        month: clarification.created_date.getMonth() + 1,
                        day: clarification.created_date.getDate()
                    };
                }
                if (clarification.modified_date) {
                    clarification.modified_date = {
                        year: clarification.modified_date.getFullYear(),
                        month: clarification.modified_date.getMonth() + 1,
                        day: clarification.modified_date.getDate()
                    };
                }
                this.clarificationModalRef(component, clarification);
            });
        } else {
            return this.clarificationModalRef(component, new Clarification());
        }
    }

    clarificationModalRef(component: Component, clarification: Clarification): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.clarification = clarification;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
