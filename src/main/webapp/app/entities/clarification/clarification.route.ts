import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ClarificationComponent } from './clarification.component';
import { ClarificationDetailComponent } from './clarification-detail.component';
import { ClarificationPopupComponent } from './clarification-dialog.component';
import { ClarificationDeletePopupComponent } from './clarification-delete-dialog.component';

import { Principal } from '../../shared';

export const clarificationRoute: Routes = [
    {
        path: 'clarification',
        component: ClarificationComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Clarifications'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'clarification/:id',
        component: ClarificationDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Clarifications'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const clarificationPopupRoute: Routes = [
    {
        path: 'clarification-new',
        component: ClarificationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Clarifications'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'clarification/:id/edit',
        component: ClarificationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Clarifications'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'clarification/:id/delete',
        component: ClarificationDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Clarifications'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
