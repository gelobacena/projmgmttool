import { BaseEntity } from './../../shared';

export class Clarification implements BaseEntity {
    constructor(
        public id?: number,
        public cl_date?: any,
        public status?: string,
        public priority?: string,
        public classification?: string,
        public assigned_to?: string,
        public description?: string,
        public resolution?: string,
        public remarks?: string,
        public date_closed?: any,
        public created_by?: string,
        public created_date?: any,
        public modified_by?: string,
        public modified_date?: any,
        public is_deleted?: number,
        public comp?: BaseEntity,
        public project?: BaseEntity,
    ) {
    }
}
