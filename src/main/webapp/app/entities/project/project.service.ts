import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { Project } from './project.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ProjectService {

    private resourceUrl = 'api/projects';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(project: Project): Observable<Project> {
        const copy = this.convert(project);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(project: Project): Observable<Project> {
        const copy = this.convert(project);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<Project> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    findComponents(id: number) {
        return this.http.get(`${this.resourceUrl}/${id}/comps`).map((res: Response) => res.json());
    }

    findTests(id: number) {
        return this.http.get(`${this.resourceUrl}/${id}/tests`).map((res: Response) => res.json());
    }

    findReviewPoints(id: number) {
        return this.http.get(`${this.resourceUrl}/${id}/reviewpoints`).map((res: Response) => res.json());
    }

    findRiskAndIssues(id: number) {
        return this.http.get(`${this.resourceUrl}/${id}/riskandissues`).map((res: Response) => res.json());
    }

    findClarifications(id: number) {
        return this.http.get(`${this.resourceUrl}/${id}/clarifications`).map((res: Response) => res.json());
    }

    findHistory(id: number) {
        return this.http.get(`${this.resourceUrl}/${id}/history`).map((res: Response) => res.json());
    }

    findAll() {
        return this.http.get(`${this.resourceUrl}/all`)
            .map((res: Response) => this.convertResponse(res));
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.planned_start = this.dateUtils
            .convertLocalDateFromServer(entity.planned_start);
        entity.planned_finish = this.dateUtils
            .convertLocalDateFromServer(entity.planned_finish);
        entity.actual_start = this.dateUtils
            .convertLocalDateFromServer(entity.actual_start);
        entity.actual_finish = this.dateUtils
            .convertLocalDateFromServer(entity.actual_finish);
        entity.created_date = this.dateUtils
            .convertLocalDateFromServer(entity.created_date);
        entity.modified_date = this.dateUtils
            .convertLocalDateFromServer(entity.modified_date);
        entity.td_date = this.dateUtils
            .convertLocalDateFromServer(entity.td_date);
    }

    private convert(project: Project): Project {
        const copy: Project = Object.assign({}, project);
        copy.planned_start = this.dateUtils
            .convertLocalDateToServer(project.planned_start);
        copy.planned_finish = this.dateUtils
            .convertLocalDateToServer(project.planned_finish);
        copy.actual_start = this.dateUtils
            .convertLocalDateToServer(project.actual_start);
        copy.actual_finish = this.dateUtils
            .convertLocalDateToServer(project.actual_finish);
        copy.created_date = this.dateUtils
            .convertLocalDateToServer(project.created_date);
        copy.modified_date = this.dateUtils
            .convertLocalDateToServer(project.modified_date);
        copy.td_date = this.dateUtils
            .convertLocalDateToServer(project.td_date);
        return copy;
    }
}
