import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { Project } from './project.model';
import { ProjectService } from './project.service';

import { SharedStorageService } from '../../shared';

@Component({
    selector: 'jhi-project-detail',
    templateUrl: './project-detail.component.html'
})
export class ProjectDetailComponent implements OnInit, OnDestroy {

    project: Project;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private projectService: ProjectService,
        private sharedStorageService: SharedStorageService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInProjects();
    }

    load(id) {
        this.projectService.find(id).subscribe((project) => {
            this.project = project;
            this.sharedStorageService.setCurrentProject(this.project);
            this.loadComponents(id);
            this.loadTests(id);
            this.loadReviewPoints(id);
            this.loadRiskAndIssues(id);
            this.loadClarifications(id);
            this.loadHistory(id);
        });
    }

    loadComponents(id) {
        this.projectService.findComponents(id).subscribe((data) => {
            this.project.comps = data;
        });
    }

    loadTests(id) {
        this.projectService.findTests(id).subscribe((data) => {
            this.project.unitTests = data;
        });
    }

    loadReviewPoints(id) {
        this.projectService.findReviewPoints(id).subscribe((data) => {
            this.project.pointsheets = data;
        });
    }

    loadRiskAndIssues(id) {
        this.projectService.findRiskAndIssues(id).subscribe((data) => {
            this.project.riskAndIssues = data;
        });
    }

    loadClarifications(id) {
        this.projectService.findClarifications(id).subscribe((data) => {
            this.project.clarifications = data;
        });
    }

    loadHistory(id) {
        this.projectService.findHistory(id).subscribe((data) => {
            this.project.projLogs = data;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
        this.sharedStorageService.clearProject();
    }

    registerChangeInProjects() {
        this.eventSubscriber = this.eventManager.subscribe(
            'projectListModification',
            (response) => this.load(this.project.id)
        );
        this.eventSubscriber = this.eventManager.subscribe(
            'compListModification',
            (response) => this.load(this.project.id)
        );
    }

    reloadProject() {
        this.projectService.update(this.project);
    }
}
