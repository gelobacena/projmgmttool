import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Project } from './project.model';
import { ProjectService } from './project.service';

@Injectable()
export class ProjectPopupService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
        private router: Router,
        private projectService: ProjectService

    ) {}

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.projectService.find(id).subscribe((project) => {
                if (project.planned_start) {
                    project.planned_start = {
                        year: project.planned_start.getFullYear(),
                        month: project.planned_start.getMonth() + 1,
                        day: project.planned_start.getDate()
                    };
                }
                if (project.planned_finish) {
                    project.planned_finish = {
                        year: project.planned_finish.getFullYear(),
                        month: project.planned_finish.getMonth() + 1,
                        day: project.planned_finish.getDate()
                    };
                }
                if (project.actual_start) {
                    project.actual_start = {
                        year: project.actual_start.getFullYear(),
                        month: project.actual_start.getMonth() + 1,
                        day: project.actual_start.getDate()
                    };
                }
                if (project.actual_finish) {
                    project.actual_finish = {
                        year: project.actual_finish.getFullYear(),
                        month: project.actual_finish.getMonth() + 1,
                        day: project.actual_finish.getDate()
                    };
                }
                if (project.created_date) {
                    project.created_date = {
                        year: project.created_date.getFullYear(),
                        month: project.created_date.getMonth() + 1,
                        day: project.created_date.getDate()
                    };
                }
                if (project.modified_date) {
                    project.modified_date = {
                        year: project.modified_date.getFullYear(),
                        month: project.modified_date.getMonth() + 1,
                        day: project.modified_date.getDate()
                    };
                }
                if (project.td_date) {
                    project.td_date = {
                        year: project.td_date.getFullYear(),
                        month: project.td_date.getMonth() + 1,
                        day: project.td_date.getDate()
                    };
                }
                this.projectModalRef(component, project);
            });
        } else {
            return this.projectModalRef(component, new Project());
        }
    }

    projectModalRef(component: Component, project: Project): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.project = project;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
