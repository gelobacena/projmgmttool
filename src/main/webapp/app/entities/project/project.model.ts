import { BaseEntity } from './../../shared';

export class Project implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public team?: string,
        public planned_start?: any,
        public planned_finish?: any,
        public actual_start?: any,
        public actual_finish?: any,
        public created_by?: string,
        public created_date?: any,
        public modified_by?: string,
        public modified_date?: any,
        public status?: string,
        public is_deleted?: number,
        public td_date?: any,
        public bac_td_walkthrough?: number,
        public bac_td_analysis?: number,
        public bac_ut_execution?: number,
        public bac_utplan_review?: number,
        public bac_utp?: number,
        public comps?: BaseEntity[],
        public clarifications?: BaseEntity[],
        public pointsheets?: BaseEntity[],
        public unitTests?: BaseEntity[],
        public riskAndIssues?: BaseEntity[],
        public projLogs?: BaseEntity[],
    ) {
    }
}
