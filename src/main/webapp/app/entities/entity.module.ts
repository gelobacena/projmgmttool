import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { QdestroyerProjectModule } from './project/project.module';
import { QdestroyerCompModule } from './comp/comp.module';
import { QdestroyerClarificationModule } from './clarification/clarification.module';
import { QdestroyerPointsheetModule } from './pointsheet/pointsheet.module';
import { QdestroyerUnitTestModule } from './unit-test/unit-test.module';
import { QdestroyerProjLogModule } from './proj-log/proj-log.module';
import { QdestroyerRiskAndIssuesModule } from './risk-and-issues/risk-and-issues.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        QdestroyerProjectModule,
        QdestroyerCompModule,
        QdestroyerClarificationModule,
        QdestroyerPointsheetModule,
        QdestroyerUnitTestModule,
        QdestroyerProjLogModule,
        QdestroyerRiskAndIssuesModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QdestroyerEntityModule {}
