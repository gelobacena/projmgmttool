import './vendor.ts';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Ng2Webstorage } from 'ng2-webstorage';

import { QdestroyerSharedModule, UserRouteAccessService, SharedStorageService } from './shared';
import { QdestroyerHomeModule } from './home/home.module';
import { QdestroyerAdminModule } from './admin/admin.module';
import { QdestroyerAccountModule } from './account/account.module';
import { QdestroyerEntityModule } from './entities/entity.module';
import { MyBuildModule } from './mybuild/mybuild.module';
import { MyReviewModule } from './myreview/myreview.module';

import { customHttpProvider } from './blocks/interceptor/http.provider';
import { PaginationConfig } from './blocks/config/uib-pagination.config';

// jhipster-needle-angular-add-module-import JHipster will add new module here

import {
    JhiMainComponent,
    LayoutRoutingModule,
    NavbarComponent,
    FooterComponent,
    ProfileService,
    PageRibbonComponent,
    ErrorComponent
} from './layouts';

@NgModule({
    imports: [
        BrowserModule,
        LayoutRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-'}),
        QdestroyerSharedModule,
        QdestroyerHomeModule,
        QdestroyerAdminModule,
        QdestroyerAccountModule,
        QdestroyerEntityModule,
        MyBuildModule,
        MyReviewModule
        // jhipster-needle-angular-add-module JHipster will add new module here
    ],
    declarations: [
        JhiMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        FooterComponent
    ],
    providers: [
        ProfileService,
        customHttpProvider(),
        PaginationConfig,
        UserRouteAccessService,
        SharedStorageService
    ],
    bootstrap: [ JhiMainComponent ]
})
export class QdestroyerAppModule {}
