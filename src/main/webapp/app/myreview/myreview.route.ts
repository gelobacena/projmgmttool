import { Route } from '@angular/router';

import { UserRouteAccessService } from '../shared';
import { MyReviewComponent } from './';

export const MYREVIEW_ROUTE: Route = {
    path: 'my-review-tasks',
    component: MyReviewComponent,
    data: {
        authorities: [],
        pageTitle: 'My Review Tasks'
    }
}
