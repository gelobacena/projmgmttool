import { Component, OnInit } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';

import { Account, Principal } from '../shared';

import { MyReviewService } from './myreview.service';

@Component({
    selector: 'jhi-myreview',
    templateUrl: './myreview.component.html'
})
export class MyReviewComponent implements OnInit {
    account: Account;
    reviews: any;

    constructor(
        private principal: Principal,
        private myReviewService: MyReviewService,
        private eventManager: JhiEventManager
    ) {

    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.account = account;
            this.myReviewService.findReviewTasks(account.login).subscribe((data) => {
                this.reviews = data;
            });
        });
    }
}
