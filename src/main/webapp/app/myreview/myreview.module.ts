import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { QdestroyerSharedModule } from '../shared';

import {
    MYREVIEW_ROUTE,
    MyReviewComponent,
    MyReviewService
} from './';

@NgModule({
    imports: [
        QdestroyerSharedModule,
        RouterModule.forRoot([ MYREVIEW_ROUTE ], {useHash: true})
    ],
    declarations: [
        MyReviewComponent
    ],
    entryComponents: [
        MyReviewComponent
    ],
    providers: [
        MyReviewService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MyReviewModule {}
