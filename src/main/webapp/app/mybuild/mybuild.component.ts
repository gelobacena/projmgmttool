import { Component, OnInit } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';

import { Account, Principal } from '../shared';

import { MyBuildService } from './mybuild.service';

@Component({
    selector: 'jhi-mybuild',
    templateUrl: './mybuild.component.html'
})
export class MyBuildComponent implements OnInit {
    account: Account;
    tasks: any;
    predicate: any;
    reverse: any;

    constructor(
        private principal: Principal,
        private eventManager: JhiEventManager,
        private myBuildService: MyBuildService
    ) {
        this.predicate = 'id';
        this.reverse = true;
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.account = account;
            this.myBuildService.findTasks(account.login).subscribe((data) => {
                this.tasks = data;
            })
        });
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }
}
