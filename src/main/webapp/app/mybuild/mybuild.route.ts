import { Route } from '@angular/router';

import { UserRouteAccessService } from '../shared';
import { MyBuildComponent } from './';

export const MYBUILD_ROUTE: Route = {
    path: 'my-build-tasks',
    component: MyBuildComponent,
    data: {
        authorities: [],
        pageTitle: 'My Build Tasks'
    }
}
