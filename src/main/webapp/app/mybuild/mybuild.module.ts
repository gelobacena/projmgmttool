import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { QdestroyerSharedModule } from '../shared';

import {
    MYBUILD_ROUTE,
    MyBuildComponent,
    MyBuildService
} from './';

@NgModule({
    imports: [
        QdestroyerSharedModule,
        RouterModule.forRoot([ MYBUILD_ROUTE ], {useHash: true})
    ],
    declarations: [
        MyBuildComponent
    ],
    entryComponents: [
        MyBuildComponent
    ],
    providers: [
        MyBuildService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MyBuildModule {}
