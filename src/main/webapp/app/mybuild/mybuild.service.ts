import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { ResponseWrapper } from '../shared';

@Injectable()
export class MyBuildService {

    private resourceUrl = 'api/comps';

    constructor(private http: Http) { }

    findTasks(eid: String) {
        return this.http.post(`${this.resourceUrl}/tasks`, eid).map((res: Response) => res.json());
    }
}
