import { Injectable } from '@angular/core';

import { Project } from '../entities/project/project.model';
import { Comp } from '../entities/comp/comp.model';
import { Pointsheet } from '../entities/pointsheet/pointsheet.model';
import { Clarification } from '../entities/clarification/clarification.model';
import { RiskAndIssues } from '../entities/risk-and-issues/risk-and-issues.model';
import { UnitTest } from '../entities/unit-test/unit-test.model';

@Injectable()
export class SharedStorageService {

    private currentProject: Project;
    private currentComp: Comp;
    private currentPointsheet: Pointsheet;
    private currentClarification: Clarification;
    private currentRiskAndIssues: RiskAndIssues;
    private currentUnitTest: UnitTest;

    constructor() {}

    setCurrentProject(project: Project) {
        this.currentProject = project;
    }

    setCurrentComp(comp: Comp) {
        this.currentComp = comp;
    }

    setCurrentPointsheet(pointsheet: Pointsheet) {
        this.currentPointsheet = pointsheet;
    }

    setCurrentClarification(clarification: Clarification) {
        this.currentClarification = clarification;
    }

    setCurrentRiskAndIssues(ri: RiskAndIssues) {
        this.currentRiskAndIssues = ri;
    }

    setCurrentUnitTest(unitTest: UnitTest) {
        this.currentUnitTest = unitTest;
    }

    getCurrentProject(): Project {
        return this.currentProject;
    }

    getCurrentComp(): Comp {
        return this.currentComp;
    }

    getCurrentPointsheet(): Pointsheet {
        return this.currentPointsheet;
    }

    getCurrentClarification(): Clarification {
        return this.currentClarification;
    }

    getCurrentRiskAndIssues(): RiskAndIssues {
        return this.currentRiskAndIssues;
    }

    getCurrentUnitTest(): UnitTest {
        return this.currentUnitTest;
    }

    clearStorage() {
        this.currentProject = null;
        this.currentComp = null;
        this.currentPointsheet = null;
        this.currentClarification = null;
        this.currentRiskAndIssues = null;
        this.currentUnitTest = null;
    }

    clearProject() {
        this.currentProject = null;
    }

    clearComp() {
        this.currentComp = null;
    }

    clearClarification() {
        this.currentClarification = null;
    }

    clearPointsheet() {
        this.currentPointsheet = null;
    }

    clearRiskAndIssues() {
        this.currentRiskAndIssues = null;
    }

    clearUnitTest() {
        this.currentUnitTest = null;
    }
}
