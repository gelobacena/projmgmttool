package com.accenture.java.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.accenture.java.domain.Comp;


/**
 * Spring Data JPA repository for the Comp entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CompRepository extends JpaRepository<Comp,Long> {
	
	Set<Comp> findByDeveloper(String developer);
	
	Set<Comp> findByReviewerAndStatus(String reviewer, String status);
    
}
