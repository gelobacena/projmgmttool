package com.accenture.java.repository;

import com.accenture.java.domain.Clarification;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Clarification entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClarificationRepository extends JpaRepository<Clarification,Long> {
    
}
