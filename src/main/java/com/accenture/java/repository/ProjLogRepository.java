package com.accenture.java.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.accenture.java.domain.ProjLog;


/**
 * Spring Data JPA repository for the ProjLog entity.
 */
@Repository
public interface ProjLogRepository extends JpaRepository<ProjLog,Long> {
    Set<ProjLog> findByRecordId(Long recordId);
}
