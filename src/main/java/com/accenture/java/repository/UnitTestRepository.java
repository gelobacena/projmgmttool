package com.accenture.java.repository;

import com.accenture.java.domain.UnitTest;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the UnitTest entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UnitTestRepository extends JpaRepository<UnitTest,Long> {
    
}
