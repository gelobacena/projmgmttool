package com.accenture.java.repository;

import com.accenture.java.domain.RiskAndIssues;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the RiskAndIssues entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RiskAndIssuesRepository extends JpaRepository<RiskAndIssues,Long> {
    
}
