package com.accenture.java.repository;

import com.accenture.java.domain.Project;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Project entity.
 */
@Repository
public interface ProjectRepository extends JpaRepository<Project,Long> {
	
	Page<Project> findByIsDeleted(Integer isDeleted, Pageable pageable);
    
}
