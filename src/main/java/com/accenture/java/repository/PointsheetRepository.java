package com.accenture.java.repository;

import com.accenture.java.domain.Pointsheet;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Pointsheet entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PointsheetRepository extends JpaRepository<Pointsheet,Long> {
    
}
