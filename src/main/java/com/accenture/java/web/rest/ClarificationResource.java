package com.accenture.java.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.accenture.java.domain.Clarification;
import com.accenture.java.domain.ProjLog;
import com.accenture.java.domain.enumeration.LogType;
import com.accenture.java.service.ClarificationService;
import com.accenture.java.service.ProjLogService;
import com.accenture.java.web.rest.util.HeaderUtil;
import com.accenture.java.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * REST controller for managing Clarification.
 */
@RestController
@RequestMapping("/api")
public class ClarificationResource {

    private final Logger log = LoggerFactory.getLogger(ClarificationResource.class);

    private static final String ENTITY_NAME = "clarification";
    
    private final LogType logType = LogType.CLARIFICATION;

    private final ClarificationService clarificationService;
    
    private final ProjLogService logService;

    public ClarificationResource(ClarificationService clarificationService, ProjLogService logService) {
        this.clarificationService = clarificationService;
        this.logService = logService;
    }

    /**
     * POST  /clarifications : Create a new clarification.
     *
     * @param clarification the clarification to create
     * @return the ResponseEntity with status 201 (Created) and with body the new clarification, or with status 400 (Bad Request) if the clarification has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/clarifications")
    @Timed
    public ResponseEntity<Clarification> createClarification(@RequestBody Clarification clarification) throws URISyntaxException {
        log.debug("REST request to save Clarification : {}", clarification);
        if (clarification.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new clarification cannot already have an ID")).body(null);
        }
        Clarification result = clarificationService.save(clarification);
        logService.logCreate(result.getId(), result.getCreated_by(), logType, result.getProject());
        return ResponseEntity.created(new URI("/api/clarifications/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /clarifications : Updates an existing clarification.
     *
     * @param clarification the clarification to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated clarification,
     * or with status 400 (Bad Request) if the clarification is not valid,
     * or with status 500 (Internal Server Error) if the clarification couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/clarifications")
    @Timed
    public ResponseEntity<Clarification> updateClarification(@RequestBody Clarification clarification) throws URISyntaxException {
        log.debug("REST request to update Clarification : {}", clarification);
        if (clarification.getId() == null) {
            return createClarification(clarification);
        }
        Clarification result = clarificationService.save(clarification);
        logService.logUpdate(result.getId(), result.getModified_by(), logType, result.getProject());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, clarification.getId().toString()))
            .body(result);
    }

    /**
     * GET  /clarifications : get all the clarifications.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of clarifications in body
     */
    @GetMapping("/clarifications")
    @Timed
    public ResponseEntity<List<Clarification>> getAllClarifications(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Clarifications");
        Page<Clarification> page = clarificationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/clarifications");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /clarifications/:id : get the "id" clarification.
     *
     * @param id the id of the clarification to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the clarification, or with status 404 (Not Found)
     */
    @GetMapping("/clarifications/{id}")
    @Timed
    public ResponseEntity<Clarification> getClarification(@PathVariable Long id) {
        log.debug("REST request to get Clarification : {}", id);
        Clarification clarification = clarificationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(clarification));
    }
    
    @GetMapping("/clarifications/{id}/history")
    @Timed
    public ResponseEntity<Set<ProjLog>> getHistory(@PathVariable Long id) {
        log.debug("REST request to get Clarification : {}", id);
        Set<ProjLog> history = clarificationService.getHistory(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(history));
    }

    /**
     * DELETE  /clarifications/:id : delete the "id" clarification.
     *
     * @param id the id of the clarification to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/clarifications/{id}")
    @Timed
    public ResponseEntity<Void> deleteClarification(@PathVariable Long id) {
        log.debug("REST request to delete Clarification : {}", id);
        Clarification result = clarificationService.delete(id);
        logService.logDelete(result.getId(), result.getModified_by(), logType, result.getProject());
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
