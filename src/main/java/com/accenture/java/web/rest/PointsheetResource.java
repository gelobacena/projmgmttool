package com.accenture.java.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.accenture.java.domain.Pointsheet;
import com.accenture.java.domain.ProjLog;
import com.accenture.java.domain.enumeration.LogType;
import com.accenture.java.service.PointsheetService;
import com.accenture.java.service.ProjLogService;
import com.accenture.java.web.rest.util.HeaderUtil;
import com.accenture.java.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * REST controller for managing Pointsheet.
 */
@RestController
@RequestMapping("/api")
public class PointsheetResource {

    private final Logger log = LoggerFactory.getLogger(PointsheetResource.class);

    private static final String ENTITY_NAME = "pointsheet";
    
    private final LogType logType = LogType.POINTSHEET;

    private final PointsheetService pointsheetService;
    
    private final ProjLogService logService;

    public PointsheetResource(PointsheetService pointsheetService, ProjLogService logService) {
        this.pointsheetService = pointsheetService;
        this.logService = logService;
    }

    /**
     * POST  /pointsheets : Create a new pointsheet.
     *
     * @param pointsheet the pointsheet to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pointsheet, or with status 400 (Bad Request) if the pointsheet has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pointsheets")
    @Timed
    public ResponseEntity<Pointsheet> createPointsheet(@RequestBody Pointsheet pointsheet) throws URISyntaxException {
        log.debug("REST request to save Pointsheet : {}", pointsheet);
        if (pointsheet.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new pointsheet cannot already have an ID")).body(null);
        }
        Pointsheet result = pointsheetService.save(pointsheet);
        logService.logCreate(result.getId(), result.getCreated_by(), logType, result.getProject());
        return ResponseEntity.created(new URI("/api/pointsheets/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pointsheets : Updates an existing pointsheet.
     *
     * @param pointsheet the pointsheet to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pointsheet,
     * or with status 400 (Bad Request) if the pointsheet is not valid,
     * or with status 500 (Internal Server Error) if the pointsheet couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pointsheets")
    @Timed
    public ResponseEntity<Pointsheet> updatePointsheet(@RequestBody Pointsheet pointsheet) throws URISyntaxException {
        log.debug("REST request to update Pointsheet : {}", pointsheet);
        if (pointsheet.getId() == null) {
            return createPointsheet(pointsheet);
        }
        Pointsheet result = pointsheetService.save(pointsheet);
        logService.logUpdate(result.getId(), result.getModified_by(), logType, result.getProject());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, pointsheet.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pointsheets : get all the pointsheets.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of pointsheets in body
     */
    @GetMapping("/pointsheets")
    @Timed
    public ResponseEntity<List<Pointsheet>> getAllPointsheets(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Pointsheets");
        Page<Pointsheet> page = pointsheetService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/pointsheets");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /pointsheets/:id : get the "id" pointsheet.
     *
     * @param id the id of the pointsheet to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the pointsheet, or with status 404 (Not Found)
     */
    @GetMapping("/pointsheets/{id}")
    @Timed
    public ResponseEntity<Pointsheet> getPointsheet(@PathVariable Long id) {
        log.debug("REST request to get Pointsheet : {}", id);
        Pointsheet pointsheet = pointsheetService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(pointsheet));
    }
    
    @GetMapping("/pointsheets/{id}/history")
    @Timed
    public ResponseEntity<Set<ProjLog>> getHistory(@PathVariable Long id) {
        log.debug("REST request to get Pointsheet : {}", id);
        Set<ProjLog> history = pointsheetService.getHistory(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(history));
    }

    /**
     * DELETE  /pointsheets/:id : delete the "id" pointsheet.
     *
     * @param id the id of the pointsheet to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pointsheets/{id}")
    @Timed
    public ResponseEntity<Void> deletePointsheet(@PathVariable Long id) {
        log.debug("REST request to delete Pointsheet : {}", id);
        Pointsheet result = pointsheetService.delete(id);
        logService.logDelete(result.getId(), result.getModified_by(), logType, result.getProject());
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
