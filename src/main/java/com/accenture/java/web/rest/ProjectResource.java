package com.accenture.java.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.java.domain.Clarification;
import com.accenture.java.domain.Comp;
import com.accenture.java.domain.Pointsheet;
import com.accenture.java.domain.ProjLog;
import com.accenture.java.domain.Project;
import com.accenture.java.domain.RiskAndIssues;
import com.accenture.java.domain.UnitTest;
import com.accenture.java.domain.enumeration.LogType;
import com.accenture.java.service.ProjLogService;
import com.accenture.java.service.ProjectService;
import com.accenture.java.web.rest.util.HeaderUtil;
import com.accenture.java.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing Project.
 */
@RestController
@RequestMapping("/api")
public class ProjectResource {

    private final Logger log = LoggerFactory.getLogger(ProjectResource.class);

    private static final String ENTITY_NAME = "project";
    
    private final LogType logType = LogType.PROJECT;

    private final ProjectService projectService;
    
    private final ProjLogService logService;

    public ProjectResource(ProjectService projectService, ProjLogService logService) {
        this.projectService = projectService;
        this.logService = logService;
    }

    /**
     * POST  /projects : Create a new project.
     *
     * @param project the project to create
     * @return the ResponseEntity with status 201 (Created) and with body the new project, or with status 400 (Bad Request) if the project has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/projects")
    @Timed
    public ResponseEntity<Project> createProject(@RequestBody Project project) throws URISyntaxException {
        log.debug("REST request to save Project : {}", project);
        if (project.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new project cannot already have an ID")).body(null);
        }
        Project result = projectService.save(project);
        logService.logCreate(result.getId(), result.getCreated_by(), logType, result);
        return ResponseEntity.created(new URI("/api/projects/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /projects : Updates an existing project.
     *
     * @param project the project to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated project,
     * or with status 400 (Bad Request) if the project is not valid,
     * or with status 500 (Internal Server Error) if the project couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/projects")
    @Timed
    public ResponseEntity<Project> updateProject(@RequestBody Project project) throws URISyntaxException {
        log.debug("REST request to update Project : {}", project);
        if (project.getId() == null) {
            return createProject(project);
        }
        Project result = projectService.save(project);
        logService.logUpdate(result.getId(), result.getModified_by(), logType, result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, project.getId().toString()))
            .body(result);
    }

    /**
     * GET  /projects : get all the projects.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of projects in body
     */
    @GetMapping("/projects")
    @Timed
    public ResponseEntity<List<Project>> getAllProjects(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Projects");
        Page<Project> page = projectService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/projects");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /projects/:id : get the "id" project.
     *
     * @param id the id of the project to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the project, or with status 404 (Not Found)
     */
    @GetMapping("/projects/{id}")
    @Timed
    public ResponseEntity<Project> getProject(@PathVariable Long id) {
        log.debug("REST request to get Project : {}", id);
        Project project = projectService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(project));
    }
    
    @GetMapping("/projects/{id}/tests")
    @Timed
    public ResponseEntity<Set<UnitTest>> getTests(@PathVariable Long id) {
    	log.debug("Getting components of of Project : {}", id);
    	Set<UnitTest> tests = projectService.getTests(id);
    	return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tests));
    }
    
    @GetMapping("/projects/{id}/comps")
    @Timed
    public ResponseEntity<Set<Comp>> getComps(@PathVariable Long id) {
    	log.debug("Getting tests of of Project : {}", id);
    	Set<Comp> components = projectService.getComponents(id);
    	return ResponseUtil.wrapOrNotFound(Optional.ofNullable(components));
    }
    
    @GetMapping("/projects/{id}/reviewpoints")
    @Timed
    public ResponseEntity<Set<Pointsheet>> getReviewPoints(@PathVariable Long id) {
    	log.debug("Getting tests of of Project : {}", id);
    	Set<Pointsheet> reviewPoints = projectService.getReviewPoints(id);
    	return ResponseUtil.wrapOrNotFound(Optional.ofNullable(reviewPoints));
    }
    
    @GetMapping("/projects/{id}/riskandissues")
    @Timed
    public ResponseEntity<Set<RiskAndIssues>> getRiskAndIssues(@PathVariable Long id) {
    	log.debug("Getting tests of of Project : {}", id);
    	Set<RiskAndIssues> riskAndIssues = projectService.getRisksAndIssues(id);
    	return ResponseUtil.wrapOrNotFound(Optional.ofNullable(riskAndIssues));
    }
    
    @GetMapping("/projects/{id}/clarifications")
    @Timed
    public ResponseEntity<Set<Clarification>> getClarifications(@PathVariable Long id) {
    	log.debug("Getting tests of of Project : {}", id);
    	Set<Clarification> clarifications = projectService.getClarifications(id);
    	return ResponseUtil.wrapOrNotFound(Optional.ofNullable(clarifications));
    }
    
    @GetMapping("/projects/{id}/history")
    @Timed
    public ResponseEntity<Set<ProjLog>> getHistory(@PathVariable Long id) {
    	log.debug("Getting tests of of Project : {}", id);
    	Set<ProjLog> history = projectService.getHistory(id);
    	return ResponseUtil.wrapOrNotFound(Optional.ofNullable(history));
    }
    
    

    /**
     * DELETE  /projects/:id : delete the "id" project.
     *
     * @param id the id of the project to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/projects/{id}")
    @Timed
    public ResponseEntity<Void> deleteProject(@PathVariable Long id) {
        log.debug("REST request to delete Project : {}", id);
        Project result = projectService.delete(id);
        logService.logDelete(result.getId(), result.getModified_by(), logType, result);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
