package com.accenture.java.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.accenture.java.domain.ProjLog;
import com.accenture.java.domain.RiskAndIssues;
import com.accenture.java.domain.enumeration.LogType;
import com.accenture.java.service.ProjLogService;
import com.accenture.java.service.RiskAndIssuesService;
import com.accenture.java.web.rest.util.HeaderUtil;
import com.accenture.java.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * REST controller for managing RiskAndIssues.
 */
@RestController
@RequestMapping("/api")
public class RiskAndIssuesResource {

    private final Logger log = LoggerFactory.getLogger(RiskAndIssuesResource.class);

    private static final String ENTITY_NAME = "riskAndIssues";
    
    private final LogType logType = LogType.RISKISSUE;

    private final RiskAndIssuesService riskAndIssuesService;
    
    private final ProjLogService logService;

    public RiskAndIssuesResource(RiskAndIssuesService riskAndIssuesService, ProjLogService logService) {
        this.riskAndIssuesService = riskAndIssuesService;
        this.logService = logService;
    }

    /**
     * POST  /risk-and-issues : Create a new riskAndIssues.
     *
     * @param riskAndIssues the riskAndIssues to create
     * @return the ResponseEntity with status 201 (Created) and with body the new riskAndIssues, or with status 400 (Bad Request) if the riskAndIssues has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/risk-and-issues")
    @Timed
    public ResponseEntity<RiskAndIssues> createRiskAndIssues(@RequestBody RiskAndIssues riskAndIssues) throws URISyntaxException {
        log.debug("REST request to save RiskAndIssues : {}", riskAndIssues);
        if (riskAndIssues.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new riskAndIssues cannot already have an ID")).body(null);
        }
        RiskAndIssues result = riskAndIssuesService.save(riskAndIssues);
        logService.logCreate(result.getId(), result.getCreated_by(), logType, result.getProject());
        return ResponseEntity.created(new URI("/api/risk-and-issues/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /risk-and-issues : Updates an existing riskAndIssues.
     *
     * @param riskAndIssues the riskAndIssues to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated riskAndIssues,
     * or with status 400 (Bad Request) if the riskAndIssues is not valid,
     * or with status 500 (Internal Server Error) if the riskAndIssues couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/risk-and-issues")
    @Timed
    public ResponseEntity<RiskAndIssues> updateRiskAndIssues(@RequestBody RiskAndIssues riskAndIssues) throws URISyntaxException {
        log.debug("REST request to update RiskAndIssues : {}", riskAndIssues);
        if (riskAndIssues.getId() == null) {
            return createRiskAndIssues(riskAndIssues);
        }
        RiskAndIssues result = riskAndIssuesService.save(riskAndIssues);
        logService.logUpdate(result.getId(), result.getModified_by(), logType, result.getProject());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, riskAndIssues.getId().toString()))
            .body(result);
    }

    /**
     * GET  /risk-and-issues : get all the riskAndIssues.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of riskAndIssues in body
     */
    @GetMapping("/risk-and-issues")
    @Timed
    public ResponseEntity<List<RiskAndIssues>> getAllRiskAndIssues(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of RiskAndIssues");
        Page<RiskAndIssues> page = riskAndIssuesService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/risk-and-issues");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /risk-and-issues/:id : get the "id" riskAndIssues.
     *
     * @param id the id of the riskAndIssues to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the riskAndIssues, or with status 404 (Not Found)
     */
    @GetMapping("/risk-and-issues/{id}")
    @Timed
    public ResponseEntity<RiskAndIssues> getRiskAndIssues(@PathVariable Long id) {
        log.debug("REST request to get RiskAndIssues : {}", id);
        RiskAndIssues riskAndIssues = riskAndIssuesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(riskAndIssues));
    }
    
    @GetMapping("/risk-and-issues/{id}/history")
    @Timed
    public ResponseEntity<Set<ProjLog>> getHistory(@PathVariable Long id) {
        log.debug("REST request to get RiskAndIssues : {}", id);
        Set<ProjLog> history = riskAndIssuesService.getHistory(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(history));
    }


    /**
     * DELETE  /risk-and-issues/:id : delete the "id" riskAndIssues.
     *
     * @param id the id of the riskAndIssues to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/risk-and-issues/{id}")
    @Timed
    public ResponseEntity<Void> deleteRiskAndIssues(@PathVariable Long id) {
        log.debug("REST request to delete RiskAndIssues : {}", id);
        RiskAndIssues result = riskAndIssuesService.delete(id);
        logService.logDelete(result.getId(), result.getModified_by(), logType, result.getProject());
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
