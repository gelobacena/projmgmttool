package com.accenture.java.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.java.domain.ProjLog;
import com.accenture.java.domain.UnitTest;
import com.accenture.java.domain.enumeration.LogType;
import com.accenture.java.service.ProjLogService;
import com.accenture.java.service.UnitTestService;
import com.accenture.java.web.rest.util.HeaderUtil;
import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing UnitTest.
 */
@RestController
@RequestMapping("/api")
public class UnitTestResource {

    private final Logger log = LoggerFactory.getLogger(UnitTestResource.class);

    private static final String ENTITY_NAME = "unitTest";
    
    private final LogType logType = LogType.TEST;

    private final UnitTestService unitTestService;
    
    private final ProjLogService logService;

    public UnitTestResource(UnitTestService unitTestService, ProjLogService logService) {
        this.unitTestService = unitTestService;
        this.logService	= logService;
    }

    /**
     * POST  /unit-tests : Create a new unitTest.
     *
     * @param unitTest the unitTest to create
     * @return the ResponseEntity with status 201 (Created) and with body the new unitTest, or with status 400 (Bad Request) if the unitTest has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unit-tests")
    @Timed
    public ResponseEntity<UnitTest> createUnitTest(@RequestBody UnitTest unitTest) throws URISyntaxException {
        log.debug("REST request to save UnitTest : {}", unitTest);
        if (unitTest.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new unitTest cannot already have an ID")).body(null);
        }
        UnitTest result = unitTestService.save(unitTest);
        logService.logCreate(result.getId(), result.getCreated_by(), logType, result.getProject());
        return ResponseEntity.created(new URI("/api/unit-tests/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /unit-tests : Updates an existing unitTest.
     *
     * @param unitTest the unitTest to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated unitTest,
     * or with status 400 (Bad Request) if the unitTest is not valid,
     * or with status 500 (Internal Server Error) if the unitTest couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/unit-tests")
    @Timed
    public ResponseEntity<UnitTest> updateUnitTest(@RequestBody UnitTest unitTest) throws URISyntaxException {
        log.debug("REST request to update UnitTest : {}", unitTest);
        if (unitTest.getId() == null) {
            return createUnitTest(unitTest);
        }
        UnitTest result = unitTestService.save(unitTest);
        logService.logUpdate(result.getId(), result.getModified_by(), logType, result.getProject());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, unitTest.getId().toString()))
            .body(result);
    }

    /**
     * GET  /unit-tests : get all the unitTests.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of unitTests in body
     */
    @GetMapping("/unit-tests")
    @Timed
    public List<UnitTest> getAllUnitTests() {
        log.debug("REST request to get all UnitTests");
        return unitTestService.findAll();
    }

    /**
     * GET  /unit-tests/:id : get the "id" unitTest.
     *
     * @param id the id of the unitTest to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the unitTest, or with status 404 (Not Found)
     */
    @GetMapping("/unit-tests/{id}")
    @Timed
    public ResponseEntity<UnitTest> getUnitTest(@PathVariable Long id) {
        log.debug("REST request to get UnitTest : {}", id);
        UnitTest unitTest = unitTestService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(unitTest));
    }
    
    @GetMapping("/unit-tests/{id}/history")
    @Timed
    public ResponseEntity<Set<ProjLog>> getHistory(@PathVariable Long id) {
        log.debug("REST request to get UnitTest : {}", id);
        Set<ProjLog> history = unitTestService.getHistory(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(history));
    }

    /**
     * DELETE  /unit-tests/:id : delete the "id" unitTest.
     *
     * @param id the id of the unitTest to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/unit-tests/{id}")
    @Timed
    public ResponseEntity<Void> deleteUnitTest(@PathVariable Long id) {
        log.debug("REST request to delete UnitTest : {}", id);
        UnitTest result = unitTestService.delete(id);
        logService.logDelete(result.getId(), result.getCreated_by(), logType, result.getProject());
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
