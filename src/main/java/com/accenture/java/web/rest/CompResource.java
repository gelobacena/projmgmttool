package com.accenture.java.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.accenture.java.domain.Clarification;
import com.accenture.java.domain.Comp;
import com.accenture.java.domain.Pointsheet;
import com.accenture.java.domain.ProjLog;
import com.accenture.java.domain.UnitTest;
import com.accenture.java.domain.enumeration.LogType;
import com.accenture.java.service.CompService;
import com.accenture.java.service.ProjLogService;
import com.accenture.java.web.rest.util.HeaderUtil;
import com.accenture.java.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * REST controller for managing Comp.
 */
@RestController
@RequestMapping("/api")
public class CompResource {

    private final Logger log = LoggerFactory.getLogger(CompResource.class);

    private static final String ENTITY_NAME = "comp";
    
    private final LogType logType = LogType.COMPONENT;

    private final CompService compService;
    
    private final ProjLogService logService;

    public CompResource(CompService compService, ProjLogService logService) {
        this.compService = compService;
        this.logService = logService;
    }

    /**
     * POST  /comps : Create a new comp.
     *
     * @param comp the comp to create
     * @return the ResponseEntity with status 201 (Created) and with body the new comp, or with status 400 (Bad Request) if the comp has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/comps")
    @Timed
    public ResponseEntity<Comp> createComp(@RequestBody Comp comp) throws URISyntaxException {
        log.debug("REST request to save Comp : {}", comp);
        if (comp.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new comp cannot already have an ID")).body(null);
        }
        Comp result = compService.save(comp);
        logService.logCreate(result.getId(), result.getCreated_by(), logType, result.getProject());
        return ResponseEntity.created(new URI("/api/comps/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /comps : Updates an existing comp.
     *
     * @param comp the comp to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated comp,
     * or with status 400 (Bad Request) if the comp is not valid,
     * or with status 500 (Internal Server Error) if the comp couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/comps")
    @Timed
    public ResponseEntity<Comp> updateComp(@RequestBody Comp comp) throws URISyntaxException {
        log.debug("REST request to update Comp : {}", comp);
        if (comp.getId() == null) {
            return createComp(comp);
        }
        Comp result = compService.save(comp);
        logService.logUpdate(result.getId(), result.getModified_by(), logType, result.getProject());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, comp.getId().toString()))
            .body(result);
    }

    /**
     * GET  /comps : get all the comps.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of comps in body
     */
    @GetMapping("/comps")
    @Timed
    public ResponseEntity<List<Comp>> getAllComps(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Comps");
        Page<Comp> page = compService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/comps");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /comps/:id : get the "id" comp.
     *
     * @param id the id of the comp to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the comp, or with status 404 (Not Found)
     */
    @GetMapping("/comps/{id}")
    @Timed
    public ResponseEntity<Comp> getComp(@PathVariable Long id) {
        log.debug("REST request to get Comp : {}", id);
        Comp comp = compService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(comp));
    }
    
    @GetMapping("/comps/{id}/tests")
    @Timed
    public ResponseEntity<Set<UnitTest>> getTests(@PathVariable Long id) {
        log.debug("REST request to get Comp : {}", id);
        Set<UnitTest> tests = compService.getTests(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tests));
    }
    
    @GetMapping("/comps/{id}/clarifications")
    @Timed
    public ResponseEntity<Set<Clarification>> getClarifications(@PathVariable Long id) {
        log.debug("REST request to get Comp : {}", id);
        Set<Clarification> clarifications = compService.getClarifications(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(clarifications));
    }
    
    @GetMapping("/comps/{id}/reviewpoints")
    @Timed
    public ResponseEntity<Set<Pointsheet>> getReviewPoints(@PathVariable Long id) {
        log.debug("REST request to get Comp : {}", id);
        Set<Pointsheet> reviewPoints = compService.getReviewPoints(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(reviewPoints));
    }
    
    @GetMapping("/comps/{id}/history")
    @Timed
    public ResponseEntity<Set<ProjLog>> getHistory(@PathVariable Long id) {
        log.debug("REST request to get Comp : {}", id);
        Set<ProjLog> history = compService.getHistory(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(history));
    }
    
    @PostMapping("/comps/tasks")
    @Timed
    public ResponseEntity<Set<Comp>> getTasks(@RequestBody String developer) {
    	log.debug("REST request to get tasks: {}", developer);
    	Set<Comp> tasks = compService.getTasks(developer);
    	return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tasks));
    }
    
    @PostMapping("/comps/reviews")
    @Timed
    public ResponseEntity<Set<Comp>> getReviewTasks(@RequestBody String reviewer) {
    	log.debug("REST request to get tasks: {}", reviewer);
    	Set<Comp> reviews = compService.getReviews(reviewer);
    	return ResponseUtil.wrapOrNotFound(Optional.ofNullable(reviews));
    }

    /**
     * DELETE  /comps/:id : delete the "id" comp.
     *
     * @param id the id of the comp to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/comps/{id}")
    @Timed
    public ResponseEntity<Void> deleteComp(@PathVariable Long id) {
        log.debug("REST request to delete Comp : {}", id);
        Comp result = compService.delete(id);
        logService.logDelete(result.getId(), result.getModified_by(), logType, result.getProject());
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
