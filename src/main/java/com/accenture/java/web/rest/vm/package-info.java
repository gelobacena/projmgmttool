/**
 * View Models used by Spring MVC REST controllers.
 */
package com.accenture.java.web.rest.vm;
