package com.accenture.java.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.accenture.java.domain.ProjLog;
import com.accenture.java.service.ProjLogService;
import com.accenture.java.web.rest.util.HeaderUtil;
import com.accenture.java.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ProjLog.
 */
@RestController
@RequestMapping("/api")
public class ProjLogResource {

    private final Logger log = LoggerFactory.getLogger(ProjLogResource.class);

    private static final String ENTITY_NAME = "projLog";

    private final ProjLogService projLogService;

    public ProjLogResource(ProjLogService projLogService) {
        this.projLogService = projLogService;
    }

    /**
     * POST  /proj-logs : Create a new projLog.
     *
     * @param projLog the projLog to create
     * @return the ResponseEntity with status 201 (Created) and with body the new projLog, or with status 400 (Bad Request) if the projLog has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/proj-logs")
    @Timed
    public ResponseEntity<ProjLog> createProjLog(@RequestBody ProjLog projLog) throws URISyntaxException {
        log.debug("REST request to save ProjLog : {}", projLog);
        if (projLog.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new projLog cannot already have an ID")).body(null);
        }
        ProjLog result = projLogService.save(projLog);
        return ResponseEntity.created(new URI("/api/proj-logs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /proj-logs : Updates an existing projLog.
     *
     * @param projLog the projLog to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated projLog,
     * or with status 400 (Bad Request) if the projLog is not valid,
     * or with status 500 (Internal Server Error) if the projLog couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/proj-logs")
    @Timed
    public ResponseEntity<ProjLog> updateProjLog(@RequestBody ProjLog projLog) throws URISyntaxException {
        log.debug("REST request to update ProjLog : {}", projLog);
        if (projLog.getId() == null) {
            return createProjLog(projLog);
        }
        ProjLog result = projLogService.save(projLog);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, projLog.getId().toString()))
            .body(result);
    }

    /**
     * GET  /proj-logs : get all the projLogs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of projLogs in body
     */
    @GetMapping("/proj-logs")
    @Timed
    public ResponseEntity<List<ProjLog>> getAllProjLogs(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ProjLogs");
        Page<ProjLog> page = projLogService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/proj-logs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /proj-logs/:id : get the "id" projLog.
     *
     * @param id the id of the projLog to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the projLog, or with status 404 (Not Found)
     */
    @GetMapping("/proj-logs/{id}")
    @Timed
    public ResponseEntity<ProjLog> getProjLog(@PathVariable Long id) {
        log.debug("REST request to get ProjLog : {}", id);
        ProjLog projLog = projLogService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(projLog));
    }

    /**
     * DELETE  /proj-logs/:id : delete the "id" projLog.
     *
     * @param id the id of the projLog to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/proj-logs/{id}")
    @Timed
    public ResponseEntity<Void> deleteProjLog(@PathVariable Long id) {
        log.debug("REST request to delete ProjLog : {}", id);
        projLogService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
