package com.accenture.java.domain.enumeration;

public enum Action {
	CREATE, UPDATE, DELETE, COMMENT
}
