package com.accenture.java.domain.enumeration;

public enum LogType {
	PROJECT, COMPONENT, RISKISSUE, CLARIFICATION, POINTSHEET, TEST, LOG
}
