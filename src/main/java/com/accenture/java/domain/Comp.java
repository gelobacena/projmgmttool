package com.accenture.java.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Comp.
 */
@Entity
@Table(name = "comp")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Comp implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "budget_at_completion")
    private Integer budget_at_completion;

    @Column(name = "baseline_start")
    private LocalDate baseline_start;

    @Column(name = "baseline_finish")
    private LocalDate baseline_finish;

    @Column(name = "actual_cost")
    private Integer actual_cost;

    @Column(name = "actual_start")
    private LocalDate actual_start;

    @Column(name = "actual_finish")
    private LocalDate actual_finish;

    @Column(name = "developer")
    private String developer;

    @Column(name = "reviewer")
    private String reviewer;

    @Column(name = "component_type")
    private String component_type;

    @Column(name = "status")
    private String status;

    @Column(name = "created_by")
    private String created_by;

    @Column(name = "created_date")
    private LocalDate created_date;

    @Column(name = "modified_by")
    private String modified_by;

    @Column(name = "modified_date")
    private LocalDate modified_date;

    @Column(name = "is_deleted")
    private Integer is_deleted;

    @ManyToOne
    private Project project;

    @OneToMany(mappedBy = "comp")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONE)
    private Set<Clarification> clarifications = new HashSet<>();

    @OneToMany(mappedBy = "comp")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONE)
    private Set<Pointsheet> pointsheets = new HashSet<>();

    @OneToMany(mappedBy = "comp")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONE)
    private Set<UnitTest> unitTests = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Comp name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Comp description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getBudget_at_completion() {
        return budget_at_completion;
    }

    public Comp budget_at_completion(Integer budget_at_completion) {
        this.budget_at_completion = budget_at_completion;
        return this;
    }

    public void setBudget_at_completion(Integer budget_at_completion) {
        this.budget_at_completion = budget_at_completion;
    }

    public LocalDate getBaseline_start() {
        return baseline_start;
    }

    public Comp baseline_start(LocalDate baseline_start) {
        this.baseline_start = baseline_start;
        return this;
    }

    public void setBaseline_start(LocalDate baseline_start) {
        this.baseline_start = baseline_start;
    }

    public LocalDate getBaseline_finish() {
        return baseline_finish;
    }

    public Comp baseline_finish(LocalDate baseline_finish) {
        this.baseline_finish = baseline_finish;
        return this;
    }

    public void setBaseline_finish(LocalDate baseline_finish) {
        this.baseline_finish = baseline_finish;
    }

    public Integer getActual_cost() {
        return actual_cost;
    }

    public Comp actual_cost(Integer actual_cost) {
        this.actual_cost = actual_cost;
        return this;
    }

    public void setActual_cost(Integer actual_cost) {
        this.actual_cost = actual_cost;
    }

    public LocalDate getActual_start() {
        return actual_start;
    }

    public Comp actual_start(LocalDate actual_start) {
        this.actual_start = actual_start;
        return this;
    }

    public void setActual_start(LocalDate actual_start) {
        this.actual_start = actual_start;
    }

    public LocalDate getActual_finish() {
        return actual_finish;
    }

    public Comp actual_finish(LocalDate actual_finish) {
        this.actual_finish = actual_finish;
        return this;
    }

    public void setActual_finish(LocalDate actual_finish) {
        this.actual_finish = actual_finish;
    }

    public String getDeveloper() {
        return developer;
    }

    public Comp developer(String developer) {
        this.developer = developer;
        return this;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    public String getReviewer() {
        return reviewer;
    }

    public Comp reviewer(String reviewer) {
        this.reviewer = reviewer;
        return this;
    }

    public void setReviewer(String reviewer) {
        this.reviewer = reviewer;
    }

    public String getComponent_type() {
        return component_type;
    }

    public Comp component_type(String component_type) {
        this.component_type = component_type;
        return this;
    }

    public void setComponent_type(String component_type) {
        this.component_type = component_type;
    }

    public String getStatus() {
        return status;
    }

    public Comp status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_by() {
        return created_by;
    }

    public Comp created_by(String created_by) {
        this.created_by = created_by;
        return this;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public LocalDate getCreated_date() {
        return created_date;
    }

    public Comp created_date(LocalDate created_date) {
        this.created_date = created_date;
        return this;
    }

    public void setCreated_date(LocalDate created_date) {
        this.created_date = created_date;
    }

    public String getModified_by() {
        return modified_by;
    }

    public Comp modified_by(String modified_by) {
        this.modified_by = modified_by;
        return this;
    }

    public void setModified_by(String modified_by) {
        this.modified_by = modified_by;
    }

    public LocalDate getModified_date() {
        return modified_date;
    }

    public Comp modified_date(LocalDate modified_date) {
        this.modified_date = modified_date;
        return this;
    }

    public void setModified_date(LocalDate modified_date) {
        this.modified_date = modified_date;
    }

    public Integer getIs_deleted() {
        return is_deleted;
    }

    public Comp is_deleted(Integer is_deleted) {
        this.is_deleted = is_deleted;
        return this;
    }

    public void setIs_deleted(Integer is_deleted) {
        this.is_deleted = is_deleted;
    }

    public Project getProject() {
        return project;
    }

    public Comp project(Project project) {
        this.project = project;
        return this;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Set<Clarification> getClarifications() {
        return clarifications;
    }

    public Comp clarifications(Set<Clarification> clarifications) {
        this.clarifications = clarifications;
        return this;
    }

    public Comp addClarification(Clarification clarification) {
        this.clarifications.add(clarification);
        clarification.setComp(this);
        return this;
    }

    public Comp removeClarification(Clarification clarification) {
        this.clarifications.remove(clarification);
        clarification.setComp(null);
        return this;
    }

    public void setClarifications(Set<Clarification> clarifications) {
        this.clarifications = clarifications;
    }

    public Set<Pointsheet> getPointsheets() {
        return pointsheets;
    }

    public Comp pointsheets(Set<Pointsheet> pointsheets) {
        this.pointsheets = pointsheets;
        return this;
    }

    public Comp addPointsheet(Pointsheet pointsheet) {
        this.pointsheets.add(pointsheet);
        pointsheet.setComp(this);
        return this;
    }

    public Comp removePointsheet(Pointsheet pointsheet) {
        this.pointsheets.remove(pointsheet);
        pointsheet.setComp(null);
        return this;
    }

    public void setPointsheets(Set<Pointsheet> pointsheets) {
        this.pointsheets = pointsheets;
    }

    public Set<UnitTest> getUnitTests() {
        return unitTests;
    }

    public Comp unitTests(Set<UnitTest> unitTests) {
        this.unitTests = unitTests;
        return this;
    }

    public Comp addUnitTest(UnitTest unitTest) {
        this.unitTests.add(unitTest);
        unitTest.setComp(this);
        return this;
    }

    public Comp removeUnitTest(UnitTest unitTest) {
        this.unitTests.remove(unitTest);
        unitTest.setComp(null);
        return this;
    }

    public void setUnitTests(Set<UnitTest> unitTests) {
        this.unitTests = unitTests;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Comp comp = (Comp) o;
        if (comp.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), comp.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Comp{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", budget_at_completion='" + getBudget_at_completion() + "'" +
            ", baseline_start='" + getBaseline_start() + "'" +
            ", baseline_finish='" + getBaseline_finish() + "'" +
            ", actual_cost='" + getActual_cost() + "'" +
            ", actual_start='" + getActual_start() + "'" +
            ", actual_finish='" + getActual_finish() + "'" +
            ", developer='" + getDeveloper() + "'" +
            ", reviewer='" + getReviewer() + "'" +
            ", component_type='" + getComponent_type() + "'" +
            ", status='" + getStatus() + "'" +
            ", created_by='" + getCreated_by() + "'" +
            ", created_date='" + getCreated_date() + "'" +
            ", modified_by='" + getModified_by() + "'" +
            ", modified_date='" + getModified_date() + "'" +
            ", is_deleted='" + getIs_deleted() + "'" +
            "}";
    }
}
