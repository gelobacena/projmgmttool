package com.accenture.java.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A RiskAndIssues.
 */
@Entity
@Table(name = "r_and_i")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RiskAndIssues implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "rec_type")
    private String rec_type;

    @Column(name = "description")
    private String description;

    @Column(name = "source")
    private String source;

    @Column(name = "priority")
    private String priority;

    @Column(name = "status")
    private String status;

    @Column(name = "severity")
    private String severity;

    @Column(name = "probability_percent")
    private String probability_percent;

    @Column(name = "mitigation_plan")
    private String mitigation_plan;

    @Column(name = "mitigation_due_date")
    private LocalDate mitigation_due_date;

    @Column(name = "contingency_plan")
    private String contingency_plan;

    @Column(name = "contingency_due_date")
    private LocalDate contingency_due_date;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "created_by")
    private String created_by;

    @Column(name = "created_date")
    private LocalDate created_date;

    @Column(name = "modified_by")
    private String modified_by;

    @Column(name = "modified_date")
    private LocalDate modified_date;

    @Column(name = "is_deleted")
    private Integer is_deleted;

    @Column(name = "resp")
    private String resp;

    @Column(name = "date_occured")
    private LocalDate date_occured;

    @Column(name = "impact")
    private String impact;

    @ManyToOne
    private Project project;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRec_type() {
        return rec_type;
    }

    public RiskAndIssues rec_type(String rec_type) {
        this.rec_type = rec_type;
        return this;
    }

    public void setRec_type(String rec_type) {
        this.rec_type = rec_type;
    }

    public String getDescription() {
        return description;
    }

    public RiskAndIssues description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSource() {
        return source;
    }

    public RiskAndIssues source(String source) {
        this.source = source;
        return this;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getPriority() {
        return priority;
    }

    public RiskAndIssues priority(String priority) {
        this.priority = priority;
        return this;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getStatus() {
        return status;
    }

    public RiskAndIssues status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSeverity() {
        return severity;
    }

    public RiskAndIssues severity(String severity) {
        this.severity = severity;
        return this;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getProbability_percent() {
        return probability_percent;
    }

    public RiskAndIssues probability_percent(String probability_percent) {
        this.probability_percent = probability_percent;
        return this;
    }

    public void setProbability_percent(String probability_percent) {
        this.probability_percent = probability_percent;
    }

    public String getMitigation_plan() {
        return mitigation_plan;
    }

    public RiskAndIssues mitigation_plan(String mitigation_plan) {
        this.mitigation_plan = mitigation_plan;
        return this;
    }

    public void setMitigation_plan(String mitigation_plan) {
        this.mitigation_plan = mitigation_plan;
    }

    public LocalDate getMitigation_due_date() {
        return mitigation_due_date;
    }

    public RiskAndIssues mitigation_due_date(LocalDate mitigation_due_date) {
        this.mitigation_due_date = mitigation_due_date;
        return this;
    }

    public void setMitigation_due_date(LocalDate mitigation_due_date) {
        this.mitigation_due_date = mitigation_due_date;
    }

    public String getContingency_plan() {
        return contingency_plan;
    }

    public RiskAndIssues contingency_plan(String contingency_plan) {
        this.contingency_plan = contingency_plan;
        return this;
    }

    public void setContingency_plan(String contingency_plan) {
        this.contingency_plan = contingency_plan;
    }

    public LocalDate getContingency_due_date() {
        return contingency_due_date;
    }

    public RiskAndIssues contingency_due_date(LocalDate contingency_due_date) {
        this.contingency_due_date = contingency_due_date;
        return this;
    }

    public void setContingency_due_date(LocalDate contingency_due_date) {
        this.contingency_due_date = contingency_due_date;
    }

    public String getRemarks() {
        return remarks;
    }

    public RiskAndIssues remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCreated_by() {
        return created_by;
    }

    public RiskAndIssues created_by(String created_by) {
        this.created_by = created_by;
        return this;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public LocalDate getCreated_date() {
        return created_date;
    }

    public RiskAndIssues created_date(LocalDate created_date) {
        this.created_date = created_date;
        return this;
    }

    public void setCreated_date(LocalDate created_date) {
        this.created_date = created_date;
    }

    public String getModified_by() {
        return modified_by;
    }

    public RiskAndIssues modified_by(String modified_by) {
        this.modified_by = modified_by;
        return this;
    }

    public void setModified_by(String modified_by) {
        this.modified_by = modified_by;
    }

    public LocalDate getModified_date() {
        return modified_date;
    }

    public RiskAndIssues modified_date(LocalDate modified_date) {
        this.modified_date = modified_date;
        return this;
    }

    public void setModified_date(LocalDate modified_date) {
        this.modified_date = modified_date;
    }

    public Integer getIs_deleted() {
        return is_deleted;
    }

    public RiskAndIssues is_deleted(Integer is_deleted) {
        this.is_deleted = is_deleted;
        return this;
    }

    public void setIs_deleted(Integer is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getResp() {
        return resp;
    }

    public RiskAndIssues resp(String resp) {
        this.resp = resp;
        return this;
    }

    public void setResp(String resp) {
        this.resp = resp;
    }

    public LocalDate getDate_occured() {
        return date_occured;
    }

    public RiskAndIssues date_occured(LocalDate date_occured) {
        this.date_occured = date_occured;
        return this;
    }

    public void setDate_occured(LocalDate date_occured) {
        this.date_occured = date_occured;
    }

    public String getImpact() {
        return impact;
    }

    public RiskAndIssues impact(String impact) {
        this.impact = impact;
        return this;
    }

    public void setImpact(String impact) {
        this.impact = impact;
    }

    public Project getProject() {
        return project;
    }

    public RiskAndIssues project(Project project) {
        this.project = project;
        return this;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RiskAndIssues riskAndIssues = (RiskAndIssues) o;
        if (riskAndIssues.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), riskAndIssues.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RiskAndIssues{" +
            "id=" + getId() +
            ", rec_type='" + getRec_type() + "'" +
            ", description='" + getDescription() + "'" +
            ", source='" + getSource() + "'" +
            ", priority='" + getPriority() + "'" +
            ", status='" + getStatus() + "'" +
            ", severity='" + getSeverity() + "'" +
            ", probability_percent='" + getProbability_percent() + "'" +
            ", mitigation_plan='" + getMitigation_plan() + "'" +
            ", mitigation_due_date='" + getMitigation_due_date() + "'" +
            ", contingency_plan='" + getContingency_plan() + "'" +
            ", contingency_due_date='" + getContingency_due_date() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", created_by='" + getCreated_by() + "'" +
            ", created_date='" + getCreated_date() + "'" +
            ", modified_by='" + getModified_by() + "'" +
            ", modified_date='" + getModified_date() + "'" +
            ", is_deleted='" + getIs_deleted() + "'" +
            ", resp='" + getResp() + "'" +
            ", date_occured='" + getDate_occured() + "'" +
            ", impact='" + getImpact() + "'" +
            "}";
    }
}
