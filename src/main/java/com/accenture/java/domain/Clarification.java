package com.accenture.java.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Clarification.
 */
@Entity
@Table(name = "clarification")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Clarification implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "cl_date")
    private LocalDate cl_date;

    @Column(name = "status")
    private String status;

    @Column(name = "priority")
    private String priority;

    @Column(name = "classification")
    private String classification;

    @Column(name = "assigned_to")
    private String assigned_to;

    @Column(name = "description")
    private String description;

    @Column(name = "resolution")
    private String resolution;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "date_closed")
    private LocalDate date_closed;

    @Column(name = "created_by")
    private String created_by;

    @Column(name = "created_date")
    private LocalDate created_date;

    @Column(name = "modified_by")
    private String modified_by;

    @Column(name = "modified_date")
    private LocalDate modified_date;

    @Column(name = "is_deleted")
    private Integer is_deleted;

    @ManyToOne
    private Comp comp;

    @ManyToOne
    private Project project;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getCl_date() {
        return cl_date;
    }

    public Clarification cl_date(LocalDate cl_date) {
        this.cl_date = cl_date;
        return this;
    }

    public void setCl_date(LocalDate cl_date) {
        this.cl_date = cl_date;
    }

    public String getStatus() {
        return status;
    }

    public Clarification status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPriority() {
        return priority;
    }

    public Clarification priority(String priority) {
        this.priority = priority;
        return this;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getClassification() {
        return classification;
    }

    public Clarification classification(String classification) {
        this.classification = classification;
        return this;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public String getAssigned_to() {
        return assigned_to;
    }

    public Clarification assigned_to(String assigned_to) {
        this.assigned_to = assigned_to;
        return this;
    }

    public void setAssigned_to(String assigned_to) {
        this.assigned_to = assigned_to;
    }

    public String getDescription() {
        return description;
    }

    public Clarification description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResolution() {
        return resolution;
    }

    public Clarification resolution(String resolution) {
        this.resolution = resolution;
        return this;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public String getRemarks() {
        return remarks;
    }

    public Clarification remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public LocalDate getDate_closed() {
        return date_closed;
    }

    public Clarification date_closed(LocalDate date_closed) {
        this.date_closed = date_closed;
        return this;
    }

    public void setDate_closed(LocalDate date_closed) {
        this.date_closed = date_closed;
    }

    public String getCreated_by() {
        return created_by;
    }

    public Clarification created_by(String created_by) {
        this.created_by = created_by;
        return this;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public LocalDate getCreated_date() {
        return created_date;
    }

    public Clarification created_date(LocalDate created_date) {
        this.created_date = created_date;
        return this;
    }

    public void setCreated_date(LocalDate created_date) {
        this.created_date = created_date;
    }

    public String getModified_by() {
        return modified_by;
    }

    public Clarification modified_by(String modified_by) {
        this.modified_by = modified_by;
        return this;
    }

    public void setModified_by(String modified_by) {
        this.modified_by = modified_by;
    }

    public LocalDate getModified_date() {
        return modified_date;
    }

    public Clarification modified_date(LocalDate modified_date) {
        this.modified_date = modified_date;
        return this;
    }

    public void setModified_date(LocalDate modified_date) {
        this.modified_date = modified_date;
    }

    public Integer getIs_deleted() {
        return is_deleted;
    }

    public Clarification is_deleted(Integer is_deleted) {
        this.is_deleted = is_deleted;
        return this;
    }

    public void setIs_deleted(Integer is_deleted) {
        this.is_deleted = is_deleted;
    }

    public Comp getComp() {
        return comp;
    }

    public Clarification comp(Comp comp) {
        this.comp = comp;
        return this;
    }

    public void setComp(Comp comp) {
        this.comp = comp;
    }

    public Project getProject() {
        return project;
    }

    public Clarification project(Project project) {
        this.project = project;
        return this;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Clarification clarification = (Clarification) o;
        if (clarification.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clarification.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Clarification{" +
            "id=" + getId() +
            ", cl_date='" + getCl_date() + "'" +
            ", status='" + getStatus() + "'" +
            ", priority='" + getPriority() + "'" +
            ", classification='" + getClassification() + "'" +
            ", assigned_to='" + getAssigned_to() + "'" +
            ", description='" + getDescription() + "'" +
            ", resolution='" + getResolution() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", date_closed='" + getDate_closed() + "'" +
            ", created_by='" + getCreated_by() + "'" +
            ", created_date='" + getCreated_date() + "'" +
            ", modified_by='" + getModified_by() + "'" +
            ", modified_date='" + getModified_date() + "'" +
            ", is_deleted='" + getIs_deleted() + "'" +
            "}";
    }
}
