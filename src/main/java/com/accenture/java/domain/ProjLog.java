package com.accenture.java.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A ProjLog.
 */
@Entity
@Table(name = "proj_log")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProjLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "record_id")
    private Long recordId;

    @Column(name = "log_type")
    private String log_type;

    @Column(name = "description")
    private String description;

    @Column(name = "created_by")
    private String created_by;

    @Column(name = "created_date")
    private LocalDate created_date;

    @Column(name = "action")
    private String action;

    @Column(name = "is_deleted")
    private Integer is_deleted;

    @ManyToOne
    private Project project;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRecord_id() {
        return recordId;
    }

    public ProjLog record_id(Long record_id) {
        this.recordId = record_id;
        return this;
    }

    public void setRecord_id(Long record_id) {
        this.recordId = record_id;
    }

    public String getLog_type() {
        return log_type;
    }

    public ProjLog log_type(String log_type) {
        this.log_type = log_type;
        return this;
    }

    public void setLog_type(String log_type) {
        this.log_type = log_type;
    }

    public String getDescription() {
        return description;
    }

    public ProjLog description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_by() {
        return created_by;
    }

    public ProjLog created_by(String created_by) {
        this.created_by = created_by;
        return this;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public LocalDate getCreated_date() {
        return created_date;
    }

    public ProjLog created_date(LocalDate created_date) {
        this.created_date = created_date;
        return this;
    }

    public void setCreated_date(LocalDate created_date) {
        this.created_date = created_date;
    }

    public String getAction() {
        return action;
    }

    public ProjLog action(String action) {
        this.action = action;
        return this;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Integer getIs_deleted() {
        return is_deleted;
    }

    public ProjLog is_deleted(Integer is_deleted) {
        this.is_deleted = is_deleted;
        return this;
    }

    public void setIs_deleted(Integer is_deleted) {
        this.is_deleted = is_deleted;
    }

    public Project getProject() {
        return project;
    }

    public ProjLog project(Project project) {
        this.project = project;
        return this;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProjLog projLog = (ProjLog) o;
        if (projLog.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), projLog.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProjLog{" +
            "id=" + getId() +
            ", record_id='" + getRecord_id() + "'" +
            ", log_type='" + getLog_type() + "'" +
            ", description='" + getDescription() + "'" +
            ", created_by='" + getCreated_by() + "'" +
            ", created_date='" + getCreated_date() + "'" +
            ", action='" + getAction() + "'" +
            ", is_deleted='" + getIs_deleted() + "'" +
            "}";
    }
}
