package com.accenture.java.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * A Project.
 */
@Entity
@Table(name = "project")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Project implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "team")
    private String team;

    @Column(name = "planned_start")
    private LocalDate planned_start;

    @Column(name = "planned_finish")
    private LocalDate planned_finish;

    @Column(name = "actual_start")
    private LocalDate actual_start;

    @Column(name = "actual_finish")
    private LocalDate actual_finish;

    @Column(name = "created_by")
    private String created_by;

    @Column(name = "created_date")
    private LocalDate created_date;

    @Column(name = "modified_by")
    private String modified_by;

    @Column(name = "modified_date")
    private LocalDate modified_date;

    @Column(name = "status")
    private String status;

    @Column(name = "is_deleted")
    private Integer isDeleted;

    @Column(name = "td_date")
    private LocalDate td_date;

    @Column(name = "bac_td_walkthrough")
    private Integer bac_td_walkthrough;

    @Column(name = "bac_td_analysis")
    private Integer bac_td_analysis;
    
    @Column(name = "bac_utp")
    private Integer bac_utp;

    @Column(name = "bac_ut_execution")
    private Integer bac_ut_execution;

    @Column(name = "bac_utplan_review")
    private Integer bac_utplan_review;

    @OneToMany(mappedBy = "project")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONE)
    private Set<Comp> comps = new HashSet<>();

    @OneToMany(mappedBy = "project")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONE)
    private Set<Clarification> clarifications = new HashSet<>();

    @OneToMany(mappedBy = "project")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONE)
    private Set<Pointsheet> pointsheets = new HashSet<>();

    @OneToMany(mappedBy = "project")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONE)
    private Set<UnitTest> unitTests = new HashSet<>();

    @OneToMany(mappedBy = "project")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONE)
    private Set<RiskAndIssues> riskAndIssues = new HashSet<>();

    @OneToMany(mappedBy = "project")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONE)
    private Set<ProjLog> projLogs = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Project name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeam() {
        return team;
    }

    public Project team(String team) {
        this.team = team;
        return this;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public LocalDate getPlanned_start() {
        return planned_start;
    }

    public Project planned_start(LocalDate planned_start) {
        this.planned_start = planned_start;
        return this;
    }

    public void setPlanned_start(LocalDate planned_start) {
        this.planned_start = planned_start;
    }

    public LocalDate getPlanned_finish() {
        return planned_finish;
    }

    public Project planned_finish(LocalDate planned_finish) {
        this.planned_finish = planned_finish;
        return this;
    }

    public void setPlanned_finish(LocalDate planned_finish) {
        this.planned_finish = planned_finish;
    }

    public LocalDate getActual_start() {
        return actual_start;
    }

    public Project actual_start(LocalDate actual_start) {
        this.actual_start = actual_start;
        return this;
    }

    public void setActual_start(LocalDate actual_start) {
        this.actual_start = actual_start;
    }

    public LocalDate getActual_finish() {
        return actual_finish;
    }

    public Project actual_finish(LocalDate actual_finish) {
        this.actual_finish = actual_finish;
        return this;
    }

    public void setActual_finish(LocalDate actual_finish) {
        this.actual_finish = actual_finish;
    }

    public String getCreated_by() {
        return created_by;
    }

    public Project created_by(String created_by) {
        this.created_by = created_by;
        return this;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public LocalDate getCreated_date() {
        return created_date;
    }

    public Project created_date(LocalDate created_date) {
        this.created_date = created_date;
        return this;
    }

    public void setCreated_date(LocalDate created_date) {
        this.created_date = created_date;
    }

    public String getModified_by() {
        return modified_by;
    }

    public Project modified_by(String modified_by) {
        this.modified_by = modified_by;
        return this;
    }

    public void setModified_by(String modified_by) {
        this.modified_by = modified_by;
    }

    public LocalDate getModified_date() {
        return modified_date;
    }

    public Project modified_date(LocalDate modified_date) {
        this.modified_date = modified_date;
        return this;
    }

    public void setModified_date(LocalDate modified_date) {
        this.modified_date = modified_date;
    }

    public String getStatus() {
        return status;
    }

    public Project status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getIs_deleted() {
        return isDeleted;
    }

    public Project is_deleted(Integer is_deleted) {
        this.isDeleted = is_deleted;
        return this;
    }

    public void setIs_deleted(Integer is_deleted) {
        this.isDeleted = is_deleted;
    }

    public LocalDate getTd_date() {
        return td_date;
    }

    public Project td_date(LocalDate td_date) {
        this.td_date = td_date;
        return this;
    }

    public void setTd_date(LocalDate td_date) {
        this.td_date = td_date;
    }

    public Integer getBac_td_walkthrough() {
        return bac_td_walkthrough;
    }

    public Project bac_td_walkthrough(Integer bac_td_walkthrough) {
        this.bac_td_walkthrough = bac_td_walkthrough;
        return this;
    }

    public void setBac_td_walkthrough(Integer bac_td_walkthrough) {
        this.bac_td_walkthrough = bac_td_walkthrough;
    }

    public Integer getBac_td_analysis() {
        return bac_td_analysis;
    }

    public Project bac_td_analysis(Integer bac_td_analysis) {
        this.bac_td_analysis = bac_td_analysis;
        return this;
    }

    public void setBac_td_analysis(Integer bac_td_analysis) {
        this.bac_td_analysis = bac_td_analysis;
    }
    
    public Integer getBac_utp() {
        return bac_utp;
    }

    public Project bac_utp(Integer bac_utp) {
        this.bac_utp = bac_utp;
        return this;
    }

    public void setBac_utp(Integer bac_utp) {
        this.bac_utp = bac_utp;
    }

    public Integer getBac_ut_execution() {
        return bac_ut_execution;
    }

    public Project bac_ut_execution(Integer bac_ut_execution) {
        this.bac_ut_execution = bac_ut_execution;
        return this;
    }

    public void setBac_ut_execution(Integer bac_ut_execution) {
        this.bac_ut_execution = bac_ut_execution;
    }

    public Integer getBac_utplan_review() {
        return bac_utplan_review;
    }

    public Project bac_utplan_review(Integer bac_utplan_review) {
        this.bac_utplan_review = bac_utplan_review;
        return this;
    }

    public void setBac_utplan_review(Integer bac_utplan_review) {
        this.bac_utplan_review = bac_utplan_review;
    }

    public Set<Comp> getComps() {
        return comps;
    }

    public Project comps(Set<Comp> comps) {
        this.comps = comps;
        return this;
    }

    public Project addComp(Comp comp) {
        this.comps.add(comp);
        comp.setProject(this);
        return this;
    }

    public Project removeComp(Comp comp) {
        this.comps.remove(comp);
        comp.setProject(null);
        return this;
    }

    public void setComps(Set<Comp> comps) {
        this.comps = comps;
    }

    public Set<Clarification> getClarifications() {
        return clarifications;
    }

    public Project clarifications(Set<Clarification> clarifications) {
        this.clarifications = clarifications;
        return this;
    }

    public Project addClarification(Clarification clarification) {
        this.clarifications.add(clarification);
        clarification.setProject(this);
        return this;
    }

    public Project removeClarification(Clarification clarification) {
        this.clarifications.remove(clarification);
        clarification.setProject(null);
        return this;
    }

    public void setClarifications(Set<Clarification> clarifications) {
        this.clarifications = clarifications;
    }

    public Set<Pointsheet> getPointsheets() {
        return pointsheets;
    }

    public Project pointsheets(Set<Pointsheet> pointsheets) {
        this.pointsheets = pointsheets;
        return this;
    }

    public Project addPointsheet(Pointsheet pointsheet) {
        this.pointsheets.add(pointsheet);
        pointsheet.setProject(this);
        return this;
    }

    public Project removePointsheet(Pointsheet pointsheet) {
        this.pointsheets.remove(pointsheet);
        pointsheet.setProject(null);
        return this;
    }

    public void setPointsheets(Set<Pointsheet> pointsheets) {
        this.pointsheets = pointsheets;
    }

    public Set<UnitTest> getUnitTests() {
        return unitTests;
    }

    public Project unitTests(Set<UnitTest> unitTests) {
        this.unitTests = unitTests;
        return this;
    }

    public Project addUnitTest(UnitTest unitTest) {
        this.unitTests.add(unitTest);
        unitTest.setProject(this);
        return this;
    }

    public Project removeUnitTest(UnitTest unitTest) {
        this.unitTests.remove(unitTest);
        unitTest.setProject(null);
        return this;
    }

    public void setUnitTests(Set<UnitTest> unitTests) {
        this.unitTests = unitTests;
    }

    public Set<RiskAndIssues> getRiskAndIssues() {
        return riskAndIssues;
    }

    public Project riskAndIssues(Set<RiskAndIssues> riskAndIssues) {
        this.riskAndIssues = riskAndIssues;
        return this;
    }

    public Project addRiskAndIssues(RiskAndIssues riskAndIssues) {
        this.riskAndIssues.add(riskAndIssues);
        riskAndIssues.setProject(this);
        return this;
    }

    public Project removeRiskAndIssues(RiskAndIssues riskAndIssues) {
        this.riskAndIssues.remove(riskAndIssues);
        riskAndIssues.setProject(null);
        return this;
    }

    public void setRiskAndIssues(Set<RiskAndIssues> riskAndIssues) {
        this.riskAndIssues = riskAndIssues;
    }

    public Set<ProjLog> getProjLogs() {
        return projLogs;
    }

    public Project projLogs(Set<ProjLog> projLogs) {
        this.projLogs = projLogs;
        return this;
    }

    public Project addProjLog(ProjLog projLog) {
        this.projLogs.add(projLog);
        projLog.setProject(this);
        return this;
    }

    public Project removeProjLog(ProjLog projLog) {
        this.projLogs.remove(projLog);
        projLog.setProject(null);
        return this;
    }

    public void setProjLogs(Set<ProjLog> projLogs) {
        this.projLogs = projLogs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Project project = (Project) o;
        if (project.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), project.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Project{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", team='" + getTeam() + "'" +
            ", planned_start='" + getPlanned_start() + "'" +
            ", planned_finish='" + getPlanned_finish() + "'" +
            ", actual_start='" + getActual_start() + "'" +
            ", actual_finish='" + getActual_finish() + "'" +
            ", created_by='" + getCreated_by() + "'" +
            ", created_date='" + getCreated_date() + "'" +
            ", modified_by='" + getModified_by() + "'" +
            ", modified_date='" + getModified_date() + "'" +
            ", status='" + getStatus() + "'" +
            ", is_deleted='" + getIs_deleted() + "'" +
            ", td_date='" + getTd_date() + "'" +
            ", bac_td_walkthrough='" + getBac_td_walkthrough() + "'" +
            ", bac_td_analysis='" + getBac_td_analysis() + "'" +
            ", bac_ut_execution='" + getBac_ut_execution() + "'" +
            ", bac_utplan_review='" + getBac_utplan_review() + "'" +
            "}";
    }
}
