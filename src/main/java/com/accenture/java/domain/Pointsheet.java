package com.accenture.java.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Pointsheet.
 */
@Entity
@Table(name = "pointsheet")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Pointsheet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "pointsheet_type")
    private String pointsheet_type;

    @Column(name = "inspection_point")
    private String inspection_point;

    @Column(name = "subprocess_defect_origin")
    private String subprocess_defect_origin;

    @Column(name = "defect_type")
    private String defect_type;

    @Column(name = "severity")
    private String severity;

    @Column(name = "action_taken")
    private String action_taken;

    @Column(name = "completion_date")
    private LocalDate completion_date;

    @Column(name = "reopen_date")
    private LocalDate reopen_date;

    @Column(name = "status")
    private String status;

    @Column(name = "possible_root_cause")
    private String possible_root_cause;

    @Column(name = "suggestion_for_avoidance")
    private String suggestion_for_avoidance;
    
    @Column(name = "is_deleted")
    private Integer is_deleted;

    @Column(name = "created_by")
    private String created_by;

    @Column(name = "created_date")
    private LocalDate created_date;

    @Column(name = "modified_by")
    private String modified_by;

    @Column(name = "modified_date")
    private LocalDate modified_date;

    @ManyToOne
    private Project project;

    @ManyToOne
    private Comp comp;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPointsheet_type() {
        return pointsheet_type;
    }

    public Pointsheet pointsheet_type(String pointsheet_type) {
        this.pointsheet_type = pointsheet_type;
        return this;
    }

    public void setPointsheet_type(String pointsheet_type) {
        this.pointsheet_type = pointsheet_type;
    }

    public String getInspection_point() {
        return inspection_point;
    }

    public Pointsheet inspection_point(String inspection_point) {
        this.inspection_point = inspection_point;
        return this;
    }

    public void setInspection_point(String inspection_point) {
        this.inspection_point = inspection_point;
    }

    public String getSubprocess_defect_origin() {
        return subprocess_defect_origin;
    }

    public Pointsheet subprocess_defect_origin(String subprocess_defect_origin) {
        this.subprocess_defect_origin = subprocess_defect_origin;
        return this;
    }

    public void setSubprocess_defect_origin(String subprocess_defect_origin) {
        this.subprocess_defect_origin = subprocess_defect_origin;
    }

    public String getDefect_type() {
        return defect_type;
    }

    public Pointsheet defect_type(String defect_type) {
        this.defect_type = defect_type;
        return this;
    }

    public void setDefect_type(String defect_type) {
        this.defect_type = defect_type;
    }

    public String getSeverity() {
        return severity;
    }

    public Pointsheet severity(String severity) {
        this.severity = severity;
        return this;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getAction_taken() {
        return action_taken;
    }

    public Pointsheet action_taken(String action_taken) {
        this.action_taken = action_taken;
        return this;
    }

    public void setAction_taken(String action_taken) {
        this.action_taken = action_taken;
    }

    public LocalDate getCompletion_date() {
        return completion_date;
    }

    public Pointsheet completion_date(LocalDate completion_date) {
        this.completion_date = completion_date;
        return this;
    }

    public void setCompletion_date(LocalDate completion_date) {
        this.completion_date = completion_date;
    }

    public LocalDate getReopen_date() {
        return reopen_date;
    }

    public Pointsheet reopen_date(LocalDate reopen_date) {
        this.reopen_date = reopen_date;
        return this;
    }

    public void setReopen_date(LocalDate reopen_date) {
        this.reopen_date = reopen_date;
    }

    public String getStatus() {
        return status;
    }

    public Pointsheet status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPossible_root_cause() {
        return possible_root_cause;
    }

    public Pointsheet possible_root_cause(String possible_root_cause) {
        this.possible_root_cause = possible_root_cause;
        return this;
    }

    public void setPossible_root_cause(String possible_root_cause) {
        this.possible_root_cause = possible_root_cause;
    }

    public String getSuggestion_for_avoidance() {
        return suggestion_for_avoidance;
    }

    public Pointsheet suggestion_for_avoidance(String suggestion_for_avoidance) {
        this.suggestion_for_avoidance = suggestion_for_avoidance;
        return this;
    }

    public void setSuggestion_for_avoidance(String suggestion_for_avoidance) {
        this.suggestion_for_avoidance = suggestion_for_avoidance;
    }

    public Integer getIs_deleted() {
		return is_deleted;
	}
    
    public Pointsheet is_deleted(Integer is_deleted) {
    	this.is_deleted = is_deleted;
    	return this;
    }

	public void setIs_deleted(Integer is_deleted) {
		this.is_deleted = is_deleted;
	}

	public String getCreated_by() {
        return created_by;
    }

    public Pointsheet created_by(String created_by) {
        this.created_by = created_by;
        return this;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public LocalDate getCreated_date() {
        return created_date;
    }

    public Pointsheet created_date(LocalDate created_date) {
        this.created_date = created_date;
        return this;
    }

    public void setCreated_date(LocalDate created_date) {
        this.created_date = created_date;
    }

    public String getModified_by() {
        return modified_by;
    }

    public Pointsheet modified_by(String modified_by) {
        this.modified_by = modified_by;
        return this;
    }

    public void setModified_by(String modified_by) {
        this.modified_by = modified_by;
    }

    public LocalDate getModified_date() {
        return modified_date;
    }

    public Pointsheet modified_date(LocalDate modified_date) {
        this.modified_date = modified_date;
        return this;
    }

    public void setModified_date(LocalDate modified_date) {
        this.modified_date = modified_date;
    }

    public Project getProject() {
        return project;
    }

    public Pointsheet project(Project project) {
        this.project = project;
        return this;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Comp getComp() {
        return comp;
    }

    public Pointsheet comp(Comp comp) {
        this.comp = comp;
        return this;
    }

    public void setComp(Comp comp) {
        this.comp = comp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pointsheet pointsheet = (Pointsheet) o;
        if (pointsheet.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pointsheet.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Pointsheet{" +
            "id=" + getId() +
            ", pointsheet_type='" + getPointsheet_type() + "'" +
            ", inspection_point='" + getInspection_point() + "'" +
            ", subprocess_defect_origin='" + getSubprocess_defect_origin() + "'" +
            ", defect_type='" + getDefect_type() + "'" +
            ", severity='" + getSeverity() + "'" +
            ", action_taken='" + getAction_taken() + "'" +
            ", completion_date='" + getCompletion_date() + "'" +
            ", reopen_date='" + getReopen_date() + "'" +
            ", status='" + getStatus() + "'" +
            ", possible_root_cause='" + getPossible_root_cause() + "'" +
            ", suggestion_for_avoidance='" + getSuggestion_for_avoidance() + "'" +
            ", created_by='" + getCreated_by() + "'" +
            ", created_date='" + getCreated_date() + "'" +
            ", modified_by='" + getModified_by() + "'" +
            ", modified_date='" + getModified_date() + "'" +
            "}";
    }
}
