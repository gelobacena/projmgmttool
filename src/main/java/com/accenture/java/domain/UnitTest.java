package com.accenture.java.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A UnitTest.
 */
@Entity
@Table(name = "unit_test")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class UnitTest implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "test_scenario")
    private String test_scenario;

    @Column(name = "test_condition")
    private String test_condition;

    @Column(name = "actions")
    private String actions;

    @Column(name = "input_data")
    private String input_data;

    @Column(name = "expected_result")
    private String expected_result;

    @Column(name = "execution_status")
    private String execution_status;

    @Column(name = "execution_date")
    private LocalDate execution_date;

    @Column(name = "evidences")
    private String evidences;

    @Column(name = "scenario_id")
    private Integer scenario_id;

    @Column(name = "condition_id")
    private Integer condition_id;

    @Column(name = "is_deleted")
    private Integer is_deleted;

    @Column(name = "created_by")
    private String created_by;

    @Column(name = "created_date")
    private LocalDate created_date;

    @Column(name = "modified_by")
    private String modified_by;

    @Column(name = "modified_date")
    private LocalDate modified_date;

    @ManyToOne
    private Project project;

    @ManyToOne
    private Comp comp;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTest_scenario() {
        return test_scenario;
    }

    public UnitTest test_scenario(String test_scenario) {
        this.test_scenario = test_scenario;
        return this;
    }

    public void setTest_scenario(String test_scenario) {
        this.test_scenario = test_scenario;
    }

    public String getTest_condition() {
        return test_condition;
    }

    public UnitTest test_condition(String test_condition) {
        this.test_condition = test_condition;
        return this;
    }

    public void setTest_condition(String test_condition) {
        this.test_condition = test_condition;
    }

    public String getActions() {
        return actions;
    }

    public UnitTest actions(String actions) {
        this.actions = actions;
        return this;
    }

    public void setActions(String actions) {
        this.actions = actions;
    }

    public String getInput_data() {
        return input_data;
    }

    public UnitTest input_data(String input_data) {
        this.input_data = input_data;
        return this;
    }

    public void setInput_data(String input_data) {
        this.input_data = input_data;
    }

    public String getExpected_result() {
        return expected_result;
    }

    public UnitTest expected_result(String expected_result) {
        this.expected_result = expected_result;
        return this;
    }

    public void setExpected_result(String expected_result) {
        this.expected_result = expected_result;
    }

    public String getExecution_status() {
        return execution_status;
    }

    public UnitTest execution_status(String execution_status) {
        this.execution_status = execution_status;
        return this;
    }

    public void setExecution_status(String execution_status) {
        this.execution_status = execution_status;
    }

    public LocalDate getExecution_date() {
        return execution_date;
    }

    public UnitTest execution_date(LocalDate execution_date) {
        this.execution_date = execution_date;
        return this;
    }

    public void setExecution_date(LocalDate execution_date) {
        this.execution_date = execution_date;
    }

    public String getEvidences() {
        return evidences;
    }

    public UnitTest evidences(String evidences) {
        this.evidences = evidences;
        return this;
    }

    public void setEvidences(String evidences) {
        this.evidences = evidences;
    }

    public Integer getScenario_id() {
        return scenario_id;
    }

    public UnitTest scenario_id(Integer scenario_id) {
        this.scenario_id = scenario_id;
        return this;
    }

    public void setScenario_id(Integer scenario_id) {
        this.scenario_id = scenario_id;
    }

    public Integer getCondition_id() {
        return condition_id;
    }

    public UnitTest condition_id(Integer condition_id) {
        this.condition_id = condition_id;
        return this;
    }

    public void setCondition_id(Integer condition_id) {
        this.condition_id = condition_id;
    }

    public Integer getIs_deleted() {
        return is_deleted;
    }

    public UnitTest is_deleted(Integer is_deleted) {
        this.is_deleted = is_deleted;
        return this;
    }

    public void setIs_deleted(Integer is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getCreated_by() {
        return created_by;
    }

    public UnitTest created_by(String created_by) {
        this.created_by = created_by;
        return this;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public LocalDate getCreated_date() {
        return created_date;
    }

    public UnitTest created_date(LocalDate created_date) {
        this.created_date = created_date;
        return this;
    }

    public void setCreated_date(LocalDate created_date) {
        this.created_date = created_date;
    }

    public String getModified_by() {
        return modified_by;
    }

    public UnitTest modified_by(String modified_by) {
        this.modified_by = modified_by;
        return this;
    }

    public void setModified_by(String modified_by) {
        this.modified_by = modified_by;
    }

    public LocalDate getModified_date() {
        return modified_date;
    }

    public UnitTest modified_date(LocalDate modified_date) {
        this.modified_date = modified_date;
        return this;
    }

    public void setModified_date(LocalDate modified_date) {
        this.modified_date = modified_date;
    }

    public Project getProject() {
        return project;
    }

    public UnitTest project(Project project) {
        this.project = project;
        return this;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Comp getComp() {
        return comp;
    }

    public UnitTest comp(Comp comp) {
        this.comp = comp;
        return this;
    }

    public void setComp(Comp comp) {
        this.comp = comp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UnitTest unitTest = (UnitTest) o;
        if (unitTest.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), unitTest.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UnitTest{" +
            "id=" + getId() +
            ", test_scenario='" + getTest_scenario() + "'" +
            ", test_condition='" + getTest_condition() + "'" +
            ", actions='" + getActions() + "'" +
            ", input_data='" + getInput_data() + "'" +
            ", expected_result='" + getExpected_result() + "'" +
            ", execution_status='" + getExecution_status() + "'" +
            ", execution_date='" + getExecution_date() + "'" +
            ", evidences='" + getEvidences() + "'" +
            ", scenario_id='" + getScenario_id() + "'" +
            ", condition_id='" + getCondition_id() + "'" +
            ", is_deleted='" + getIs_deleted() + "'" +
            ", created_by='" + getCreated_by() + "'" +
            ", created_date='" + getCreated_date() + "'" +
            ", modified_by='" + getModified_by() + "'" +
            ", modified_date='" + getModified_date() + "'" +
            "}";
    }
}
