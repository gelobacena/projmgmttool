package com.accenture.java.service;

import com.accenture.java.domain.Clarification;
import com.accenture.java.domain.Comp;
import com.accenture.java.domain.Pointsheet;
import com.accenture.java.domain.ProjLog;
import com.accenture.java.domain.UnitTest;
import com.accenture.java.repository.CompRepository;
import com.accenture.java.repository.ProjLogRepository;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Comp.
 */
@Service
@Transactional
public class CompService {

    private final Logger log = LoggerFactory.getLogger(CompService.class);

    private final CompRepository compRepository;
    
    private final ProjLogRepository projLogRepository;

    public CompService(CompRepository compRepository, ProjLogRepository projLogRepository) {
        this.compRepository = compRepository;
        this.projLogRepository = projLogRepository;
    }

    /**
     * Save a comp.
     *
     * @param comp the entity to save
     * @return the persisted entity
     */
    public Comp save(Comp comp) {
        log.debug("Request to save Comp : {}", comp);
        return compRepository.save(comp);
    }

    /**
     *  Get all the comps.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Comp> findAll(Pageable pageable) {
        log.debug("Request to get all Comps");
        return compRepository.findAll(pageable);
    }

    /**
     *  Get one comp by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Comp findOne(Long id) {
        log.debug("Request to get Comp : {}", id);
        return compRepository.findOne(id);
    }
    
    @Transactional(readOnly = true)
    public Set<UnitTest> getTests(Long id) {
        log.debug("Request to get Comp : {}", id);
        return compRepository.findOne(id).getUnitTests();
    }
    
    @Transactional(readOnly = true)
    public Set<Clarification> getClarifications(Long id) {
        log.debug("Request to get Comp : {}", id);
        return compRepository.findOne(id).getClarifications();
    }
    
    @Transactional(readOnly = true)
    public Set<ProjLog> getHistory(Long id) {
        log.debug("Request to get Comp : {}", id);
        return projLogRepository.findByRecordId(id);
    }
    
    @Transactional(readOnly = true)
    public Set<Pointsheet> getReviewPoints(Long id) {
        log.debug("Request to get Comp : {}", id);
        return compRepository.findOne(id).getPointsheets();
    }
    
    @Transactional(readOnly = true)
    public Set<Comp> getTasks(String developer) {
        log.debug("Request to get Tasks : {}", developer);
        return compRepository.findByDeveloper(developer);
    }
    
    @Transactional(readOnly = true)
    public Set<Comp> getReviews(String reviewer) {
        log.debug("Request to get Review Tasks : {}", reviewer);
        return compRepository.findByReviewerAndStatus(reviewer, "For Review");
    }

    /**
     *  Delete the  comp by id.
     *
     *  @param id the id of the entity
     */
    public Comp delete(Long id) {
        log.debug("Request to delete Comp : {}", id);
        Comp comp = compRepository.findOne(id);
        comp.setIs_deleted(1);
        return compRepository.save(comp);
    }
}
