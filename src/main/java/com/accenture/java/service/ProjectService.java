package com.accenture.java.service;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.java.domain.Clarification;
import com.accenture.java.domain.Comp;
import com.accenture.java.domain.Pointsheet;
import com.accenture.java.domain.ProjLog;
import com.accenture.java.domain.Project;
import com.accenture.java.domain.RiskAndIssues;
import com.accenture.java.domain.UnitTest;
import com.accenture.java.repository.ProjectRepository;


/**
 * Service Implementation for managing Project.
 */
@Service
@Transactional
public class ProjectService {

    private final Logger log = LoggerFactory.getLogger(ProjectService.class);

    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    /**
     * Save a project.
     *
     * @param project the entity to save
     * @return the persisted entity
     */
    public Project save(Project project) {
        log.debug("Request to save Project : {}", project);
        return projectRepository.save(project);
    }

    /**
     *  Get all the projects.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Project> findAll(Pageable pageable) {
        log.debug("Request to get all Projects");
        return projectRepository.findByIsDeleted(0, pageable);
    }

    /**
     *  Get one project by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Project findOne(Long id) {
        log.debug("Request to get Project : {}", id);
        return projectRepository.findOne(id);
    }
    
    @Transactional(readOnly = true)
    public Set<Comp> getComponents(Long id) {
    	log.debug("Request to get Project Components {}", id);
    	Project project = findOne(id);
    	return project.getComps();
    }
    
    @Transactional(readOnly = true)
    public Set<UnitTest> getTests(Long id) {
    	log.debug("Request to get Project Components {}", id);
    	Project project = findOne(id);
    	return project.getUnitTests();
    }
    
    @Transactional(readOnly = true)
    public Set<Pointsheet> getReviewPoints(Long id) {
    	log.debug("Request to get Project Components {}", id);
    	Project project = findOne(id);
    	return project.getPointsheets();
    }
    
    @Transactional(readOnly = true)
    public Set<RiskAndIssues> getRisksAndIssues(Long id) {
    	log.debug("Request to get Project Components {}", id);
    	Project project = findOne(id);
    	return project.getRiskAndIssues();
    }
    
    @Transactional(readOnly = true)
    public Set<Clarification> getClarifications(Long id) {
    	log.debug("Request to get Project Components {}", id);
    	Project project = findOne(id);
    	return project.getClarifications();
    }
    
    @Transactional(readOnly = true)
    public Set<ProjLog> getHistory(Long id) {
    	log.debug("Request to get Project Components {}", id);
    	Project project = findOne(id);
    	return project.getProjLogs();
    }
    

    /**
     *  Delete the  project by id.
     *
     *  @param id the id of the entity
     */
    public Project delete(Long id) {
        log.debug("Request to delete Project : {}", id);
        Project p = projectRepository.findOne(id);
        p.setIs_deleted(1);
        return projectRepository.save(p);
    }
}
