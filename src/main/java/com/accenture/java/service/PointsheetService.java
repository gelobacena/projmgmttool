package com.accenture.java.service;

import com.accenture.java.domain.Pointsheet;
import com.accenture.java.domain.ProjLog;
import com.accenture.java.repository.PointsheetRepository;
import com.accenture.java.repository.ProjLogRepository;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Pointsheet.
 */
@Service
@Transactional
public class PointsheetService {

    private final Logger log = LoggerFactory.getLogger(PointsheetService.class);

    private final PointsheetRepository pointsheetRepository;
    
    private final ProjLogRepository projLogRepository;

    public PointsheetService(PointsheetRepository pointsheetRepository, ProjLogRepository projLogRepository) {
        this.pointsheetRepository = pointsheetRepository;
        this.projLogRepository = projLogRepository;
    }

    /**
     * Save a pointsheet.
     *
     * @param pointsheet the entity to save
     * @return the persisted entity
     */
    public Pointsheet save(Pointsheet pointsheet) {
        log.debug("Request to save Pointsheet : {}", pointsheet);
        return pointsheetRepository.save(pointsheet);
    }

    /**
     *  Get all the pointsheets.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Pointsheet> findAll(Pageable pageable) {
        log.debug("Request to get all Pointsheets");
        return pointsheetRepository.findAll(pageable);
    }

    /**
     *  Get one pointsheet by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Pointsheet findOne(Long id) {
        log.debug("Request to get Pointsheet : {}", id);
        return pointsheetRepository.findOne(id);
    }
    
    /**
     *  Get one history by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Set<ProjLog> getHistory(Long id) {
        log.debug("Request to get Pointsheet : {}", id);
        return projLogRepository.findByRecordId(id);
    }

    /**
     *  Delete the  pointsheet by id.
     *
     *  @param id the id of the entity
     */
    public Pointsheet delete(Long id) {
        log.debug("Request to delete Pointsheet : {}", id);
        Pointsheet p = pointsheetRepository.findOne(id);
        p.setIs_deleted(1);
        return pointsheetRepository.save(p);
    }
}
