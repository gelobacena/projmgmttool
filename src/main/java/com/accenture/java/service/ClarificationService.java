package com.accenture.java.service;

import com.accenture.java.domain.Clarification;
import com.accenture.java.domain.ProjLog;
import com.accenture.java.repository.ClarificationRepository;
import com.accenture.java.repository.ProjLogRepository;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Clarification.
 */
@Service
@Transactional
public class ClarificationService {

    private final Logger log = LoggerFactory.getLogger(ClarificationService.class);

    private final ClarificationRepository clarificationRepository;
    
    private final ProjLogRepository projLogRepository;

    public ClarificationService(ClarificationRepository clarificationRepository, ProjLogRepository projLogRepository) {
        this.clarificationRepository = clarificationRepository;
        this.projLogRepository = projLogRepository;
    }

    /**
     * Save a clarification.
     *
     * @param clarification the entity to save
     * @return the persisted entity
     */
    public Clarification save(Clarification clarification) {
        log.debug("Request to save Clarification : {}", clarification);
        return clarificationRepository.save(clarification);
    }

    /**
     *  Get all the clarifications.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Clarification> findAll(Pageable pageable) {
        log.debug("Request to get all Clarifications");
        return clarificationRepository.findAll(pageable);
    }

    /**
     *  Get one clarification by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Clarification findOne(Long id) {
        log.debug("Request to get Clarification : {}", id);
        return clarificationRepository.findOne(id);
    }
    
    @Transactional(readOnly = true)
    public Set<ProjLog> getHistory(Long id) {
        log.debug("Request to get Clarification : {}", id);
        return projLogRepository.findByRecordId(id);
    }

    /**
     *  Delete the  clarification by id.
     *
     *  @param id the id of the entity
     */
    public Clarification delete(Long id) {
        log.debug("Request to delete Clarification : {}", id);
        Clarification cl = clarificationRepository.findOne(id);
        cl.setIs_deleted(1);
        return clarificationRepository.save(cl);
    }
}
