package com.accenture.java.service;

import com.accenture.java.domain.ProjLog;
import com.accenture.java.domain.UnitTest;
import com.accenture.java.repository.ProjLogRepository;
import com.accenture.java.repository.UnitTestRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/**
 * Service Implementation for managing UnitTest.
 */
@Service
@Transactional
public class UnitTestService {

    private final Logger log = LoggerFactory.getLogger(UnitTestService.class);

    private final UnitTestRepository unitTestRepository;
    
    private final ProjLogRepository projLogRepository;

    public UnitTestService(UnitTestRepository unitTestRepository, ProjLogRepository projLogRepository) {
        this.unitTestRepository = unitTestRepository;
        this.projLogRepository = projLogRepository;
    }

    /**
     * Save a unitTest.
     *
     * @param unitTest the entity to save
     * @return the persisted entity
     */
    public UnitTest save(UnitTest unitTest) {
        log.debug("Request to save UnitTest : {}", unitTest);
        return unitTestRepository.save(unitTest);
    }

    /**
     *  Get all the unitTests.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<UnitTest> findAll() {
        log.debug("Request to get all UnitTests");
        return unitTestRepository.findAll();
    }

    /**
     *  Get one unitTest by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public UnitTest findOne(Long id) {
        log.debug("Request to get UnitTest : {}", id);
        return unitTestRepository.findOne(id);
    }

    /**
     *  Delete the  unitTest by id.
     *
     *  @param id the id of the entity
     */
    public UnitTest delete(Long id) {
        log.debug("Request to delete UnitTest : {}", id);
        UnitTest test = unitTestRepository.findOne(id);
        test.setIs_deleted(1);
        return unitTestRepository.save(test);
    }
    
    /**
     *  Get one history by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Set<ProjLog> getHistory(Long id) {
        log.debug("Request to get UnitTest : {}", id);
        return projLogRepository.findByRecordId(id);
    }
}
