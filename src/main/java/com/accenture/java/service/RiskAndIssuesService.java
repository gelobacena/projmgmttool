package com.accenture.java.service;

import com.accenture.java.domain.ProjLog;
import com.accenture.java.domain.RiskAndIssues;
import com.accenture.java.repository.ProjLogRepository;
import com.accenture.java.repository.RiskAndIssuesRepository;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing RiskAndIssues.
 */
@Service
@Transactional
public class RiskAndIssuesService {

    private final Logger log = LoggerFactory.getLogger(RiskAndIssuesService.class);

    private final RiskAndIssuesRepository riskAndIssuesRepository;
    
    private final ProjLogRepository projLogRepository;

    public RiskAndIssuesService(RiskAndIssuesRepository riskAndIssuesRepository, ProjLogRepository projLogRepository) {
        this.riskAndIssuesRepository = riskAndIssuesRepository;
        this.projLogRepository = projLogRepository;
    }

    /**
     * Save a riskAndIssues.
     *
     * @param riskAndIssues the entity to save
     * @return the persisted entity
     */
    public RiskAndIssues save(RiskAndIssues riskAndIssues) {
        log.debug("Request to save RiskAndIssues : {}", riskAndIssues);
        return riskAndIssuesRepository.save(riskAndIssues);
    }

    /**
     *  Get all the riskAndIssues.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RiskAndIssues> findAll(Pageable pageable) {
        log.debug("Request to get all RiskAndIssues");
        return riskAndIssuesRepository.findAll(pageable);
    }

    /**
     *  Get one riskAndIssues by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public RiskAndIssues findOne(Long id) {
        log.debug("Request to get RiskAndIssues : {}", id);
        return riskAndIssuesRepository.findOne(id);
    }
    
    @Transactional(readOnly = true)
    public Set<ProjLog> getHistory(Long id) {
        log.debug("Request to get RiskAndIssues : {}", id);
        return projLogRepository.findByRecordId(id);
    }

    /**
     *  Delete the  riskAndIssues by id.
     *
     *  @param id the id of the entity
     */
    public RiskAndIssues delete(Long id) {
        log.debug("Request to delete RiskAndIssues : {}", id);
        RiskAndIssues ri = riskAndIssuesRepository.findOne(id);
        ri.setIs_deleted(1);
        return riskAndIssuesRepository.save(ri);
    }
}
