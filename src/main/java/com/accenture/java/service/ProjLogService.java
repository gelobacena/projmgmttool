package com.accenture.java.service;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.java.domain.ProjLog;
import com.accenture.java.domain.Project;
import com.accenture.java.domain.enumeration.Action;
import com.accenture.java.domain.enumeration.LogType;
import com.accenture.java.repository.ProjLogRepository;


/**
 * Service Implementation for managing ProjLog.
 */
@Service
@Transactional
public class ProjLogService {

    private final Logger log = LoggerFactory.getLogger(ProjLogService.class);

    private final ProjLogRepository projLogRepository;

    public ProjLogService(ProjLogRepository projLogRepository) {
        this.projLogRepository = projLogRepository;
    }

    /**
     * Save a projLog.
     *
     * @param projLog the entity to save
     * @return the persisted entity
     */
    public ProjLog save(ProjLog projLog) {
        log.debug("Request to save ProjLog : {}", projLog);
        return projLogRepository.save(projLog);
    }

    /**
     *  Get all the projLogs.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProjLog> findAll(Pageable pageable) {
        log.debug("Request to get all ProjLogs");
        return projLogRepository.findAll(pageable);
    }

    /**
     *  Get one projLog by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ProjLog findOne(Long id) {
        log.debug("Request to get ProjLog : {}", id);
        return projLogRepository.findOne(id);
    }

    /**
     *  Delete the  projLog by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ProjLog : {}", id);
        projLogRepository.delete(id);
    }
    
    public void logCreate(Long recordId, String createdBy, LogType logType, Project project) {
    	ProjLog pLog = new ProjLog();
    	pLog.setRecord_id(recordId);
    	pLog.setLog_type(logType.toString());
    	pLog.setDescription(Action.CREATE.toString() + " " + logType.toString());
    	pLog.setCreated_by(createdBy);
    	pLog.setCreated_date(LocalDate.now());
    	pLog.setAction(Action.CREATE.toString());
    	pLog.setIs_deleted(0);
    	pLog.setProject(project);
    	save(pLog);
    }
    
    public void logUpdate(Long recordId, String createdBy, LogType logType, Project project) {
    	ProjLog pLog = new ProjLog();
    	pLog.setRecord_id(recordId);
    	pLog.setLog_type(logType.toString());
    	pLog.setDescription(Action.UPDATE.toString() + " " + logType.toString());
    	pLog.setCreated_by(createdBy);
    	pLog.setCreated_date(LocalDate.now());
    	pLog.setAction(Action.UPDATE.toString());
    	pLog.setIs_deleted(0);
    	pLog.setProject(project);
    	save(pLog);
    }
    
    public void logDelete(Long recordId, String createdBy, LogType logType, Project project) {
    	ProjLog pLog = new ProjLog();
    	pLog.setRecord_id(recordId);
    	pLog.setLog_type(logType.toString());
    	pLog.setDescription(Action.DELETE.toString() + " " + logType.toString());
    	pLog.setCreated_by(createdBy);
    	pLog.setCreated_date(LocalDate.now());
    	pLog.setAction(Action.DELETE.toString());
    	pLog.setIs_deleted(0);
    	pLog.setProject(project);
    	save(pLog);
    }
}
