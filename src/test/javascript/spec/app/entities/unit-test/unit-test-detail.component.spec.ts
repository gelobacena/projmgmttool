/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { QdestroyerTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { UnitTestDetailComponent } from '../../../../../../main/webapp/app/entities/unit-test/unit-test-detail.component';
import { UnitTestService } from '../../../../../../main/webapp/app/entities/unit-test/unit-test.service';
import { UnitTest } from '../../../../../../main/webapp/app/entities/unit-test/unit-test.model';

describe('Component Tests', () => {

    describe('UnitTest Management Detail Component', () => {
        let comp: UnitTestDetailComponent;
        let fixture: ComponentFixture<UnitTestDetailComponent>;
        let service: UnitTestService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [QdestroyerTestModule],
                declarations: [UnitTestDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    UnitTestService,
                    JhiEventManager
                ]
            }).overrideTemplate(UnitTestDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(UnitTestDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(UnitTestService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new UnitTest(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.unitTest).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
