/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { QdestroyerTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { ProjLogDetailComponent } from '../../../../../../main/webapp/app/entities/proj-log/proj-log-detail.component';
import { ProjLogService } from '../../../../../../main/webapp/app/entities/proj-log/proj-log.service';
import { ProjLog } from '../../../../../../main/webapp/app/entities/proj-log/proj-log.model';

describe('Component Tests', () => {

    describe('ProjLog Management Detail Component', () => {
        let comp: ProjLogDetailComponent;
        let fixture: ComponentFixture<ProjLogDetailComponent>;
        let service: ProjLogService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [QdestroyerTestModule],
                declarations: [ProjLogDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    ProjLogService,
                    JhiEventManager
                ]
            }).overrideTemplate(ProjLogDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ProjLogDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProjLogService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new ProjLog(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.projLog).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
