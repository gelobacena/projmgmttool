/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { QdestroyerTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { RiskAndIssuesDetailComponent } from '../../../../../../main/webapp/app/entities/risk-and-issues/risk-and-issues-detail.component';
import { RiskAndIssuesService } from '../../../../../../main/webapp/app/entities/risk-and-issues/risk-and-issues.service';
import { RiskAndIssues } from '../../../../../../main/webapp/app/entities/risk-and-issues/risk-and-issues.model';

describe('Component Tests', () => {

    describe('RiskAndIssues Management Detail Component', () => {
        let comp: RiskAndIssuesDetailComponent;
        let fixture: ComponentFixture<RiskAndIssuesDetailComponent>;
        let service: RiskAndIssuesService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [QdestroyerTestModule],
                declarations: [RiskAndIssuesDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    RiskAndIssuesService,
                    JhiEventManager
                ]
            }).overrideTemplate(RiskAndIssuesDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RiskAndIssuesDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RiskAndIssuesService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new RiskAndIssues(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.riskAndIssues).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
