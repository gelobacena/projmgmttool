/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { QdestroyerTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { PointsheetDetailComponent } from '../../../../../../main/webapp/app/entities/pointsheet/pointsheet-detail.component';
import { PointsheetService } from '../../../../../../main/webapp/app/entities/pointsheet/pointsheet.service';
import { Pointsheet } from '../../../../../../main/webapp/app/entities/pointsheet/pointsheet.model';

describe('Component Tests', () => {

    describe('Pointsheet Management Detail Component', () => {
        let comp: PointsheetDetailComponent;
        let fixture: ComponentFixture<PointsheetDetailComponent>;
        let service: PointsheetService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [QdestroyerTestModule],
                declarations: [PointsheetDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    PointsheetService,
                    JhiEventManager
                ]
            }).overrideTemplate(PointsheetDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PointsheetDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PointsheetService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Pointsheet(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.pointsheet).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
