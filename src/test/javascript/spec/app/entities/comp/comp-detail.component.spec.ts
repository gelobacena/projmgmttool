/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { QdestroyerTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { CompDetailComponent } from '../../../../../../main/webapp/app/entities/comp/comp-detail.component';
import { CompService } from '../../../../../../main/webapp/app/entities/comp/comp.service';
import { Comp } from '../../../../../../main/webapp/app/entities/comp/comp.model';

describe('Component Tests', () => {

    describe('Comp Management Detail Component', () => {
        let comp: CompDetailComponent;
        let fixture: ComponentFixture<CompDetailComponent>;
        let service: CompService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [QdestroyerTestModule],
                declarations: [CompDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    CompService,
                    JhiEventManager
                ]
            }).overrideTemplate(CompDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CompDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CompService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Comp(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.comp).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
