/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { QdestroyerTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { ClarificationDetailComponent } from '../../../../../../main/webapp/app/entities/clarification/clarification-detail.component';
import { ClarificationService } from '../../../../../../main/webapp/app/entities/clarification/clarification.service';
import { Clarification } from '../../../../../../main/webapp/app/entities/clarification/clarification.model';

describe('Component Tests', () => {

    describe('Clarification Management Detail Component', () => {
        let comp: ClarificationDetailComponent;
        let fixture: ComponentFixture<ClarificationDetailComponent>;
        let service: ClarificationService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [QdestroyerTestModule],
                declarations: [ClarificationDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    ClarificationService,
                    JhiEventManager
                ]
            }).overrideTemplate(ClarificationDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ClarificationDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ClarificationService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Clarification(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.clarification).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
