package com.accenture.java.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.java.QdestroyerApp;
import com.accenture.java.domain.UnitTest;
import com.accenture.java.repository.UnitTestRepository;
import com.accenture.java.service.ProjLogService;
import com.accenture.java.service.UnitTestService;
import com.accenture.java.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the UnitTestResource REST controller.
 *
 * @see UnitTestResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = QdestroyerApp.class)
public class UnitTestResourceIntTest {

    private static final String DEFAULT_TEST_SCENARIO = "AAAAAAAAAA";
    private static final String UPDATED_TEST_SCENARIO = "BBBBBBBBBB";

    private static final String DEFAULT_TEST_CONDITION = "AAAAAAAAAA";
    private static final String UPDATED_TEST_CONDITION = "BBBBBBBBBB";

    private static final String DEFAULT_ACTIONS = "AAAAAAAAAA";
    private static final String UPDATED_ACTIONS = "BBBBBBBBBB";

    private static final String DEFAULT_INPUT_DATA = "AAAAAAAAAA";
    private static final String UPDATED_INPUT_DATA = "BBBBBBBBBB";

    private static final String DEFAULT_EXPECTED_RESULT = "AAAAAAAAAA";
    private static final String UPDATED_EXPECTED_RESULT = "BBBBBBBBBB";

    private static final String DEFAULT_EXECUTION_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_EXECUTION_STATUS = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_EXECUTION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_EXECUTION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_EVIDENCES = "AAAAAAAAAA";
    private static final String UPDATED_EVIDENCES = "BBBBBBBBBB";

    private static final Integer DEFAULT_SCENARIO_ID = 1;
    private static final Integer UPDATED_SCENARIO_ID = 2;

    private static final Integer DEFAULT_CONDITION_ID = 1;
    private static final Integer UPDATED_CONDITION_ID = 2;

    private static final Integer DEFAULT_IS_DELETED = 1;
    private static final Integer UPDATED_IS_DELETED = 2;

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_MODIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_MODIFIED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_MODIFIED_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private UnitTestRepository unitTestRepository;

    @Autowired
    private UnitTestService unitTestService;
    
    @Autowired
    private ProjLogService logService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restUnitTestMockMvc;

    private UnitTest unitTest;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        UnitTestResource unitTestResource = new UnitTestResource(unitTestService, logService);
        this.restUnitTestMockMvc = MockMvcBuilders.standaloneSetup(unitTestResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UnitTest createEntity(EntityManager em) {
        UnitTest unitTest = new UnitTest()
            .test_scenario(DEFAULT_TEST_SCENARIO)
            .test_condition(DEFAULT_TEST_CONDITION)
            .actions(DEFAULT_ACTIONS)
            .input_data(DEFAULT_INPUT_DATA)
            .expected_result(DEFAULT_EXPECTED_RESULT)
            .execution_status(DEFAULT_EXECUTION_STATUS)
            .execution_date(DEFAULT_EXECUTION_DATE)
            .evidences(DEFAULT_EVIDENCES)
            .scenario_id(DEFAULT_SCENARIO_ID)
            .condition_id(DEFAULT_CONDITION_ID)
            .is_deleted(DEFAULT_IS_DELETED)
            .created_by(DEFAULT_CREATED_BY)
            .created_date(DEFAULT_CREATED_DATE)
            .modified_by(DEFAULT_MODIFIED_BY)
            .modified_date(DEFAULT_MODIFIED_DATE);
        return unitTest;
    }

    @Before
    public void initTest() {
        unitTest = createEntity(em);
    }

    @Test
    @Transactional
    public void createUnitTest() throws Exception {
        int databaseSizeBeforeCreate = unitTestRepository.findAll().size();

        // Create the UnitTest
        restUnitTestMockMvc.perform(post("/api/unit-tests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(unitTest)))
            .andExpect(status().isCreated());

        // Validate the UnitTest in the database
        List<UnitTest> unitTestList = unitTestRepository.findAll();
        assertThat(unitTestList).hasSize(databaseSizeBeforeCreate + 1);
        UnitTest testUnitTest = unitTestList.get(unitTestList.size() - 1);
        assertThat(testUnitTest.getTest_scenario()).isEqualTo(DEFAULT_TEST_SCENARIO);
        assertThat(testUnitTest.getTest_condition()).isEqualTo(DEFAULT_TEST_CONDITION);
        assertThat(testUnitTest.getActions()).isEqualTo(DEFAULT_ACTIONS);
        assertThat(testUnitTest.getInput_data()).isEqualTo(DEFAULT_INPUT_DATA);
        assertThat(testUnitTest.getExpected_result()).isEqualTo(DEFAULT_EXPECTED_RESULT);
        assertThat(testUnitTest.getExecution_status()).isEqualTo(DEFAULT_EXECUTION_STATUS);
        assertThat(testUnitTest.getExecution_date()).isEqualTo(DEFAULT_EXECUTION_DATE);
        assertThat(testUnitTest.getEvidences()).isEqualTo(DEFAULT_EVIDENCES);
        assertThat(testUnitTest.getScenario_id()).isEqualTo(DEFAULT_SCENARIO_ID);
        assertThat(testUnitTest.getCondition_id()).isEqualTo(DEFAULT_CONDITION_ID);
        assertThat(testUnitTest.getIs_deleted()).isEqualTo(DEFAULT_IS_DELETED);
        assertThat(testUnitTest.getCreated_by()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testUnitTest.getCreated_date()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testUnitTest.getModified_by()).isEqualTo(DEFAULT_MODIFIED_BY);
        assertThat(testUnitTest.getModified_date()).isEqualTo(DEFAULT_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void createUnitTestWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = unitTestRepository.findAll().size();

        // Create the UnitTest with an existing ID
        unitTest.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUnitTestMockMvc.perform(post("/api/unit-tests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(unitTest)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<UnitTest> unitTestList = unitTestRepository.findAll();
        assertThat(unitTestList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllUnitTests() throws Exception {
        // Initialize the database
        unitTestRepository.saveAndFlush(unitTest);

        // Get all the unitTestList
        restUnitTestMockMvc.perform(get("/api/unit-tests?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(unitTest.getId().intValue())))
            .andExpect(jsonPath("$.[*].test_scenario").value(hasItem(DEFAULT_TEST_SCENARIO.toString())))
            .andExpect(jsonPath("$.[*].test_condition").value(hasItem(DEFAULT_TEST_CONDITION.toString())))
            .andExpect(jsonPath("$.[*].actions").value(hasItem(DEFAULT_ACTIONS.toString())))
            .andExpect(jsonPath("$.[*].input_data").value(hasItem(DEFAULT_INPUT_DATA.toString())))
            .andExpect(jsonPath("$.[*].expected_result").value(hasItem(DEFAULT_EXPECTED_RESULT.toString())))
            .andExpect(jsonPath("$.[*].execution_status").value(hasItem(DEFAULT_EXECUTION_STATUS.toString())))
            .andExpect(jsonPath("$.[*].execution_date").value(hasItem(DEFAULT_EXECUTION_DATE.toString())))
            .andExpect(jsonPath("$.[*].evidences").value(hasItem(DEFAULT_EVIDENCES.toString())))
            .andExpect(jsonPath("$.[*].scenario_id").value(hasItem(DEFAULT_SCENARIO_ID)))
            .andExpect(jsonPath("$.[*].condition_id").value(hasItem(DEFAULT_CONDITION_ID)))
            .andExpect(jsonPath("$.[*].is_deleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].created_by").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].created_date").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].modified_by").value(hasItem(DEFAULT_MODIFIED_BY.toString())))
            .andExpect(jsonPath("$.[*].modified_date").value(hasItem(DEFAULT_MODIFIED_DATE.toString())));
    }

    @Test
    @Transactional
    public void getUnitTest() throws Exception {
        // Initialize the database
        unitTestRepository.saveAndFlush(unitTest);

        // Get the unitTest
        restUnitTestMockMvc.perform(get("/api/unit-tests/{id}", unitTest.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(unitTest.getId().intValue()))
            .andExpect(jsonPath("$.test_scenario").value(DEFAULT_TEST_SCENARIO.toString()))
            .andExpect(jsonPath("$.test_condition").value(DEFAULT_TEST_CONDITION.toString()))
            .andExpect(jsonPath("$.actions").value(DEFAULT_ACTIONS.toString()))
            .andExpect(jsonPath("$.input_data").value(DEFAULT_INPUT_DATA.toString()))
            .andExpect(jsonPath("$.expected_result").value(DEFAULT_EXPECTED_RESULT.toString()))
            .andExpect(jsonPath("$.execution_status").value(DEFAULT_EXECUTION_STATUS.toString()))
            .andExpect(jsonPath("$.execution_date").value(DEFAULT_EXECUTION_DATE.toString()))
            .andExpect(jsonPath("$.evidences").value(DEFAULT_EVIDENCES.toString()))
            .andExpect(jsonPath("$.scenario_id").value(DEFAULT_SCENARIO_ID))
            .andExpect(jsonPath("$.condition_id").value(DEFAULT_CONDITION_ID))
            .andExpect(jsonPath("$.is_deleted").value(DEFAULT_IS_DELETED))
            .andExpect(jsonPath("$.created_by").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.created_date").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.modified_by").value(DEFAULT_MODIFIED_BY.toString()))
            .andExpect(jsonPath("$.modified_date").value(DEFAULT_MODIFIED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingUnitTest() throws Exception {
        // Get the unitTest
        restUnitTestMockMvc.perform(get("/api/unit-tests/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUnitTest() throws Exception {
        // Initialize the database
        unitTestService.save(unitTest);

        int databaseSizeBeforeUpdate = unitTestRepository.findAll().size();

        // Update the unitTest
        UnitTest updatedUnitTest = unitTestRepository.findOne(unitTest.getId());
        updatedUnitTest
            .test_scenario(UPDATED_TEST_SCENARIO)
            .test_condition(UPDATED_TEST_CONDITION)
            .actions(UPDATED_ACTIONS)
            .input_data(UPDATED_INPUT_DATA)
            .expected_result(UPDATED_EXPECTED_RESULT)
            .execution_status(UPDATED_EXECUTION_STATUS)
            .execution_date(UPDATED_EXECUTION_DATE)
            .evidences(UPDATED_EVIDENCES)
            .scenario_id(UPDATED_SCENARIO_ID)
            .condition_id(UPDATED_CONDITION_ID)
            .is_deleted(UPDATED_IS_DELETED)
            .created_by(UPDATED_CREATED_BY)
            .created_date(UPDATED_CREATED_DATE)
            .modified_by(UPDATED_MODIFIED_BY)
            .modified_date(UPDATED_MODIFIED_DATE);

        restUnitTestMockMvc.perform(put("/api/unit-tests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedUnitTest)))
            .andExpect(status().isOk());

        // Validate the UnitTest in the database
        List<UnitTest> unitTestList = unitTestRepository.findAll();
        assertThat(unitTestList).hasSize(databaseSizeBeforeUpdate);
        UnitTest testUnitTest = unitTestList.get(unitTestList.size() - 1);
        assertThat(testUnitTest.getTest_scenario()).isEqualTo(UPDATED_TEST_SCENARIO);
        assertThat(testUnitTest.getTest_condition()).isEqualTo(UPDATED_TEST_CONDITION);
        assertThat(testUnitTest.getActions()).isEqualTo(UPDATED_ACTIONS);
        assertThat(testUnitTest.getInput_data()).isEqualTo(UPDATED_INPUT_DATA);
        assertThat(testUnitTest.getExpected_result()).isEqualTo(UPDATED_EXPECTED_RESULT);
        assertThat(testUnitTest.getExecution_status()).isEqualTo(UPDATED_EXECUTION_STATUS);
        assertThat(testUnitTest.getExecution_date()).isEqualTo(UPDATED_EXECUTION_DATE);
        assertThat(testUnitTest.getEvidences()).isEqualTo(UPDATED_EVIDENCES);
        assertThat(testUnitTest.getScenario_id()).isEqualTo(UPDATED_SCENARIO_ID);
        assertThat(testUnitTest.getCondition_id()).isEqualTo(UPDATED_CONDITION_ID);
        assertThat(testUnitTest.getIs_deleted()).isEqualTo(UPDATED_IS_DELETED);
        assertThat(testUnitTest.getCreated_by()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testUnitTest.getCreated_date()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testUnitTest.getModified_by()).isEqualTo(UPDATED_MODIFIED_BY);
        assertThat(testUnitTest.getModified_date()).isEqualTo(UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingUnitTest() throws Exception {
        int databaseSizeBeforeUpdate = unitTestRepository.findAll().size();

        // Create the UnitTest

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restUnitTestMockMvc.perform(put("/api/unit-tests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(unitTest)))
            .andExpect(status().isCreated());

        // Validate the UnitTest in the database
        List<UnitTest> unitTestList = unitTestRepository.findAll();
        assertThat(unitTestList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteUnitTest() throws Exception {
        // Initialize the database
        unitTestService.save(unitTest);

        int databaseSizeBeforeDelete = unitTestRepository.findAll().size();

        // Get the unitTest
        restUnitTestMockMvc.perform(delete("/api/unit-tests/{id}", unitTest.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<UnitTest> unitTestList = unitTestRepository.findAll();
        assertThat(unitTestList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UnitTest.class);
        UnitTest unitTest1 = new UnitTest();
        unitTest1.setId(1L);
        UnitTest unitTest2 = new UnitTest();
        unitTest2.setId(unitTest1.getId());
        assertThat(unitTest1).isEqualTo(unitTest2);
        unitTest2.setId(2L);
        assertThat(unitTest1).isNotEqualTo(unitTest2);
        unitTest1.setId(null);
        assertThat(unitTest1).isNotEqualTo(unitTest2);
    }
}
