package com.accenture.java.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.java.QdestroyerApp;
import com.accenture.java.domain.Pointsheet;
import com.accenture.java.repository.PointsheetRepository;
import com.accenture.java.service.PointsheetService;
import com.accenture.java.service.ProjLogService;
import com.accenture.java.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the PointsheetResource REST controller.
 *
 * @see PointsheetResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = QdestroyerApp.class)
public class PointsheetResourceIntTest {

    private static final String DEFAULT_POINTSHEET_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_POINTSHEET_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_INSPECTION_POINT = "AAAAAAAAAA";
    private static final String UPDATED_INSPECTION_POINT = "BBBBBBBBBB";

    private static final String DEFAULT_SUBPROCESS_DEFECT_ORIGIN = "AAAAAAAAAA";
    private static final String UPDATED_SUBPROCESS_DEFECT_ORIGIN = "BBBBBBBBBB";

    private static final String DEFAULT_DEFECT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_DEFECT_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_SEVERITY = "AAAAAAAAAA";
    private static final String UPDATED_SEVERITY = "BBBBBBBBBB";

    private static final String DEFAULT_ACTION_TAKEN = "AAAAAAAAAA";
    private static final String UPDATED_ACTION_TAKEN = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_COMPLETION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_COMPLETION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_REOPEN_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_REOPEN_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_POSSIBLE_ROOT_CAUSE = "AAAAAAAAAA";
    private static final String UPDATED_POSSIBLE_ROOT_CAUSE = "BBBBBBBBBB";

    private static final String DEFAULT_SUGGESTION_FOR_AVOIDANCE = "AAAAAAAAAA";
    private static final String UPDATED_SUGGESTION_FOR_AVOIDANCE = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_MODIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_MODIFIED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_MODIFIED_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private PointsheetRepository pointsheetRepository;

    @Autowired
    private PointsheetService pointsheetService;
    
    @Autowired
    private ProjLogService logService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPointsheetMockMvc;

    private Pointsheet pointsheet;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PointsheetResource pointsheetResource = new PointsheetResource(pointsheetService, logService);
        this.restPointsheetMockMvc = MockMvcBuilders.standaloneSetup(pointsheetResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pointsheet createEntity(EntityManager em) {
        Pointsheet pointsheet = new Pointsheet()
            .pointsheet_type(DEFAULT_POINTSHEET_TYPE)
            .inspection_point(DEFAULT_INSPECTION_POINT)
            .subprocess_defect_origin(DEFAULT_SUBPROCESS_DEFECT_ORIGIN)
            .defect_type(DEFAULT_DEFECT_TYPE)
            .severity(DEFAULT_SEVERITY)
            .action_taken(DEFAULT_ACTION_TAKEN)
            .completion_date(DEFAULT_COMPLETION_DATE)
            .reopen_date(DEFAULT_REOPEN_DATE)
            .status(DEFAULT_STATUS)
            .possible_root_cause(DEFAULT_POSSIBLE_ROOT_CAUSE)
            .suggestion_for_avoidance(DEFAULT_SUGGESTION_FOR_AVOIDANCE)
            .created_by(DEFAULT_CREATED_BY)
            .created_date(DEFAULT_CREATED_DATE)
            .modified_by(DEFAULT_MODIFIED_BY)
            .modified_date(DEFAULT_MODIFIED_DATE);
        return pointsheet;
    }

    @Before
    public void initTest() {
        pointsheet = createEntity(em);
    }

    @Test
    @Transactional
    public void createPointsheet() throws Exception {
        int databaseSizeBeforeCreate = pointsheetRepository.findAll().size();

        // Create the Pointsheet
        restPointsheetMockMvc.perform(post("/api/pointsheets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pointsheet)))
            .andExpect(status().isCreated());

        // Validate the Pointsheet in the database
        List<Pointsheet> pointsheetList = pointsheetRepository.findAll();
        assertThat(pointsheetList).hasSize(databaseSizeBeforeCreate + 1);
        Pointsheet testPointsheet = pointsheetList.get(pointsheetList.size() - 1);
        assertThat(testPointsheet.getPointsheet_type()).isEqualTo(DEFAULT_POINTSHEET_TYPE);
        assertThat(testPointsheet.getInspection_point()).isEqualTo(DEFAULT_INSPECTION_POINT);
        assertThat(testPointsheet.getSubprocess_defect_origin()).isEqualTo(DEFAULT_SUBPROCESS_DEFECT_ORIGIN);
        assertThat(testPointsheet.getDefect_type()).isEqualTo(DEFAULT_DEFECT_TYPE);
        assertThat(testPointsheet.getSeverity()).isEqualTo(DEFAULT_SEVERITY);
        assertThat(testPointsheet.getAction_taken()).isEqualTo(DEFAULT_ACTION_TAKEN);
        assertThat(testPointsheet.getCompletion_date()).isEqualTo(DEFAULT_COMPLETION_DATE);
        assertThat(testPointsheet.getReopen_date()).isEqualTo(DEFAULT_REOPEN_DATE);
        assertThat(testPointsheet.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testPointsheet.getPossible_root_cause()).isEqualTo(DEFAULT_POSSIBLE_ROOT_CAUSE);
        assertThat(testPointsheet.getSuggestion_for_avoidance()).isEqualTo(DEFAULT_SUGGESTION_FOR_AVOIDANCE);
        assertThat(testPointsheet.getCreated_by()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testPointsheet.getCreated_date()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testPointsheet.getModified_by()).isEqualTo(DEFAULT_MODIFIED_BY);
        assertThat(testPointsheet.getModified_date()).isEqualTo(DEFAULT_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void createPointsheetWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pointsheetRepository.findAll().size();

        // Create the Pointsheet with an existing ID
        pointsheet.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPointsheetMockMvc.perform(post("/api/pointsheets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pointsheet)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Pointsheet> pointsheetList = pointsheetRepository.findAll();
        assertThat(pointsheetList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPointsheets() throws Exception {
        // Initialize the database
        pointsheetRepository.saveAndFlush(pointsheet);

        // Get all the pointsheetList
        restPointsheetMockMvc.perform(get("/api/pointsheets?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pointsheet.getId().intValue())))
            .andExpect(jsonPath("$.[*].pointsheet_type").value(hasItem(DEFAULT_POINTSHEET_TYPE.toString())))
            .andExpect(jsonPath("$.[*].inspection_point").value(hasItem(DEFAULT_INSPECTION_POINT.toString())))
            .andExpect(jsonPath("$.[*].subprocess_defect_origin").value(hasItem(DEFAULT_SUBPROCESS_DEFECT_ORIGIN.toString())))
            .andExpect(jsonPath("$.[*].defect_type").value(hasItem(DEFAULT_DEFECT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].severity").value(hasItem(DEFAULT_SEVERITY.toString())))
            .andExpect(jsonPath("$.[*].action_taken").value(hasItem(DEFAULT_ACTION_TAKEN.toString())))
            .andExpect(jsonPath("$.[*].completion_date").value(hasItem(DEFAULT_COMPLETION_DATE.toString())))
            .andExpect(jsonPath("$.[*].reopen_date").value(hasItem(DEFAULT_REOPEN_DATE.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].possible_root_cause").value(hasItem(DEFAULT_POSSIBLE_ROOT_CAUSE.toString())))
            .andExpect(jsonPath("$.[*].suggestion_for_avoidance").value(hasItem(DEFAULT_SUGGESTION_FOR_AVOIDANCE.toString())))
            .andExpect(jsonPath("$.[*].created_by").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].created_date").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].modified_by").value(hasItem(DEFAULT_MODIFIED_BY.toString())))
            .andExpect(jsonPath("$.[*].modified_date").value(hasItem(DEFAULT_MODIFIED_DATE.toString())));
    }

    @Test
    @Transactional
    public void getPointsheet() throws Exception {
        // Initialize the database
        pointsheetRepository.saveAndFlush(pointsheet);

        // Get the pointsheet
        restPointsheetMockMvc.perform(get("/api/pointsheets/{id}", pointsheet.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pointsheet.getId().intValue()))
            .andExpect(jsonPath("$.pointsheet_type").value(DEFAULT_POINTSHEET_TYPE.toString()))
            .andExpect(jsonPath("$.inspection_point").value(DEFAULT_INSPECTION_POINT.toString()))
            .andExpect(jsonPath("$.subprocess_defect_origin").value(DEFAULT_SUBPROCESS_DEFECT_ORIGIN.toString()))
            .andExpect(jsonPath("$.defect_type").value(DEFAULT_DEFECT_TYPE.toString()))
            .andExpect(jsonPath("$.severity").value(DEFAULT_SEVERITY.toString()))
            .andExpect(jsonPath("$.action_taken").value(DEFAULT_ACTION_TAKEN.toString()))
            .andExpect(jsonPath("$.completion_date").value(DEFAULT_COMPLETION_DATE.toString()))
            .andExpect(jsonPath("$.reopen_date").value(DEFAULT_REOPEN_DATE.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.possible_root_cause").value(DEFAULT_POSSIBLE_ROOT_CAUSE.toString()))
            .andExpect(jsonPath("$.suggestion_for_avoidance").value(DEFAULT_SUGGESTION_FOR_AVOIDANCE.toString()))
            .andExpect(jsonPath("$.created_by").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.created_date").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.modified_by").value(DEFAULT_MODIFIED_BY.toString()))
            .andExpect(jsonPath("$.modified_date").value(DEFAULT_MODIFIED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPointsheet() throws Exception {
        // Get the pointsheet
        restPointsheetMockMvc.perform(get("/api/pointsheets/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePointsheet() throws Exception {
        // Initialize the database
        pointsheetService.save(pointsheet);

        int databaseSizeBeforeUpdate = pointsheetRepository.findAll().size();

        // Update the pointsheet
        Pointsheet updatedPointsheet = pointsheetRepository.findOne(pointsheet.getId());
        updatedPointsheet
            .pointsheet_type(UPDATED_POINTSHEET_TYPE)
            .inspection_point(UPDATED_INSPECTION_POINT)
            .subprocess_defect_origin(UPDATED_SUBPROCESS_DEFECT_ORIGIN)
            .defect_type(UPDATED_DEFECT_TYPE)
            .severity(UPDATED_SEVERITY)
            .action_taken(UPDATED_ACTION_TAKEN)
            .completion_date(UPDATED_COMPLETION_DATE)
            .reopen_date(UPDATED_REOPEN_DATE)
            .status(UPDATED_STATUS)
            .possible_root_cause(UPDATED_POSSIBLE_ROOT_CAUSE)
            .suggestion_for_avoidance(UPDATED_SUGGESTION_FOR_AVOIDANCE)
            .created_by(UPDATED_CREATED_BY)
            .created_date(UPDATED_CREATED_DATE)
            .modified_by(UPDATED_MODIFIED_BY)
            .modified_date(UPDATED_MODIFIED_DATE);

        restPointsheetMockMvc.perform(put("/api/pointsheets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPointsheet)))
            .andExpect(status().isOk());

        // Validate the Pointsheet in the database
        List<Pointsheet> pointsheetList = pointsheetRepository.findAll();
        assertThat(pointsheetList).hasSize(databaseSizeBeforeUpdate);
        Pointsheet testPointsheet = pointsheetList.get(pointsheetList.size() - 1);
        assertThat(testPointsheet.getPointsheet_type()).isEqualTo(UPDATED_POINTSHEET_TYPE);
        assertThat(testPointsheet.getInspection_point()).isEqualTo(UPDATED_INSPECTION_POINT);
        assertThat(testPointsheet.getSubprocess_defect_origin()).isEqualTo(UPDATED_SUBPROCESS_DEFECT_ORIGIN);
        assertThat(testPointsheet.getDefect_type()).isEqualTo(UPDATED_DEFECT_TYPE);
        assertThat(testPointsheet.getSeverity()).isEqualTo(UPDATED_SEVERITY);
        assertThat(testPointsheet.getAction_taken()).isEqualTo(UPDATED_ACTION_TAKEN);
        assertThat(testPointsheet.getCompletion_date()).isEqualTo(UPDATED_COMPLETION_DATE);
        assertThat(testPointsheet.getReopen_date()).isEqualTo(UPDATED_REOPEN_DATE);
        assertThat(testPointsheet.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testPointsheet.getPossible_root_cause()).isEqualTo(UPDATED_POSSIBLE_ROOT_CAUSE);
        assertThat(testPointsheet.getSuggestion_for_avoidance()).isEqualTo(UPDATED_SUGGESTION_FOR_AVOIDANCE);
        assertThat(testPointsheet.getCreated_by()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testPointsheet.getCreated_date()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testPointsheet.getModified_by()).isEqualTo(UPDATED_MODIFIED_BY);
        assertThat(testPointsheet.getModified_date()).isEqualTo(UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingPointsheet() throws Exception {
        int databaseSizeBeforeUpdate = pointsheetRepository.findAll().size();

        // Create the Pointsheet

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPointsheetMockMvc.perform(put("/api/pointsheets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pointsheet)))
            .andExpect(status().isCreated());

        // Validate the Pointsheet in the database
        List<Pointsheet> pointsheetList = pointsheetRepository.findAll();
        assertThat(pointsheetList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePointsheet() throws Exception {
        // Initialize the database
        pointsheetService.save(pointsheet);

        int databaseSizeBeforeDelete = pointsheetRepository.findAll().size();

        // Get the pointsheet
        restPointsheetMockMvc.perform(delete("/api/pointsheets/{id}", pointsheet.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Pointsheet> pointsheetList = pointsheetRepository.findAll();
        assertThat(pointsheetList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Pointsheet.class);
        Pointsheet pointsheet1 = new Pointsheet();
        pointsheet1.setId(1L);
        Pointsheet pointsheet2 = new Pointsheet();
        pointsheet2.setId(pointsheet1.getId());
        assertThat(pointsheet1).isEqualTo(pointsheet2);
        pointsheet2.setId(2L);
        assertThat(pointsheet1).isNotEqualTo(pointsheet2);
        pointsheet1.setId(null);
        assertThat(pointsheet1).isNotEqualTo(pointsheet2);
    }
}
