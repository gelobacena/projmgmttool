package com.accenture.java.web.rest;

import com.accenture.java.QdestroyerApp;

import com.accenture.java.domain.ProjLog;
import com.accenture.java.repository.ProjLogRepository;
import com.accenture.java.service.ProjLogService;
import com.accenture.java.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ProjLogResource REST controller.
 *
 * @see ProjLogResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = QdestroyerApp.class)
public class ProjLogResourceIntTest {

    private static final Long DEFAULT_RECORD_ID = 1L;
    private static final Long UPDATED_RECORD_ID = 2L;

    private static final String DEFAULT_LOG_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_LOG_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_ACTION = "AAAAAAAAAA";
    private static final String UPDATED_ACTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_IS_DELETED = 1;
    private static final Integer UPDATED_IS_DELETED = 2;

    @Autowired
    private ProjLogRepository projLogRepository;

    @Autowired
    private ProjLogService projLogService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restProjLogMockMvc;

    private ProjLog projLog;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ProjLogResource projLogResource = new ProjLogResource(projLogService);
        this.restProjLogMockMvc = MockMvcBuilders.standaloneSetup(projLogResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProjLog createEntity(EntityManager em) {
        ProjLog projLog = new ProjLog()
            .record_id(DEFAULT_RECORD_ID)
            .log_type(DEFAULT_LOG_TYPE)
            .description(DEFAULT_DESCRIPTION)
            .created_by(DEFAULT_CREATED_BY)
            .created_date(DEFAULT_CREATED_DATE)
            .action(DEFAULT_ACTION)
            .is_deleted(DEFAULT_IS_DELETED);
        return projLog;
    }

    @Before
    public void initTest() {
        projLog = createEntity(em);
    }

    @Test
    @Transactional
    public void createProjLog() throws Exception {
        int databaseSizeBeforeCreate = projLogRepository.findAll().size();

        // Create the ProjLog
        restProjLogMockMvc.perform(post("/api/proj-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(projLog)))
            .andExpect(status().isCreated());

        // Validate the ProjLog in the database
        List<ProjLog> projLogList = projLogRepository.findAll();
        assertThat(projLogList).hasSize(databaseSizeBeforeCreate + 1);
        ProjLog testProjLog = projLogList.get(projLogList.size() - 1);
        assertThat(testProjLog.getRecord_id()).isEqualTo(DEFAULT_RECORD_ID);
        assertThat(testProjLog.getLog_type()).isEqualTo(DEFAULT_LOG_TYPE);
        assertThat(testProjLog.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testProjLog.getCreated_by()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testProjLog.getCreated_date()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testProjLog.getAction()).isEqualTo(DEFAULT_ACTION);
        assertThat(testProjLog.getIs_deleted()).isEqualTo(DEFAULT_IS_DELETED);
    }

    @Test
    @Transactional
    public void createProjLogWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = projLogRepository.findAll().size();

        // Create the ProjLog with an existing ID
        projLog.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProjLogMockMvc.perform(post("/api/proj-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(projLog)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<ProjLog> projLogList = projLogRepository.findAll();
        assertThat(projLogList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllProjLogs() throws Exception {
        // Initialize the database
        projLogRepository.saveAndFlush(projLog);

        // Get all the projLogList
        restProjLogMockMvc.perform(get("/api/proj-logs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(projLog.getId().intValue())))
            .andExpect(jsonPath("$.[*].record_id").value(hasItem(DEFAULT_RECORD_ID)))
            .andExpect(jsonPath("$.[*].log_type").value(hasItem(DEFAULT_LOG_TYPE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].created_by").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].created_date").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].action").value(hasItem(DEFAULT_ACTION.toString())))
            .andExpect(jsonPath("$.[*].is_deleted").value(hasItem(DEFAULT_IS_DELETED)));
    }

    @Test
    @Transactional
    public void getProjLog() throws Exception {
        // Initialize the database
        projLogRepository.saveAndFlush(projLog);

        // Get the projLog
        restProjLogMockMvc.perform(get("/api/proj-logs/{id}", projLog.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(projLog.getId().intValue()))
            .andExpect(jsonPath("$.record_id").value(DEFAULT_RECORD_ID))
            .andExpect(jsonPath("$.log_type").value(DEFAULT_LOG_TYPE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.created_by").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.created_date").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.action").value(DEFAULT_ACTION.toString()))
            .andExpect(jsonPath("$.is_deleted").value(DEFAULT_IS_DELETED));
    }

    @Test
    @Transactional
    public void getNonExistingProjLog() throws Exception {
        // Get the projLog
        restProjLogMockMvc.perform(get("/api/proj-logs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProjLog() throws Exception {
        // Initialize the database
        projLogService.save(projLog);

        int databaseSizeBeforeUpdate = projLogRepository.findAll().size();

        // Update the projLog
        ProjLog updatedProjLog = projLogRepository.findOne(projLog.getId());
        updatedProjLog
            .record_id(UPDATED_RECORD_ID)
            .log_type(UPDATED_LOG_TYPE)
            .description(UPDATED_DESCRIPTION)
            .created_by(UPDATED_CREATED_BY)
            .created_date(UPDATED_CREATED_DATE)
            .action(UPDATED_ACTION)
            .is_deleted(UPDATED_IS_DELETED);

        restProjLogMockMvc.perform(put("/api/proj-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedProjLog)))
            .andExpect(status().isOk());

        // Validate the ProjLog in the database
        List<ProjLog> projLogList = projLogRepository.findAll();
        assertThat(projLogList).hasSize(databaseSizeBeforeUpdate);
        ProjLog testProjLog = projLogList.get(projLogList.size() - 1);
        assertThat(testProjLog.getRecord_id()).isEqualTo(UPDATED_RECORD_ID);
        assertThat(testProjLog.getLog_type()).isEqualTo(UPDATED_LOG_TYPE);
        assertThat(testProjLog.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testProjLog.getCreated_by()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testProjLog.getCreated_date()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testProjLog.getAction()).isEqualTo(UPDATED_ACTION);
        assertThat(testProjLog.getIs_deleted()).isEqualTo(UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void updateNonExistingProjLog() throws Exception {
        int databaseSizeBeforeUpdate = projLogRepository.findAll().size();

        // Create the ProjLog

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restProjLogMockMvc.perform(put("/api/proj-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(projLog)))
            .andExpect(status().isCreated());

        // Validate the ProjLog in the database
        List<ProjLog> projLogList = projLogRepository.findAll();
        assertThat(projLogList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteProjLog() throws Exception {
        // Initialize the database
        projLogService.save(projLog);

        int databaseSizeBeforeDelete = projLogRepository.findAll().size();

        // Get the projLog
        restProjLogMockMvc.perform(delete("/api/proj-logs/{id}", projLog.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ProjLog> projLogList = projLogRepository.findAll();
        assertThat(projLogList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProjLog.class);
        ProjLog projLog1 = new ProjLog();
        projLog1.setId(1L);
        ProjLog projLog2 = new ProjLog();
        projLog2.setId(projLog1.getId());
        assertThat(projLog1).isEqualTo(projLog2);
        projLog2.setId(2L);
        assertThat(projLog1).isNotEqualTo(projLog2);
        projLog1.setId(null);
        assertThat(projLog1).isNotEqualTo(projLog2);
    }
}
