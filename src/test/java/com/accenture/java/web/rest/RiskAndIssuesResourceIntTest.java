package com.accenture.java.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.java.QdestroyerApp;
import com.accenture.java.domain.RiskAndIssues;
import com.accenture.java.repository.RiskAndIssuesRepository;
import com.accenture.java.service.ProjLogService;
import com.accenture.java.service.RiskAndIssuesService;
import com.accenture.java.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the RiskAndIssuesResource REST controller.
 *
 * @see RiskAndIssuesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = QdestroyerApp.class)
public class RiskAndIssuesResourceIntTest {

    private static final String DEFAULT_REC_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_REC_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_SOURCE = "AAAAAAAAAA";
    private static final String UPDATED_SOURCE = "BBBBBBBBBB";

    private static final String DEFAULT_PRIORITY = "Low";
    private static final String UPDATED_PRIORITY = "HIgh";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_SEVERITY = "Major";
    private static final String UPDATED_SEVERITY = "Minor";

    private static final String DEFAULT_PROBABILITY_PERCENT = "10%";
    private static final String UPDATED_PROBABILITY_PERCENT = "20%";

    private static final String DEFAULT_MITIGATION_PLAN = "AAAAAAAAAA";
    private static final String UPDATED_MITIGATION_PLAN = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_MITIGATION_DUE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_MITIGATION_DUE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CONTINGENCY_PLAN = "AAAAAAAAAA";
    private static final String UPDATED_CONTINGENCY_PLAN = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CONTINGENCY_DUE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CONTINGENCY_DUE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_MODIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_MODIFIED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_MODIFIED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_IS_DELETED = 1;
    private static final Integer UPDATED_IS_DELETED = 2;

    private static final String DEFAULT_RESP = "AAAAAAAAAA";
    private static final String UPDATED_RESP = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_OCCURED = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_OCCURED = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_IMPACT = "AAAAAAAAAA";
    private static final String UPDATED_IMPACT = "BBBBBBBBBB";

    @Autowired
    private RiskAndIssuesRepository riskAndIssuesRepository;

    @Autowired
    private RiskAndIssuesService riskAndIssuesService;
    
    @Autowired
    private ProjLogService logService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRiskAndIssuesMockMvc;

    private RiskAndIssues riskAndIssues;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        RiskAndIssuesResource riskAndIssuesResource = new RiskAndIssuesResource(riskAndIssuesService, logService);
        this.restRiskAndIssuesMockMvc = MockMvcBuilders.standaloneSetup(riskAndIssuesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RiskAndIssues createEntity(EntityManager em) {
        RiskAndIssues riskAndIssues = new RiskAndIssues()
            .rec_type(DEFAULT_REC_TYPE)
            .description(DEFAULT_DESCRIPTION)
            .source(DEFAULT_SOURCE)
            .priority(DEFAULT_PRIORITY)
            .status(DEFAULT_STATUS)
            .severity(DEFAULT_SEVERITY)
            .probability_percent(DEFAULT_PROBABILITY_PERCENT)
            .mitigation_plan(DEFAULT_MITIGATION_PLAN)
            .mitigation_due_date(DEFAULT_MITIGATION_DUE_DATE)
            .contingency_plan(DEFAULT_CONTINGENCY_PLAN)
            .contingency_due_date(DEFAULT_CONTINGENCY_DUE_DATE)
            .remarks(DEFAULT_REMARKS)
            .created_by(DEFAULT_CREATED_BY)
            .created_date(DEFAULT_CREATED_DATE)
            .modified_by(DEFAULT_MODIFIED_BY)
            .modified_date(DEFAULT_MODIFIED_DATE)
            .is_deleted(DEFAULT_IS_DELETED)
            .resp(DEFAULT_RESP)
            .date_occured(DEFAULT_DATE_OCCURED)
            .impact(DEFAULT_IMPACT);
        return riskAndIssues;
    }

    @Before
    public void initTest() {
        riskAndIssues = createEntity(em);
    }

    @Test
    @Transactional
    public void createRiskAndIssues() throws Exception {
        int databaseSizeBeforeCreate = riskAndIssuesRepository.findAll().size();

        // Create the RiskAndIssues
        restRiskAndIssuesMockMvc.perform(post("/api/risk-and-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(riskAndIssues)))
            .andExpect(status().isCreated());

        // Validate the RiskAndIssues in the database
        List<RiskAndIssues> riskAndIssuesList = riskAndIssuesRepository.findAll();
        assertThat(riskAndIssuesList).hasSize(databaseSizeBeforeCreate + 1);
        RiskAndIssues testRiskAndIssues = riskAndIssuesList.get(riskAndIssuesList.size() - 1);
        assertThat(testRiskAndIssues.getRec_type()).isEqualTo(DEFAULT_REC_TYPE);
        assertThat(testRiskAndIssues.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testRiskAndIssues.getSource()).isEqualTo(DEFAULT_SOURCE);
        assertThat(testRiskAndIssues.getPriority()).isEqualTo(DEFAULT_PRIORITY);
        assertThat(testRiskAndIssues.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testRiskAndIssues.getSeverity()).isEqualTo(DEFAULT_SEVERITY);
        assertThat(testRiskAndIssues.getProbability_percent()).isEqualTo(DEFAULT_PROBABILITY_PERCENT);
        assertThat(testRiskAndIssues.getMitigation_plan()).isEqualTo(DEFAULT_MITIGATION_PLAN);
        assertThat(testRiskAndIssues.getMitigation_due_date()).isEqualTo(DEFAULT_MITIGATION_DUE_DATE);
        assertThat(testRiskAndIssues.getContingency_plan()).isEqualTo(DEFAULT_CONTINGENCY_PLAN);
        assertThat(testRiskAndIssues.getContingency_due_date()).isEqualTo(DEFAULT_CONTINGENCY_DUE_DATE);
        assertThat(testRiskAndIssues.getRemarks()).isEqualTo(DEFAULT_REMARKS);
        assertThat(testRiskAndIssues.getCreated_by()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testRiskAndIssues.getCreated_date()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testRiskAndIssues.getModified_by()).isEqualTo(DEFAULT_MODIFIED_BY);
        assertThat(testRiskAndIssues.getModified_date()).isEqualTo(DEFAULT_MODIFIED_DATE);
        assertThat(testRiskAndIssues.getIs_deleted()).isEqualTo(DEFAULT_IS_DELETED);
        assertThat(testRiskAndIssues.getResp()).isEqualTo(DEFAULT_RESP);
        assertThat(testRiskAndIssues.getDate_occured()).isEqualTo(DEFAULT_DATE_OCCURED);
        assertThat(testRiskAndIssues.getImpact()).isEqualTo(DEFAULT_IMPACT);
    }

    @Test
    @Transactional
    public void createRiskAndIssuesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = riskAndIssuesRepository.findAll().size();

        // Create the RiskAndIssues with an existing ID
        riskAndIssues.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRiskAndIssuesMockMvc.perform(post("/api/risk-and-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(riskAndIssues)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<RiskAndIssues> riskAndIssuesList = riskAndIssuesRepository.findAll();
        assertThat(riskAndIssuesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllRiskAndIssues() throws Exception {
        // Initialize the database
        riskAndIssuesRepository.saveAndFlush(riskAndIssues);

        // Get all the riskAndIssuesList
        restRiskAndIssuesMockMvc.perform(get("/api/risk-and-issues?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(riskAndIssues.getId().intValue())))
            .andExpect(jsonPath("$.[*].rec_type").value(hasItem(DEFAULT_REC_TYPE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].source").value(hasItem(DEFAULT_SOURCE.toString())))
            .andExpect(jsonPath("$.[*].priority").value(hasItem(DEFAULT_PRIORITY)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].severity").value(hasItem(DEFAULT_SEVERITY)))
            .andExpect(jsonPath("$.[*].probability_percent").value(hasItem(DEFAULT_PROBABILITY_PERCENT)))
            .andExpect(jsonPath("$.[*].mitigation_plan").value(hasItem(DEFAULT_MITIGATION_PLAN.toString())))
            .andExpect(jsonPath("$.[*].mitigation_due_date").value(hasItem(DEFAULT_MITIGATION_DUE_DATE.toString())))
            .andExpect(jsonPath("$.[*].contingency_plan").value(hasItem(DEFAULT_CONTINGENCY_PLAN.toString())))
            .andExpect(jsonPath("$.[*].contingency_due_date").value(hasItem(DEFAULT_CONTINGENCY_DUE_DATE.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS.toString())))
            .andExpect(jsonPath("$.[*].created_by").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].created_date").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].modified_by").value(hasItem(DEFAULT_MODIFIED_BY.toString())))
            .andExpect(jsonPath("$.[*].modified_date").value(hasItem(DEFAULT_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].is_deleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].resp").value(hasItem(DEFAULT_RESP.toString())))
            .andExpect(jsonPath("$.[*].date_occured").value(hasItem(DEFAULT_DATE_OCCURED.toString())))
            .andExpect(jsonPath("$.[*].impact").value(hasItem(DEFAULT_IMPACT.toString())));
    }

    @Test
    @Transactional
    public void getRiskAndIssues() throws Exception {
        // Initialize the database
        riskAndIssuesRepository.saveAndFlush(riskAndIssues);

        // Get the riskAndIssues
        restRiskAndIssuesMockMvc.perform(get("/api/risk-and-issues/{id}", riskAndIssues.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(riskAndIssues.getId().intValue()))
            .andExpect(jsonPath("$.rec_type").value(DEFAULT_REC_TYPE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.source").value(DEFAULT_SOURCE.toString()))
            .andExpect(jsonPath("$.priority").value(DEFAULT_PRIORITY))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.severity").value(DEFAULT_SEVERITY))
            .andExpect(jsonPath("$.probability_percent").value(DEFAULT_PROBABILITY_PERCENT))
            .andExpect(jsonPath("$.mitigation_plan").value(DEFAULT_MITIGATION_PLAN.toString()))
            .andExpect(jsonPath("$.mitigation_due_date").value(DEFAULT_MITIGATION_DUE_DATE.toString()))
            .andExpect(jsonPath("$.contingency_plan").value(DEFAULT_CONTINGENCY_PLAN.toString()))
            .andExpect(jsonPath("$.contingency_due_date").value(DEFAULT_CONTINGENCY_DUE_DATE.toString()))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS.toString()))
            .andExpect(jsonPath("$.created_by").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.created_date").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.modified_by").value(DEFAULT_MODIFIED_BY.toString()))
            .andExpect(jsonPath("$.modified_date").value(DEFAULT_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.is_deleted").value(DEFAULT_IS_DELETED))
            .andExpect(jsonPath("$.resp").value(DEFAULT_RESP.toString()))
            .andExpect(jsonPath("$.date_occured").value(DEFAULT_DATE_OCCURED.toString()))
            .andExpect(jsonPath("$.impact").value(DEFAULT_IMPACT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingRiskAndIssues() throws Exception {
        // Get the riskAndIssues
        restRiskAndIssuesMockMvc.perform(get("/api/risk-and-issues/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRiskAndIssues() throws Exception {
        // Initialize the database
        riskAndIssuesService.save(riskAndIssues);

        int databaseSizeBeforeUpdate = riskAndIssuesRepository.findAll().size();

        // Update the riskAndIssues
        RiskAndIssues updatedRiskAndIssues = riskAndIssuesRepository.findOne(riskAndIssues.getId());
        updatedRiskAndIssues
            .rec_type(UPDATED_REC_TYPE)
            .description(UPDATED_DESCRIPTION)
            .source(UPDATED_SOURCE)
            .priority(UPDATED_PRIORITY)
            .status(UPDATED_STATUS)
            .severity(UPDATED_SEVERITY)
            .probability_percent(UPDATED_PROBABILITY_PERCENT)
            .mitigation_plan(UPDATED_MITIGATION_PLAN)
            .mitigation_due_date(UPDATED_MITIGATION_DUE_DATE)
            .contingency_plan(UPDATED_CONTINGENCY_PLAN)
            .contingency_due_date(UPDATED_CONTINGENCY_DUE_DATE)
            .remarks(UPDATED_REMARKS)
            .created_by(UPDATED_CREATED_BY)
            .created_date(UPDATED_CREATED_DATE)
            .modified_by(UPDATED_MODIFIED_BY)
            .modified_date(UPDATED_MODIFIED_DATE)
            .is_deleted(UPDATED_IS_DELETED)
            .resp(UPDATED_RESP)
            .date_occured(UPDATED_DATE_OCCURED)
            .impact(UPDATED_IMPACT);

        restRiskAndIssuesMockMvc.perform(put("/api/risk-and-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRiskAndIssues)))
            .andExpect(status().isOk());

        // Validate the RiskAndIssues in the database
        List<RiskAndIssues> riskAndIssuesList = riskAndIssuesRepository.findAll();
        assertThat(riskAndIssuesList).hasSize(databaseSizeBeforeUpdate);
        RiskAndIssues testRiskAndIssues = riskAndIssuesList.get(riskAndIssuesList.size() - 1);
        assertThat(testRiskAndIssues.getRec_type()).isEqualTo(UPDATED_REC_TYPE);
        assertThat(testRiskAndIssues.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testRiskAndIssues.getSource()).isEqualTo(UPDATED_SOURCE);
        assertThat(testRiskAndIssues.getPriority()).isEqualTo(UPDATED_PRIORITY);
        assertThat(testRiskAndIssues.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testRiskAndIssues.getSeverity()).isEqualTo(UPDATED_SEVERITY);
        assertThat(testRiskAndIssues.getProbability_percent()).isEqualTo(UPDATED_PROBABILITY_PERCENT);
        assertThat(testRiskAndIssues.getMitigation_plan()).isEqualTo(UPDATED_MITIGATION_PLAN);
        assertThat(testRiskAndIssues.getMitigation_due_date()).isEqualTo(UPDATED_MITIGATION_DUE_DATE);
        assertThat(testRiskAndIssues.getContingency_plan()).isEqualTo(UPDATED_CONTINGENCY_PLAN);
        assertThat(testRiskAndIssues.getContingency_due_date()).isEqualTo(UPDATED_CONTINGENCY_DUE_DATE);
        assertThat(testRiskAndIssues.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testRiskAndIssues.getCreated_by()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testRiskAndIssues.getCreated_date()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testRiskAndIssues.getModified_by()).isEqualTo(UPDATED_MODIFIED_BY);
        assertThat(testRiskAndIssues.getModified_date()).isEqualTo(UPDATED_MODIFIED_DATE);
        assertThat(testRiskAndIssues.getIs_deleted()).isEqualTo(UPDATED_IS_DELETED);
        assertThat(testRiskAndIssues.getResp()).isEqualTo(UPDATED_RESP);
        assertThat(testRiskAndIssues.getDate_occured()).isEqualTo(UPDATED_DATE_OCCURED);
        assertThat(testRiskAndIssues.getImpact()).isEqualTo(UPDATED_IMPACT);
    }

    @Test
    @Transactional
    public void updateNonExistingRiskAndIssues() throws Exception {
        int databaseSizeBeforeUpdate = riskAndIssuesRepository.findAll().size();

        // Create the RiskAndIssues

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restRiskAndIssuesMockMvc.perform(put("/api/risk-and-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(riskAndIssues)))
            .andExpect(status().isCreated());

        // Validate the RiskAndIssues in the database
        List<RiskAndIssues> riskAndIssuesList = riskAndIssuesRepository.findAll();
        assertThat(riskAndIssuesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteRiskAndIssues() throws Exception {
        // Initialize the database
        riskAndIssuesService.save(riskAndIssues);

        int databaseSizeBeforeDelete = riskAndIssuesRepository.findAll().size();

        // Get the riskAndIssues
        restRiskAndIssuesMockMvc.perform(delete("/api/risk-and-issues/{id}", riskAndIssues.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<RiskAndIssues> riskAndIssuesList = riskAndIssuesRepository.findAll();
        assertThat(riskAndIssuesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RiskAndIssues.class);
        RiskAndIssues riskAndIssues1 = new RiskAndIssues();
        riskAndIssues1.setId(1L);
        RiskAndIssues riskAndIssues2 = new RiskAndIssues();
        riskAndIssues2.setId(riskAndIssues1.getId());
        assertThat(riskAndIssues1).isEqualTo(riskAndIssues2);
        riskAndIssues2.setId(2L);
        assertThat(riskAndIssues1).isNotEqualTo(riskAndIssues2);
        riskAndIssues1.setId(null);
        assertThat(riskAndIssues1).isNotEqualTo(riskAndIssues2);
    }
}
