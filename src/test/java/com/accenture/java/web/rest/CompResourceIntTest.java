package com.accenture.java.web.rest;

import com.accenture.java.QdestroyerApp;

import com.accenture.java.domain.Comp;
import com.accenture.java.repository.CompRepository;
import com.accenture.java.service.CompService;
import com.accenture.java.service.ProjLogService;
import com.accenture.java.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CompResource REST controller.
 *
 * @see CompResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = QdestroyerApp.class)
public class CompResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_BUDGET_AT_COMPLETION = 1;
    private static final Integer UPDATED_BUDGET_AT_COMPLETION = 2;

    private static final LocalDate DEFAULT_BASELINE_START = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_BASELINE_START = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_BASELINE_FINISH = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_BASELINE_FINISH = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_ACTUAL_COST = 1;
    private static final Integer UPDATED_ACTUAL_COST = 2;

    private static final LocalDate DEFAULT_ACTUAL_START = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ACTUAL_START = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_ACTUAL_FINISH = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ACTUAL_FINISH = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_DEVELOPER = "AAAAAAAAAA";
    private static final String UPDATED_DEVELOPER = "BBBBBBBBBB";

    private static final String DEFAULT_REVIEWER = "AAAAAAAAAA";
    private static final String UPDATED_REVIEWER = "BBBBBBBBBB";

    private static final String DEFAULT_COMPONENT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_COMPONENT_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_MODIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_MODIFIED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_MODIFIED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_IS_DELETED = 1;
    private static final Integer UPDATED_IS_DELETED = 2;

    @Autowired
    private CompRepository compRepository;

    @Autowired
    private CompService compService;
    
    @Autowired
    private ProjLogService logService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCompMockMvc;

    private Comp comp;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CompResource compResource = new CompResource(compService, logService);
        this.restCompMockMvc = MockMvcBuilders.standaloneSetup(compResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Comp createEntity(EntityManager em) {
        Comp comp = new Comp()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .budget_at_completion(DEFAULT_BUDGET_AT_COMPLETION)
            .baseline_start(DEFAULT_BASELINE_START)
            .baseline_finish(DEFAULT_BASELINE_FINISH)
            .actual_cost(DEFAULT_ACTUAL_COST)
            .actual_start(DEFAULT_ACTUAL_START)
            .actual_finish(DEFAULT_ACTUAL_FINISH)
            .developer(DEFAULT_DEVELOPER)
            .reviewer(DEFAULT_REVIEWER)
            .component_type(DEFAULT_COMPONENT_TYPE)
            .status(DEFAULT_STATUS)
            .created_by(DEFAULT_CREATED_BY)
            .created_date(DEFAULT_CREATED_DATE)
            .modified_by(DEFAULT_MODIFIED_BY)
            .modified_date(DEFAULT_MODIFIED_DATE)
            .is_deleted(DEFAULT_IS_DELETED);
        return comp;
    }

    @Before
    public void initTest() {
        comp = createEntity(em);
    }

    @Test
    @Transactional
    public void createComp() throws Exception {
        int databaseSizeBeforeCreate = compRepository.findAll().size();

        // Create the Comp
        restCompMockMvc.perform(post("/api/comps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(comp)))
            .andExpect(status().isCreated());

        // Validate the Comp in the database
        List<Comp> compList = compRepository.findAll();
        assertThat(compList).hasSize(databaseSizeBeforeCreate + 1);
        Comp testComp = compList.get(compList.size() - 1);
        assertThat(testComp.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testComp.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testComp.getBudget_at_completion()).isEqualTo(DEFAULT_BUDGET_AT_COMPLETION);
        assertThat(testComp.getBaseline_start()).isEqualTo(DEFAULT_BASELINE_START);
        assertThat(testComp.getBaseline_finish()).isEqualTo(DEFAULT_BASELINE_FINISH);
        assertThat(testComp.getActual_cost()).isEqualTo(DEFAULT_ACTUAL_COST);
        assertThat(testComp.getActual_start()).isEqualTo(DEFAULT_ACTUAL_START);
        assertThat(testComp.getActual_finish()).isEqualTo(DEFAULT_ACTUAL_FINISH);
        assertThat(testComp.getDeveloper()).isEqualTo(DEFAULT_DEVELOPER);
        assertThat(testComp.getReviewer()).isEqualTo(DEFAULT_REVIEWER);
        assertThat(testComp.getComponent_type()).isEqualTo(DEFAULT_COMPONENT_TYPE);
        assertThat(testComp.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testComp.getCreated_by()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testComp.getCreated_date()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testComp.getModified_by()).isEqualTo(DEFAULT_MODIFIED_BY);
        assertThat(testComp.getModified_date()).isEqualTo(DEFAULT_MODIFIED_DATE);
        assertThat(testComp.getIs_deleted()).isEqualTo(DEFAULT_IS_DELETED);
    }

    @Test
    @Transactional
    public void createCompWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = compRepository.findAll().size();

        // Create the Comp with an existing ID
        comp.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCompMockMvc.perform(post("/api/comps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(comp)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Comp> compList = compRepository.findAll();
        assertThat(compList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllComps() throws Exception {
        // Initialize the database
        compRepository.saveAndFlush(comp);

        // Get all the compList
        restCompMockMvc.perform(get("/api/comps?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(comp.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].budget_at_completion").value(hasItem(DEFAULT_BUDGET_AT_COMPLETION)))
            .andExpect(jsonPath("$.[*].baseline_start").value(hasItem(DEFAULT_BASELINE_START.toString())))
            .andExpect(jsonPath("$.[*].baseline_finish").value(hasItem(DEFAULT_BASELINE_FINISH.toString())))
            .andExpect(jsonPath("$.[*].actual_cost").value(hasItem(DEFAULT_ACTUAL_COST)))
            .andExpect(jsonPath("$.[*].actual_start").value(hasItem(DEFAULT_ACTUAL_START.toString())))
            .andExpect(jsonPath("$.[*].actual_finish").value(hasItem(DEFAULT_ACTUAL_FINISH.toString())))
            .andExpect(jsonPath("$.[*].developer").value(hasItem(DEFAULT_DEVELOPER.toString())))
            .andExpect(jsonPath("$.[*].reviewer").value(hasItem(DEFAULT_REVIEWER.toString())))
            .andExpect(jsonPath("$.[*].component_type").value(hasItem(DEFAULT_COMPONENT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].created_by").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].created_date").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].modified_by").value(hasItem(DEFAULT_MODIFIED_BY.toString())))
            .andExpect(jsonPath("$.[*].modified_date").value(hasItem(DEFAULT_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].is_deleted").value(hasItem(DEFAULT_IS_DELETED)));
    }

    @Test
    @Transactional
    public void getComp() throws Exception {
        // Initialize the database
        compRepository.saveAndFlush(comp);

        // Get the comp
        restCompMockMvc.perform(get("/api/comps/{id}", comp.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(comp.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.budget_at_completion").value(DEFAULT_BUDGET_AT_COMPLETION))
            .andExpect(jsonPath("$.baseline_start").value(DEFAULT_BASELINE_START.toString()))
            .andExpect(jsonPath("$.baseline_finish").value(DEFAULT_BASELINE_FINISH.toString()))
            .andExpect(jsonPath("$.actual_cost").value(DEFAULT_ACTUAL_COST))
            .andExpect(jsonPath("$.actual_start").value(DEFAULT_ACTUAL_START.toString()))
            .andExpect(jsonPath("$.actual_finish").value(DEFAULT_ACTUAL_FINISH.toString()))
            .andExpect(jsonPath("$.developer").value(DEFAULT_DEVELOPER.toString()))
            .andExpect(jsonPath("$.reviewer").value(DEFAULT_REVIEWER.toString()))
            .andExpect(jsonPath("$.component_type").value(DEFAULT_COMPONENT_TYPE.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.created_by").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.created_date").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.modified_by").value(DEFAULT_MODIFIED_BY.toString()))
            .andExpect(jsonPath("$.modified_date").value(DEFAULT_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.is_deleted").value(DEFAULT_IS_DELETED));
    }

    @Test
    @Transactional
    public void getNonExistingComp() throws Exception {
        // Get the comp
        restCompMockMvc.perform(get("/api/comps/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateComp() throws Exception {
        // Initialize the database
        compService.save(comp);

        int databaseSizeBeforeUpdate = compRepository.findAll().size();

        // Update the comp
        Comp updatedComp = compRepository.findOne(comp.getId());
        updatedComp
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .budget_at_completion(UPDATED_BUDGET_AT_COMPLETION)
            .baseline_start(UPDATED_BASELINE_START)
            .baseline_finish(UPDATED_BASELINE_FINISH)
            .actual_cost(UPDATED_ACTUAL_COST)
            .actual_start(UPDATED_ACTUAL_START)
            .actual_finish(UPDATED_ACTUAL_FINISH)
            .developer(UPDATED_DEVELOPER)
            .reviewer(UPDATED_REVIEWER)
            .component_type(UPDATED_COMPONENT_TYPE)
            .status(UPDATED_STATUS)
            .created_by(UPDATED_CREATED_BY)
            .created_date(UPDATED_CREATED_DATE)
            .modified_by(UPDATED_MODIFIED_BY)
            .modified_date(UPDATED_MODIFIED_DATE)
            .is_deleted(UPDATED_IS_DELETED);

        restCompMockMvc.perform(put("/api/comps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedComp)))
            .andExpect(status().isOk());

        // Validate the Comp in the database
        List<Comp> compList = compRepository.findAll();
        assertThat(compList).hasSize(databaseSizeBeforeUpdate);
        Comp testComp = compList.get(compList.size() - 1);
        assertThat(testComp.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testComp.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testComp.getBudget_at_completion()).isEqualTo(UPDATED_BUDGET_AT_COMPLETION);
        assertThat(testComp.getBaseline_start()).isEqualTo(UPDATED_BASELINE_START);
        assertThat(testComp.getBaseline_finish()).isEqualTo(UPDATED_BASELINE_FINISH);
        assertThat(testComp.getActual_cost()).isEqualTo(UPDATED_ACTUAL_COST);
        assertThat(testComp.getActual_start()).isEqualTo(UPDATED_ACTUAL_START);
        assertThat(testComp.getActual_finish()).isEqualTo(UPDATED_ACTUAL_FINISH);
        assertThat(testComp.getDeveloper()).isEqualTo(UPDATED_DEVELOPER);
        assertThat(testComp.getReviewer()).isEqualTo(UPDATED_REVIEWER);
        assertThat(testComp.getComponent_type()).isEqualTo(UPDATED_COMPONENT_TYPE);
        assertThat(testComp.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testComp.getCreated_by()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testComp.getCreated_date()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testComp.getModified_by()).isEqualTo(UPDATED_MODIFIED_BY);
        assertThat(testComp.getModified_date()).isEqualTo(UPDATED_MODIFIED_DATE);
        assertThat(testComp.getIs_deleted()).isEqualTo(UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void updateNonExistingComp() throws Exception {
        int databaseSizeBeforeUpdate = compRepository.findAll().size();

        // Create the Comp

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCompMockMvc.perform(put("/api/comps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(comp)))
            .andExpect(status().isCreated());

        // Validate the Comp in the database
        List<Comp> compList = compRepository.findAll();
        assertThat(compList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteComp() throws Exception {
        // Initialize the database
        compService.save(comp);

        int databaseSizeBeforeDelete = compRepository.findAll().size();

        // Get the comp
        restCompMockMvc.perform(delete("/api/comps/{id}", comp.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Comp> compList = compRepository.findAll();
        assertThat(compList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Comp.class);
        Comp comp1 = new Comp();
        comp1.setId(1L);
        Comp comp2 = new Comp();
        comp2.setId(comp1.getId());
        assertThat(comp1).isEqualTo(comp2);
        comp2.setId(2L);
        assertThat(comp1).isNotEqualTo(comp2);
        comp1.setId(null);
        assertThat(comp1).isNotEqualTo(comp2);
    }
}
