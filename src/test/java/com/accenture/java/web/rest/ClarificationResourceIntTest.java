package com.accenture.java.web.rest;

import com.accenture.java.QdestroyerApp;

import com.accenture.java.domain.Clarification;
import com.accenture.java.repository.ClarificationRepository;
import com.accenture.java.service.ClarificationService;
import com.accenture.java.service.ProjLogService;
import com.accenture.java.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ClarificationResource REST controller.
 *
 * @see ClarificationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = QdestroyerApp.class)
public class ClarificationResourceIntTest {

    private static final LocalDate DEFAULT_CL_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CL_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_PRIORITY = "Minor";
    private static final String UPDATED_PRIORITY = "Major";

    private static final String DEFAULT_CLASSIFICATION = "AAAAAAAAAA";
    private static final String UPDATED_CLASSIFICATION = "BBBBBBBBBB";

    private static final String DEFAULT_ASSIGNED_TO = "AAAAAAAAAA";
    private static final String UPDATED_ASSIGNED_TO = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_RESOLUTION = "AAAAAAAAAA";
    private static final String UPDATED_RESOLUTION = "BBBBBBBBBB";

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_CLOSED = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_CLOSED = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_MODIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_MODIFIED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_MODIFIED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_IS_DELETED = 1;
    private static final Integer UPDATED_IS_DELETED = 2;

    @Autowired
    private ClarificationRepository clarificationRepository;

    @Autowired
    private ClarificationService clarificationService;
    
    @Autowired
    private ProjLogService logService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restClarificationMockMvc;

    private Clarification clarification;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ClarificationResource clarificationResource = new ClarificationResource(clarificationService, logService);
        this.restClarificationMockMvc = MockMvcBuilders.standaloneSetup(clarificationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Clarification createEntity(EntityManager em) {
        Clarification clarification = new Clarification()
            .cl_date(DEFAULT_CL_DATE)
            .status(DEFAULT_STATUS)
            .priority(DEFAULT_PRIORITY)
            .classification(DEFAULT_CLASSIFICATION)
            .assigned_to(DEFAULT_ASSIGNED_TO)
            .description(DEFAULT_DESCRIPTION)
            .resolution(DEFAULT_RESOLUTION)
            .remarks(DEFAULT_REMARKS)
            .date_closed(DEFAULT_DATE_CLOSED)
            .created_by(DEFAULT_CREATED_BY)
            .created_date(DEFAULT_CREATED_DATE)
            .modified_by(DEFAULT_MODIFIED_BY)
            .modified_date(DEFAULT_MODIFIED_DATE)
            .is_deleted(DEFAULT_IS_DELETED);
        return clarification;
    }

    @Before
    public void initTest() {
        clarification = createEntity(em);
    }

    @Test
    @Transactional
    public void createClarification() throws Exception {
        int databaseSizeBeforeCreate = clarificationRepository.findAll().size();

        // Create the Clarification
        restClarificationMockMvc.perform(post("/api/clarifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clarification)))
            .andExpect(status().isCreated());

        // Validate the Clarification in the database
        List<Clarification> clarificationList = clarificationRepository.findAll();
        assertThat(clarificationList).hasSize(databaseSizeBeforeCreate + 1);
        Clarification testClarification = clarificationList.get(clarificationList.size() - 1);
        assertThat(testClarification.getCl_date()).isEqualTo(DEFAULT_CL_DATE);
        assertThat(testClarification.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testClarification.getPriority()).isEqualTo(DEFAULT_PRIORITY);
        assertThat(testClarification.getClassification()).isEqualTo(DEFAULT_CLASSIFICATION);
        assertThat(testClarification.getAssigned_to()).isEqualTo(DEFAULT_ASSIGNED_TO);
        assertThat(testClarification.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testClarification.getResolution()).isEqualTo(DEFAULT_RESOLUTION);
        assertThat(testClarification.getRemarks()).isEqualTo(DEFAULT_REMARKS);
        assertThat(testClarification.getDate_closed()).isEqualTo(DEFAULT_DATE_CLOSED);
        assertThat(testClarification.getCreated_by()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testClarification.getCreated_date()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testClarification.getModified_by()).isEqualTo(DEFAULT_MODIFIED_BY);
        assertThat(testClarification.getModified_date()).isEqualTo(DEFAULT_MODIFIED_DATE);
        assertThat(testClarification.getIs_deleted()).isEqualTo(DEFAULT_IS_DELETED);
    }

    @Test
    @Transactional
    public void createClarificationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clarificationRepository.findAll().size();

        // Create the Clarification with an existing ID
        clarification.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClarificationMockMvc.perform(post("/api/clarifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clarification)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Clarification> clarificationList = clarificationRepository.findAll();
        assertThat(clarificationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllClarifications() throws Exception {
        // Initialize the database
        clarificationRepository.saveAndFlush(clarification);

        // Get all the clarificationList
        restClarificationMockMvc.perform(get("/api/clarifications?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clarification.getId().intValue())))
            .andExpect(jsonPath("$.[*].cl_date").value(hasItem(DEFAULT_CL_DATE.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].priority").value(hasItem(DEFAULT_PRIORITY)))
            .andExpect(jsonPath("$.[*].classification").value(hasItem(DEFAULT_CLASSIFICATION.toString())))
            .andExpect(jsonPath("$.[*].assigned_to").value(hasItem(DEFAULT_ASSIGNED_TO.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].resolution").value(hasItem(DEFAULT_RESOLUTION.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS.toString())))
            .andExpect(jsonPath("$.[*].date_closed").value(hasItem(DEFAULT_DATE_CLOSED.toString())))
            .andExpect(jsonPath("$.[*].created_by").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].created_date").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].modified_by").value(hasItem(DEFAULT_MODIFIED_BY.toString())))
            .andExpect(jsonPath("$.[*].modified_date").value(hasItem(DEFAULT_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].is_deleted").value(hasItem(DEFAULT_IS_DELETED)));
    }

    @Test
    @Transactional
    public void getClarification() throws Exception {
        // Initialize the database
        clarificationRepository.saveAndFlush(clarification);

        // Get the clarification
        restClarificationMockMvc.perform(get("/api/clarifications/{id}", clarification.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(clarification.getId().intValue()))
            .andExpect(jsonPath("$.cl_date").value(DEFAULT_CL_DATE.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.priority").value(DEFAULT_PRIORITY))
            .andExpect(jsonPath("$.classification").value(DEFAULT_CLASSIFICATION.toString()))
            .andExpect(jsonPath("$.assigned_to").value(DEFAULT_ASSIGNED_TO.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.resolution").value(DEFAULT_RESOLUTION.toString()))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS.toString()))
            .andExpect(jsonPath("$.date_closed").value(DEFAULT_DATE_CLOSED.toString()))
            .andExpect(jsonPath("$.created_by").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.created_date").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.modified_by").value(DEFAULT_MODIFIED_BY.toString()))
            .andExpect(jsonPath("$.modified_date").value(DEFAULT_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.is_deleted").value(DEFAULT_IS_DELETED));
    }

    @Test
    @Transactional
    public void getNonExistingClarification() throws Exception {
        // Get the clarification
        restClarificationMockMvc.perform(get("/api/clarifications/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClarification() throws Exception {
        // Initialize the database
        clarificationService.save(clarification);

        int databaseSizeBeforeUpdate = clarificationRepository.findAll().size();

        // Update the clarification
        Clarification updatedClarification = clarificationRepository.findOne(clarification.getId());
        updatedClarification
            .cl_date(UPDATED_CL_DATE)
            .status(UPDATED_STATUS)
            .priority(UPDATED_PRIORITY)
            .classification(UPDATED_CLASSIFICATION)
            .assigned_to(UPDATED_ASSIGNED_TO)
            .description(UPDATED_DESCRIPTION)
            .resolution(UPDATED_RESOLUTION)
            .remarks(UPDATED_REMARKS)
            .date_closed(UPDATED_DATE_CLOSED)
            .created_by(UPDATED_CREATED_BY)
            .created_date(UPDATED_CREATED_DATE)
            .modified_by(UPDATED_MODIFIED_BY)
            .modified_date(UPDATED_MODIFIED_DATE)
            .is_deleted(UPDATED_IS_DELETED);

        restClarificationMockMvc.perform(put("/api/clarifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedClarification)))
            .andExpect(status().isOk());

        // Validate the Clarification in the database
        List<Clarification> clarificationList = clarificationRepository.findAll();
        assertThat(clarificationList).hasSize(databaseSizeBeforeUpdate);
        Clarification testClarification = clarificationList.get(clarificationList.size() - 1);
        assertThat(testClarification.getCl_date()).isEqualTo(UPDATED_CL_DATE);
        assertThat(testClarification.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testClarification.getPriority()).isEqualTo(UPDATED_PRIORITY);
        assertThat(testClarification.getClassification()).isEqualTo(UPDATED_CLASSIFICATION);
        assertThat(testClarification.getAssigned_to()).isEqualTo(UPDATED_ASSIGNED_TO);
        assertThat(testClarification.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testClarification.getResolution()).isEqualTo(UPDATED_RESOLUTION);
        assertThat(testClarification.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testClarification.getDate_closed()).isEqualTo(UPDATED_DATE_CLOSED);
        assertThat(testClarification.getCreated_by()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testClarification.getCreated_date()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testClarification.getModified_by()).isEqualTo(UPDATED_MODIFIED_BY);
        assertThat(testClarification.getModified_date()).isEqualTo(UPDATED_MODIFIED_DATE);
        assertThat(testClarification.getIs_deleted()).isEqualTo(UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void updateNonExistingClarification() throws Exception {
        int databaseSizeBeforeUpdate = clarificationRepository.findAll().size();

        // Create the Clarification

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restClarificationMockMvc.perform(put("/api/clarifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clarification)))
            .andExpect(status().isCreated());

        // Validate the Clarification in the database
        List<Clarification> clarificationList = clarificationRepository.findAll();
        assertThat(clarificationList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteClarification() throws Exception {
        // Initialize the database
        clarificationService.save(clarification);

        int databaseSizeBeforeDelete = clarificationRepository.findAll().size();

        // Get the clarification
        restClarificationMockMvc.perform(delete("/api/clarifications/{id}", clarification.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Clarification> clarificationList = clarificationRepository.findAll();
        assertThat(clarificationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Clarification.class);
        Clarification clarification1 = new Clarification();
        clarification1.setId(1L);
        Clarification clarification2 = new Clarification();
        clarification2.setId(clarification1.getId());
        assertThat(clarification1).isEqualTo(clarification2);
        clarification2.setId(2L);
        assertThat(clarification1).isNotEqualTo(clarification2);
        clarification1.setId(null);
        assertThat(clarification1).isNotEqualTo(clarification2);
    }
}
