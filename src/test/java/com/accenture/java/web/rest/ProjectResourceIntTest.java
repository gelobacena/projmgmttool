package com.accenture.java.web.rest;

import com.accenture.java.QdestroyerApp;

import com.accenture.java.domain.Project;
import com.accenture.java.repository.ProjectRepository;
import com.accenture.java.service.ProjLogService;
import com.accenture.java.service.ProjectService;
import com.accenture.java.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ProjectResource REST controller.
 *
 * @see ProjectResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = QdestroyerApp.class)
public class ProjectResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_TEAM = "AAAAAAAAAA";
    private static final String UPDATED_TEAM = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_PLANNED_START = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_PLANNED_START = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_PLANNED_FINISH = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_PLANNED_FINISH = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_ACTUAL_START = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ACTUAL_START = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_ACTUAL_FINISH = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ACTUAL_FINISH = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_MODIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_MODIFIED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_MODIFIED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final Integer DEFAULT_IS_DELETED = 1;
    private static final Integer UPDATED_IS_DELETED = 2;

    private static final LocalDate DEFAULT_TD_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TD_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_BAC_TD_WALKTHROUGH = 1;
    private static final Integer UPDATED_BAC_TD_WALKTHROUGH = 2;

    private static final Integer DEFAULT_BAC_TD_ANALYSIS = 1;
    private static final Integer UPDATED_BAC_TD_ANALYSIS = 2;

    private static final Integer DEFAULT_BAC_UT_EXECUTION = 1;
    private static final Integer UPDATED_BAC_UT_EXECUTION = 2;

    private static final Integer DEFAULT_BAC_UTPLAN_REVIEW = 1;
    private static final Integer UPDATED_BAC_UTPLAN_REVIEW = 2;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private ProjectService projectService;
    
    @Autowired
    private ProjLogService logService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restProjectMockMvc;

    private Project project;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ProjectResource projectResource = new ProjectResource(projectService, logService);
        this.restProjectMockMvc = MockMvcBuilders.standaloneSetup(projectResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Project createEntity(EntityManager em) {
        Project project = new Project()
            .name(DEFAULT_NAME)
            .team(DEFAULT_TEAM)
            .planned_start(DEFAULT_PLANNED_START)
            .planned_finish(DEFAULT_PLANNED_FINISH)
            .actual_start(DEFAULT_ACTUAL_START)
            .actual_finish(DEFAULT_ACTUAL_FINISH)
            .created_by(DEFAULT_CREATED_BY)
            .created_date(DEFAULT_CREATED_DATE)
            .modified_by(DEFAULT_MODIFIED_BY)
            .modified_date(DEFAULT_MODIFIED_DATE)
            .status(DEFAULT_STATUS)
            .is_deleted(DEFAULT_IS_DELETED)
            .td_date(DEFAULT_TD_DATE)
            .bac_td_walkthrough(DEFAULT_BAC_TD_WALKTHROUGH)
            .bac_td_analysis(DEFAULT_BAC_TD_ANALYSIS)
            .bac_ut_execution(DEFAULT_BAC_UT_EXECUTION)
            .bac_utplan_review(DEFAULT_BAC_UTPLAN_REVIEW);
        return project;
    }

    @Before
    public void initTest() {
        project = createEntity(em);
    }

    @Test
    @Transactional
    public void createProject() throws Exception {
        int databaseSizeBeforeCreate = projectRepository.findAll().size();

        // Create the Project
        restProjectMockMvc.perform(post("/api/projects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(project)))
            .andExpect(status().isCreated());

        // Validate the Project in the database
        List<Project> projectList = projectRepository.findAll();
        assertThat(projectList).hasSize(databaseSizeBeforeCreate + 1);
        Project testProject = projectList.get(projectList.size() - 1);
        assertThat(testProject.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testProject.getTeam()).isEqualTo(DEFAULT_TEAM);
        assertThat(testProject.getPlanned_start()).isEqualTo(DEFAULT_PLANNED_START);
        assertThat(testProject.getPlanned_finish()).isEqualTo(DEFAULT_PLANNED_FINISH);
        assertThat(testProject.getActual_start()).isEqualTo(DEFAULT_ACTUAL_START);
        assertThat(testProject.getActual_finish()).isEqualTo(DEFAULT_ACTUAL_FINISH);
        assertThat(testProject.getCreated_by()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testProject.getCreated_date()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testProject.getModified_by()).isEqualTo(DEFAULT_MODIFIED_BY);
        assertThat(testProject.getModified_date()).isEqualTo(DEFAULT_MODIFIED_DATE);
        assertThat(testProject.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testProject.getIs_deleted()).isEqualTo(DEFAULT_IS_DELETED);
        assertThat(testProject.getTd_date()).isEqualTo(DEFAULT_TD_DATE);
        assertThat(testProject.getBac_td_walkthrough()).isEqualTo(DEFAULT_BAC_TD_WALKTHROUGH);
        assertThat(testProject.getBac_td_analysis()).isEqualTo(DEFAULT_BAC_TD_ANALYSIS);
        assertThat(testProject.getBac_ut_execution()).isEqualTo(DEFAULT_BAC_UT_EXECUTION);
        assertThat(testProject.getBac_utplan_review()).isEqualTo(DEFAULT_BAC_UTPLAN_REVIEW);
    }

    @Test
    @Transactional
    public void createProjectWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = projectRepository.findAll().size();

        // Create the Project with an existing ID
        project.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProjectMockMvc.perform(post("/api/projects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(project)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Project> projectList = projectRepository.findAll();
        assertThat(projectList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllProjects() throws Exception {
        // Initialize the database
        projectRepository.saveAndFlush(project);

        // Get all the projectList
        restProjectMockMvc.perform(get("/api/projects?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(project.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].team").value(hasItem(DEFAULT_TEAM.toString())))
            .andExpect(jsonPath("$.[*].planned_start").value(hasItem(DEFAULT_PLANNED_START.toString())))
            .andExpect(jsonPath("$.[*].planned_finish").value(hasItem(DEFAULT_PLANNED_FINISH.toString())))
            .andExpect(jsonPath("$.[*].actual_start").value(hasItem(DEFAULT_ACTUAL_START.toString())))
            .andExpect(jsonPath("$.[*].actual_finish").value(hasItem(DEFAULT_ACTUAL_FINISH.toString())))
            .andExpect(jsonPath("$.[*].created_by").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].created_date").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].modified_by").value(hasItem(DEFAULT_MODIFIED_BY.toString())))
            .andExpect(jsonPath("$.[*].modified_date").value(hasItem(DEFAULT_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].is_deleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].td_date").value(hasItem(DEFAULT_TD_DATE.toString())))
            .andExpect(jsonPath("$.[*].bac_td_walkthrough").value(hasItem(DEFAULT_BAC_TD_WALKTHROUGH)))
            .andExpect(jsonPath("$.[*].bac_td_analysis").value(hasItem(DEFAULT_BAC_TD_ANALYSIS)))
            .andExpect(jsonPath("$.[*].bac_ut_execution").value(hasItem(DEFAULT_BAC_UT_EXECUTION)))
            .andExpect(jsonPath("$.[*].bac_utplan_review").value(hasItem(DEFAULT_BAC_UTPLAN_REVIEW)));
    }

    @Test
    @Transactional
    public void getProject() throws Exception {
        // Initialize the database
        projectRepository.saveAndFlush(project);

        // Get the project
        restProjectMockMvc.perform(get("/api/projects/{id}", project.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(project.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.team").value(DEFAULT_TEAM.toString()))
            .andExpect(jsonPath("$.planned_start").value(DEFAULT_PLANNED_START.toString()))
            .andExpect(jsonPath("$.planned_finish").value(DEFAULT_PLANNED_FINISH.toString()))
            .andExpect(jsonPath("$.actual_start").value(DEFAULT_ACTUAL_START.toString()))
            .andExpect(jsonPath("$.actual_finish").value(DEFAULT_ACTUAL_FINISH.toString()))
            .andExpect(jsonPath("$.created_by").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.created_date").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.modified_by").value(DEFAULT_MODIFIED_BY.toString()))
            .andExpect(jsonPath("$.modified_date").value(DEFAULT_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.is_deleted").value(DEFAULT_IS_DELETED))
            .andExpect(jsonPath("$.td_date").value(DEFAULT_TD_DATE.toString()))
            .andExpect(jsonPath("$.bac_td_walkthrough").value(DEFAULT_BAC_TD_WALKTHROUGH))
            .andExpect(jsonPath("$.bac_td_analysis").value(DEFAULT_BAC_TD_ANALYSIS))
            .andExpect(jsonPath("$.bac_ut_execution").value(DEFAULT_BAC_UT_EXECUTION))
            .andExpect(jsonPath("$.bac_utplan_review").value(DEFAULT_BAC_UTPLAN_REVIEW));
    }

    @Test
    @Transactional
    public void getNonExistingProject() throws Exception {
        // Get the project
        restProjectMockMvc.perform(get("/api/projects/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProject() throws Exception {
        // Initialize the database
        projectService.save(project);

        int databaseSizeBeforeUpdate = projectRepository.findAll().size();

        // Update the project
        Project updatedProject = projectRepository.findOne(project.getId());
        updatedProject
            .name(UPDATED_NAME)
            .team(UPDATED_TEAM)
            .planned_start(UPDATED_PLANNED_START)
            .planned_finish(UPDATED_PLANNED_FINISH)
            .actual_start(UPDATED_ACTUAL_START)
            .actual_finish(UPDATED_ACTUAL_FINISH)
            .created_by(UPDATED_CREATED_BY)
            .created_date(UPDATED_CREATED_DATE)
            .modified_by(UPDATED_MODIFIED_BY)
            .modified_date(UPDATED_MODIFIED_DATE)
            .status(UPDATED_STATUS)
            .is_deleted(UPDATED_IS_DELETED)
            .td_date(UPDATED_TD_DATE)
            .bac_td_walkthrough(UPDATED_BAC_TD_WALKTHROUGH)
            .bac_td_analysis(UPDATED_BAC_TD_ANALYSIS)
            .bac_ut_execution(UPDATED_BAC_UT_EXECUTION)
            .bac_utplan_review(UPDATED_BAC_UTPLAN_REVIEW);

        restProjectMockMvc.perform(put("/api/projects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedProject)))
            .andExpect(status().isOk());

        // Validate the Project in the database
        List<Project> projectList = projectRepository.findAll();
        assertThat(projectList).hasSize(databaseSizeBeforeUpdate);
        Project testProject = projectList.get(projectList.size() - 1);
        assertThat(testProject.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testProject.getTeam()).isEqualTo(UPDATED_TEAM);
        assertThat(testProject.getPlanned_start()).isEqualTo(UPDATED_PLANNED_START);
        assertThat(testProject.getPlanned_finish()).isEqualTo(UPDATED_PLANNED_FINISH);
        assertThat(testProject.getActual_start()).isEqualTo(UPDATED_ACTUAL_START);
        assertThat(testProject.getActual_finish()).isEqualTo(UPDATED_ACTUAL_FINISH);
        assertThat(testProject.getCreated_by()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testProject.getCreated_date()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testProject.getModified_by()).isEqualTo(UPDATED_MODIFIED_BY);
        assertThat(testProject.getModified_date()).isEqualTo(UPDATED_MODIFIED_DATE);
        assertThat(testProject.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testProject.getIs_deleted()).isEqualTo(UPDATED_IS_DELETED);
        assertThat(testProject.getTd_date()).isEqualTo(UPDATED_TD_DATE);
        assertThat(testProject.getBac_td_walkthrough()).isEqualTo(UPDATED_BAC_TD_WALKTHROUGH);
        assertThat(testProject.getBac_td_analysis()).isEqualTo(UPDATED_BAC_TD_ANALYSIS);
        assertThat(testProject.getBac_ut_execution()).isEqualTo(UPDATED_BAC_UT_EXECUTION);
        assertThat(testProject.getBac_utplan_review()).isEqualTo(UPDATED_BAC_UTPLAN_REVIEW);
    }

    @Test
    @Transactional
    public void updateNonExistingProject() throws Exception {
        int databaseSizeBeforeUpdate = projectRepository.findAll().size();

        // Create the Project

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restProjectMockMvc.perform(put("/api/projects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(project)))
            .andExpect(status().isCreated());

        // Validate the Project in the database
        List<Project> projectList = projectRepository.findAll();
        assertThat(projectList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteProject() throws Exception {
        // Initialize the database
        projectService.save(project);

        int databaseSizeBeforeDelete = projectRepository.findAll().size();

        // Get the project
        restProjectMockMvc.perform(delete("/api/projects/{id}", project.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Project> projectList = projectRepository.findAll();
        assertThat(projectList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Project.class);
        Project project1 = new Project();
        project1.setId(1L);
        Project project2 = new Project();
        project2.setId(project1.getId());
        assertThat(project1).isEqualTo(project2);
        project2.setId(2L);
        assertThat(project1).isNotEqualTo(project2);
        project1.setId(null);
        assertThat(project1).isNotEqualTo(project2);
    }
}
